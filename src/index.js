import React from 'react';

import {useState, useContext} from 'react';




import ReactDOM from 'react-dom';

import firebaseConfig from "Components/Constants/firebaseJSON"

//import * as serviceWorker from './serviceWorker';
import theme from 'Components/Styles/theme';
import {ThemeProvider} from 'styled-components';
import { BrowserRouter } from 'react-router-dom';
import ClayProvider from 'local_npm/clay-components'

import App from './App';


import Auth from "Components/Providers/Auth"
import ModalManager from "Components/Providers/Modals"

//import "static/fonts/Gordita.css" 
const Init = ()=>(
	<ClayProvider>
	     <Auth>
          <ModalManager>
			<ThemeProvider theme={theme}>
				<BrowserRouter>
					<App />
				</BrowserRouter> 
			</ThemeProvider>
          </ModalManager>
        </Auth>
	</ClayProvider>
);






























ReactDOM.render(<Init />, document.getElementById('root'));


// Error:
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
