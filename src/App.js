import React, { Component, useState, useContext } from 'react';

import Landing from 'Components/Views/Landing';
import Home from 'Components/Views/Home';
import Dashboard from 'Components/Views/Dashboard';
import Workspace from 'Components/Views/Workspace';
import Draft from 'Components/Views/Draft';
import Editor, {EditorUpdate} from 'Components/Views/Editor';

import Error404 from 'Components/Views/Error404';

import Privacy from 'Components/Views/Privacy';
import Login from 'Components/Views/Login';
import Logout from 'Components/Molecules/Logout';
import Terms from 'Components/Views/Terms';
import Success from "Components/Views/Success"

import scrollToTopOnMount from 'Components/General/scrollToTopOnMount';
import { Route, Switch } from 'react-router-dom';

import {NoMatch, AuthenticatedRouteRedirect as IfAutenticatedRouteRedirect , AuthenticatedRoute, AuthenticatedRouteHidden} from "Components/Routes/utils/auth"


import {AuthContext} from "Components/Providers/Auth"

export const ModalsContext = React.createContext();

const App = (props)=> {


  const {user, loading} = useContext(AuthContext)

  if(loading)
    return <loading/>

  return (
      <React.Fragment>
  
          <Switch>

            <IfAutenticatedRouteRedirect authenticated={user}  exact path="/login" component = {scrollToTopOnMount(Login)}/>

            <AuthenticatedRoute authenticated={user} exact path="/projects" component = {scrollToTopOnMount(Landing)}/>

            <AuthenticatedRoute authenticated={user} exact  path="/open/editor/:type/:id" component = {scrollToTopOnMount(EditorUpdate)}/>
            <AuthenticatedRoute authenticated={user} exact  path="/success/:type/:id" component = {scrollToTopOnMount(Success)}/>

            <AuthenticatedRoute authenticated={user} path="/editor/:type" component = {scrollToTopOnMount(Editor)}/>
            <AuthenticatedRoute authenticated={user} path="/w/:type" component = {scrollToTopOnMount(Workspace)}/>
            
            <AuthenticatedRoute authenticated={user} path="/draft" component = {scrollToTopOnMount(Draft)}/>

            <Route exact path="/terms" component = {scrollToTopOnMount(Terms)}/>

            <Route exact path="/privacy" component = {scrollToTopOnMount(Privacy)}/>
             <Route exact path="/logout" component = {scrollToTopOnMount(Logout)}/>

            <AuthenticatedRoute 
              authenticated={user} 
              exact path="/" 
              component = {scrollToTopOnMount(Home)}/>

                
                <NoMatch/>

              </Switch>

            
          

      </React.Fragment>
    );
  }

export default App;

