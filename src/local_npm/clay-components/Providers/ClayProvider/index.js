import React, {useState, useEffect} from "react";
import ManagerMenu from "./ManagerMenu";




import Rebase from "re-base";
import firebase from "firebase";



export const ClayContext  = React.createContext();


const ClayProvider = ({children, databaseId, manager})=>{
	const [slots, _setSlot] = useState({
		"container": "custom",

		"typography": "default"
	});

	const [_props, _setProps] = useState({

		"container":{
			default:{none:""},
			custom:{none:""}
		},

		"typography":{
			default:{none:""},
			custom:{none:""}
			
		}


	});




	const setSlot = (type, value)=>{

		const copy = {...slots};

		copy[type] = value

		_setSlot(copy)
	}




	const setProps = (type,slot, id, props)=>{

		const copy = {..._props};

		if(!copy[type])
			copy[type] = {}


		if(!copy[type][slot])
			copy[type][slot] = {}

		if(!copy[type][slot][id])
			copy[type][slot][id] = {}

		copy[type][slot][id] = props
		
		_setProps(copy)
	//	firebase.database().ref(`clayComponentsDemo/`).set(copy)

		/*if(databaseId)

		firebase.database().ref(`clayComponentsDemo/${databaseId}`).set(copy)*/


	}



	const setPropsValue = (type,slot, id, propName, value)=>{

		const copy = {..._props};

		if(!copy[type])
			copy[type] = {}

		if(!copy[type][slot])
			copy[type][slot] = {}

		if(!copy[type][slot][id])
			copy[type][slot][id] = {}

		copy[type][slot][id][propName] = value

		if(slot === "default")
			alert("Your are editing the default slot")
		
		_setProps(copy)

		
		if(databaseId)
		firebase.database().ref(`clayComponentsDemo/${databaseId}`).set(copy)

	}



  useEffect(() => {

  		const base = Rebase.createClass(firebase.database());



  	
  
  		if(databaseId)
          base.syncState(`clayComponentsDemo/${databaseId}`, {



              context: {



                setState: (data) => {


                	

                	if(!data._props)
                	{



                			console.log(_props)

                			firebase.database().ref(`clayComponentsDemo/${databaseId}`).set(_props)


                	}
  

                	return _setProps(data._props)
                },

                state: { _props },
              },


              state: "_props"


            })





       /*  const closeJson1  = base.syncState(`clayComponentsDemoSlot`, {
              context: {
                setState: (data) => {

                		if(!data.slots)
                		firebase.database().ref(`clayComponentsDemoSlot`).set(slots)
                		
                		_setSlot(data.slots)
                },
                state: { slots },
              },
              state: "slots"
              
            })
*/


        return ()=>{/*
      
        	
        		if(closeJson)
        			closeJson()
  		if(closeJson1)
        			closeJson1()*/


        }   
    },[children])



































	return (
		<React.Fragment>

			<ClayContext.Provider 
				value={{
					slots,
					setSlot,
					manager,
					data:_props, 
					setProps,
					setPropsValue,
				}}>

				{children}
				{manager && <ManagerMenu />}
			</ClayContext.Provider>
		</React.Fragment>
		)
}


export default ClayProvider;