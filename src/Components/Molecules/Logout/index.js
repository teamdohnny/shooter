
import React, { Component } from 'react';
import {Container, Text} from 'local_npm/clay-components';
import ReactDOM from 'react-dom';
import { Redirect, withRouter} from 'react-router-dom';

import firebase from "firebase"



const Logout = withRouter((props)=> {

class Logout extends Component {
  constructor() {
    super()
    this.state = {
      redirect: false
    }
  }

  componentDidMount() {
    firebase.auth().signOut().then((user) => {
     this.setState({ redirect: true })
    })
  }

  render() {

    if (this.state.redirect === true) {
      return <Redirect to="/" />
    }

    return <Container center  fullscreen css="background: white;"><h1>Cerrando sesion 👋🏽</h1></Container>
  }
}

return <Logout />
                
}
)

export default Logout