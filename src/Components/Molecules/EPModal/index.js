import React, {  useContext  } from 'react';

import Navbar from "Components/Molecules/Navbar";
import Brands from "Components/Molecules/Brands";


import { Button, PlayIcon } from 'Components/Primitives/Button';

import Footer from "Components/Molecules/Footer";
import BaselineClose from "react-md-icon/dist/BaselineClose";
import {ModalsContext} from "App";
import styled, {createGlobalStyle} from 'styled-components';

export const Fullscreen = styled.div`
  ${props=>props.theme.utils.fullscreen()}
`;

export const Body = styled.div`
  width: 35%;
  height: 350px;
  margin: 3rem auto;
  text-align: center;
  background: white;
  padding: 2.2rem 1.5rem;
  border: #CBCBCB solid 1px;
  border-radius: 6px;
  position: relative;
  color: #202124;
  ${props=>props.theme.utils.centerContent()}
  ${props=>props.theme.media.tablet`
    width: 90%;
  `}
`;


export const Title = styled.div`
    font-size: 20px;
    color: #202124;
    font-weight: 500;
    width: 80%;
    margin: 1rem auto;
    
    text-align: center;
     ${props=>props.theme.media.tablet`
      width: 90%;
     
    `}
`
export const Subtitle = styled.div`
 font-size: 18px; 
 color: #65696B;
  width: 70%;
  text-align: center;
  margin: 2rem auto;
  line-height: 25px;
  font-weight: 400;
  ${props=>props.theme.media.tablet`

       line-height: 26px;
     font-size: 16px; 
      width: 80%;
  `}
`
export const ImgContainer = styled.div`

width: 30%;
margin: 0 auto;
z-index: 1;
`
export const Img = styled.img`
 
  width: 100%;

`

export const FooterImg = styled.img`
   width: 45%;
   margin: 4rem auto;
`
export const Row = styled.div`
  ${props=>props.theme.utils.rowContent()}
  width: 91%;
  margin: 0 auto;

    ${props=>props.theme.media.tablet`

      width: 100%;
   
       
    `}
    
`
export const CardRow = styled.div`
  ${props=>props.theme.utils.rowContent()}
  width: 75%;
  margin: 0 auto;

   ${props=>props.theme.media.tablet`

      width: 90%;
   
       
    `}
  
    ${props=>props.theme.utils.centerContent()}
    height: 70px;
`


export const Card = styled.div`
  position: relative;
  width: 48%;
  margin: 2rem auto;
  text-align: center;
  ${props=>props.theme.utils.centerContent()}
 
  background: white;
  border-radius: 5px;
  overflow: hidden;
  padding: 2.2rem 0rem;
  border: #CBCBCB solid 1px;
   .closeIcon{
      display: none;
     }
 
  &:hover{
    cursor: pointer;
      border: none;
     box-shadow: 0 0.3px 5px rgba(0,0,0,0.2);
     .closeIcon{
      display: block;
     }

  }
 
    ${props=>props.theme.media.tablet`
      width: 90%;
      margin-top: 1.5rem;
       
    `}
`;

export const CardTitle = styled.div`
 font-size: 40px;
 color: #202124;
 font-weight: 500;
  width: 80%;
  margin: 0 auto;
  
       text-align: left;
     ${props=>props.theme.media.tablet`

 
       font-size: 22px;
       text-align: left;
    `}
`

export const CardSubtitle = styled.div`
 font-size: 18px; 
 color: #65696B;
  width: 70%;
  text-align: center;
  margin: 2rem auto;
  line-height: 16px;
  font-weight: 400;
  ${props=>props.theme.media.tablet`
    width: 90%;
    font-size: 16px; 
    line-height: 26px;
  `}
`;


const BodyScrollFixed = createGlobalStyle`
  body {
    position: fixed;
  }`




const EPModal = () => {


  const modals = useContext(ModalsContext);

  return (
<React.Fragment>
<BodyScrollFixed />

<Fullscreen 
  style={{overflowY:"scroll"}}>

<Fullscreen 
  style={{background:"transparent", zIndex:"-1"}} 
  onClick={()=>{modals.close();}}
/>

<Body> 
  <label  style={{color:"rgba(0,0,0,0.7)", fontSize:"25px", textAlign:"left", marginBottom:"2rem"}}>LLamanos:</label>
  <Title>
    +52-1-33-2620-8608
  </Title>
  <br/>
  <br/>
  <br/>

<Button onClick={()=>modals.close()}>Continuar</Button>

<BaselineClose 
   onClick={()=>{modals.close()}}
    style={{ 
      cursor: "pointer",
      color: "#202124",
      position:"absolute", 
      top:"1rem", 
      right: "1rem", 
      fontSize: "25px"
    }}
  />

  </Body>

  </Fullscreen>
</React.Fragment>
)};

export default EPModal