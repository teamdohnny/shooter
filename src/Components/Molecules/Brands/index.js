import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';

const width = (100 / 7) - 1;
const width1 = (100 / 9) ;
const width2 = (100 / 5) ;

const Brands = () => (
   <React.Fragment>

    <Container 
    	row 
    	width="100%" 
    	margin="0 auto" 
    	css="margin-top:110px;">

		<Container  
			center 
			width={(width)+"%"} 
			margin="0.5rem 0"  
			overflowHidden  
			tabletCSS="width:100%; display: inline;" >

			<Typography 
				fontSize="14px" 
				align="left" 
				weight="500">

			Experiencia:
			</Typography>

		</Container>
		<Img
			 
			center 
			src={require("static/pictures/Iconos/Google Developers.png")}  
			width={width+"%"} 
			margin="0.5rem 0"
			tabletCSS="width:32%;"
		/>

		<Img
			 
			center
			width={width+"%"} 
			margin="0.5rem 0"
			img={{css:`width:70%;`}}
			tabletCSS="width:32%;"
			src={require("static/pictures/Iconos/Hackers y founders.png")}  

			
		/>
		<Img
			 
			img={{css:`width:70%;`}}
			center 
			src={require("static/pictures/Iconos/Developers Circles.png")}  
			width={width+"%"} margin="0.5rem 0"
			tabletCSS="width:32%;"
		/>
		<Img
			 
			center 
			img={{css:`width:60%;`}}
			src={require("static/pictures/Iconos/Produtc Hunt.png")}  
			width={width+"%"} margin="0.5rem 0"
			tabletCSS="width:32%;"
		/>
		<Img
			 
			center 
			src={require("static/pictures/Iconos/Startup GDL.png")}  
			width={width+"%"} margin="0.5rem 0"
			tabletCSS="width:32%;"  
		/>
		<Img
			 
			center 
			src={require("static/pictures/Iconos/Startup Battle.png")}  
			width={width+"%"} margin="0.5rem 0"
			tabletCSS="width:32%;"  
		/>

    </Container>

    <Container 
    	row 
    	width="100%" 
    	margin="0 auto" 
    	css="margin-top:50px;">

		<Container  
			center 
			width={(width)+"%"} 
			margin="0.5rem 0"    
			tabletCSS="width:100%; display: inline;">

		<Typography 
			fontSize="14px" 
			align="left" 
			weight="500">Tecnologías:</Typography>
		</Container>
			<Img
				center
				
				style={{overflow:"none"}}
				img={{
					tabletCSS:"margin:0 auto;", 
					css:`width:65%; margin-left:-3.7rem;`,
				}} 
	
				src={require("static/pictures/Iconos/Javascript.png")}  
				width={width1 +"%"} margin="0.5rem 0"
				tabletCSS="width:32%;"  
			/>
			<Img
				
			style={{overflow:"none"}}
			img={{tabletCSS:"margin:0 auto;", css:`width:55%; margin-left:-4.8rem;`}} 
			center 
			src={require("static/pictures/Iconos/React.png")}  
			width={(width1)+"%"} margin="0.5rem 0"
			tabletCSS="width:32%;"  
			/>
			<Img	
				style={{overflow:"none"}} 
				img={{
					tabletCSS:"margin:0 auto;", 
					css:`width:65%; margin-left:-4.8rem;`
				}} 
				center 
				src={require("static/pictures/Iconos/Vue.png")}  
				width={(width1)+"%"} 
				margin="0.5rem 0"
				tabletCSS="width:32%;"  
			/>

			<Img
				style={{
					overflow:"none"
				}} 
				center 
				img={{
					tabletCSS:"margin:0 auto;", 
					css:`width:65%; margin-left:-4.8rem;`
				}} 
				src={require("static/pictures/Iconos/Graphql.png")}  
				width={(width1 )+"%"} 
				margin="0.5rem 0"
				tabletCSS="width:32%;"  
			/>

			<Img
				center
				margin="0.5rem 0"
				width={width1 +"%"} 
				tabletCSS="width:32%;"
				style={{overflow:"none"}} 
				img={{
					tabletCSS:"margin:0 auto;", 
					css:`width:65%; margin-left:-4.8rem;`
				}} 
				src={require("static/pictures/Iconos/AWS.png")}  
				
				 />

			<Img
				center
				width={width1 +"%"} 
				margin="0.5rem 0"
				tabletCSS="width:32%;"  
				style={{overflow:"none"}} 
				img={{
					tabletCSS:"margin:0 auto;", 
					css:`width:65%; margin-left:-4.8rem;`}} 
				src={require("static/pictures/Iconos/github.png")}  
			/>

			<Img
				center 
				style={{overflow:"none"}}
				width={width1+"%"} 
				margin="0.5rem 0"
				tabletCSS="width:32%;" 
				img={{
					tabletCSS:"margin:0 auto;", 
					css:`width:65%; margin-left:-4.8rem;`
				}}
				src={require("static/pictures/Iconos/Adobe XD.png")}/>

    </Container>


    <Container 
    	row
    	width="100%" 
    	margin="50px auto">

		<Container  
			center 
			width={(width -2.5) +"%"} 
			margin="0.5rem 0"    
			tabletCSS="width:100%; display: inline;">

			<Typography 
				fontSize="14px" 
				align="left" 
				weight="500">Clientes:</Typography>

		</Container>

		<Img
			center
			margin="0.5rem 0"  
			tabletCSS="width:49%;"
			width={(width2+1)+"%"} 
			img={{
				tabletCSS:"margin:0 auto;", 
				css:`width:65%; margin-left:-1rem;`}} 
			 
			src={require("static/pictures/Iconos/SIAC.png")}  
			

		/>

		<Img
			center
			width={(width2 +2)+"%"} 
			margin="0.5rem 0"  
			tabletCSS="width:49%;"
			img={{
				tabletCSS:"margin:0 auto;", 
				css:`width:65%;  
				margin-left:-4rem;`}}  
			src={require("static/pictures/Iconos/Reviu.png")} />

		<Img
			center 
			margin="0.5rem 0"
			width={(width2 +2)+"%"}  
			tabletCSS="width:49%;"			
			img={{
				tabletCSS:"margin:0 auto;", 
				css:`width:65%;  
				margin-left:-4rem;`}} 
			src={require("static/pictures/Iconos/CHANGE.png")}  

			/>

		<Img
			center
			margin="0.5rem 0"
			width={(width2 +2)+"%"}  
			tabletCSS="width:49%;" 
			img={{
				tabletCSS:"margin:0 auto;", 
				css:`margin-left:-4rem;`}} 
			src={require("static/pictures/Iconos/Vigilancia de marcas.png")}  
			
			
		/>
    </Container>
    </React.Fragment>
);

export default Brands;