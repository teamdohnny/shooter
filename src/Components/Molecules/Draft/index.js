
import React, { Component } from 'react';

import Editor, { createEditorStateWithText, ContentState , composeDecorators  } from 'draft-js-plugins-editor';
import { convertFromRaw, convertToRaw, createWithContent,EditorState} from 'draft-js';
import {Container, Text} from 'local_npm/clay-components';

import StickyNav from "./StickyNav";



import {
  ItalicButton,
  BoldButton,
  UnderlineButton,
  CodeButton,
  HeadlineOneButton,
  HeadlineTwoButton,
  HeadlineThreeButton,
  UnorderedListButton,
  OrderedListButton,
  BlockquoteButton,
  CodeBlockButton,
} from 'draft-js-buttons';





import createImagePlugin from 'draft-js-image-plugin';
import createInlineToolbarPlugin, { Separator } from 'draft-js-inline-toolbar-plugin';

//Limpo
import createMarkdownPlugin from 'draft-js-markdown-plugin';

import createToolbarPlugin from 'draft-js-static-toolbar-plugin';
import 'draft-js-static-toolbar-plugin/lib/plugin.css';

import createBlockDndPlugin from 'draft-js-drag-n-drop-plugin';
import createFocusPlugin from 'draft-js-focus-plugin'; //'draft-js-focus-plugin'



import createSideToolbarPlugin from 'draft-js-side-toolbar-plugin';


import 'draft-js-side-toolbar-plugin/lib/plugin.css';

import editorStyles from './editorStyles.css';

import 'draft-js-image-plugin/lib/plugin.css';

import  './editorStyles.css';

import ImageAdd from './ImageAdd';
import { stateToMarkdown } from "draft-js-export-markdown";
import { markdownToDraft, draftToMarkdown  } from 'markdown-draft-js'


import {convertMdToDraft, convertDraftToMd} from "./md"


/*
const entityType = {
  IMAGE: "IMAGE",
};

*/
// For `draft-js-image-plugin` to work, the entity type of an image must be `IMAGE`.
//const markdownPlugin = createMarkdownPlugin({ entityType });




















const blockDndPlugin = createBlockDndPlugin();


const focusPlugin = createFocusPlugin();



const decorator = composeDecorators(
  focusPlugin.decorator,
  blockDndPlugin.decorator
);

const imagePlugin = createImagePlugin({ decorator });


const inlineToolbarPlugin = createInlineToolbarPlugin();


const { InlineToolbar } = inlineToolbarPlugin;



const staticToolbarPlugin = createToolbarPlugin();
const { Toolbar } = staticToolbarPlugin;


const sideToolbarPlugin = createSideToolbarPlugin();
const { SideToolbar } = sideToolbarPlugin;



//const plugins = [sideToolbarPlugin];
/*inlineToolbarPlugin*/

const plugins = [staticToolbarPlugin, sideToolbarPlugin,  imagePlugin, blockDndPlugin, focusPlugin];


export default class CustomInlineToolbarEditor extends Component {

  constructor(props){
  


  super();
    


  var initialContent;


  try
  {
    initialContent = convertFromRaw(JSON.parse(props.value.content));
  }
   catch(e){

     initialContent = convertFromRaw(markdownToDraft(props.value.markdown || "", {
      blockEntities: {
        IMAGE:  (item)=> {

          return {
            type: 'atomic',
            mutability: 'IMMUTABLE',
            data: {src: item.src}
          }
        }
      }
    }
    )
    )
   }



    

    this.state ={
      editorState: EditorState.createWithContent(initialContent),
      initial: true
    }





  }



  onChange = (editorState) => {

    const sty = JSON.stringify( convertToRaw(editorState.getCurrentContent()))
    const markdown = stateToMarkdown(editorState.getCurrentContent())


    this.setState({
      editorState,
    });



    if(this.props.onChange && !this.state.initial ){
  
      this.props.onChange({

            content: sty,
            markdown



      });

    }else if(this.state.initial){
      this.setState({initial:false})
    }



  };




  focus = () => {
    this.editor.focus();
  };

  render() {
    return (
      <div>
      <Container 

      defaultShadow
      corner="10px"
            css={`


              padding: 1.5rem; 
              padding-top: 0rem; 
              font-weight: 500; 
              color: #212121; 
              font-size: 14px; 
              min-height: 400px;

              
                img,figure{
                  width:100%;
                  margin:0;
              

              }





              `}
            margin= "1rem auto"
                background="white" 
                onClick={this.focus}
        
                >


        <StickyNav  active={true} >
          <Toolbar>
          {
            // may be use React.Fragment instead of div to improve perfomance after React 16
            (externalProps) => (
              <div >

              
               <HeadlineOneButton {...externalProps} />
               <HeadlineTwoButton {...externalProps} />

                <BoldButton {...externalProps} />
                <ItalicButton {...externalProps} />
               
               
               
                <UnorderedListButton {...externalProps} />
                <OrderedListButton {...externalProps} />

                <ImageAdd
                     editorState={this.state.editorState}
                     onChange={this.onChange}
                     modifier={imagePlugin.addImage}
                   />
             


                
              </div>
            )
          }

        </Toolbar>
        </StickyNav>




        <Container height="2rem"/>

        <Container position="relative">

        <Editor
          placeholder={this.props.placeholder}
          editorState={this.state.editorState}
          onChange={this.onChange}
          plugins={plugins}
          ref={(element) => { this.editor = element; }}
        />






      
   </Container>



      </Container>
 

            

      </div>
    );
  }
}



/*

  <SideToolbar >
        {
            // may be use React.Fragment instead of div to improve perfomance after React 16
            (externalProps) => (
              <div >

              
               <HeadlineOneButton {...externalProps} />
               <HeadlineTwoButton {...externalProps} />

                <BoldButton {...externalProps} />
                <ItalicButton {...externalProps} />
               
               
               
                <UnorderedListButton {...externalProps} />
                <OrderedListButton {...externalProps} />

                <ImageAdd
                     editorState={this.state.editorState}
                     onChange={this.onChange}
                     modifier={imagePlugin.addImage}
                   />
             
                
              </div>
            )
          }
        </SideToolbar>





*/