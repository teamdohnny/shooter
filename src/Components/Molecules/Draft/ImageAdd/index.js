import React, {useState, useContext, useEffect} from 'react';import OutlineAddPhotoAlternate from "react-md-icon/dist/OutlineAddPhotoAlternate";
import {ModalContexts} from "Components/Providers/Modals";
import {Container, Text} from 'local_npm/clay-components';
import {Input2 as Input} from "Components/Primitives/Input";
import UploadFile from "Components/Primitives/UploadFile"

import  './style.css';


const ImageModal = ({closeModal, addImage})=>{


    const [url, setUrl] = useState("");





  return (<Container fullscreen center>
<Container fullscreen onClick={closeModal}/>

<Container 
  height="480px" 
  width="530px" 
  defaultCorner 
  defaultShadow 
  position="relative"
  background="white" 
  css="padding: 2rem;">


  <Text marginT="150px"  weight="500" fontSize="22px" marginB="20">
    Subir imagen.
  </Text>

  <Input
            type="text"
            placeholder="Pegar link de la imagen"
     
            onChange={(e)=>{
              console.log(e.target.value)
              setUrl(e.target.value)

              
            }}
            value={url}
          />



          <UploadFile   onChange={(data)=>{
                setUrl(data.url);
                
               // console.log(data);
              }}/>
         
  <Container row width="50%" css="position: absolute; bottom: 2rem; right:2rem;">
    <Container width="50%" onClick={closeModal} css="cursor:pointer;">
      Cancelar
    </Container>
    <Container width="50%" css="cursor:pointer;" onClick={()=>{
      addImage(url);
      closeModal();


    }}>
      <b>Añadir</b>
    </Container>
  </Container>
  </Container>
  </Container>
  )
}




const  ImageAdd  = ({editorState,onChange, modifier}) => {


  // Start the popover.

    const {openModal, closeModal}  = useContext(ModalContexts);





const addImage = (url) => {

    if(!url)
      {
        alert("No se ha seleccionado ninguna imagen.")
        return null;
      }

    onChange(modifier(editorState,url));
  };



const openImageModal  = ()=>{

    openModal(<ImageModal addImage={addImage} closeModal={closeModal}/>)


}



return (<div className={"ImgButtonWrapper"}>
  <button type="button" onClick={openImageModal} className={"ImgButton"}>
    <OutlineAddPhotoAlternate />
  </button>
</div>)


}

export default ImageAdd



/*


    ()    ()
(<=====//=====>)



<div className={"ImgButtonWrapper"}>
  <button onClick={this.onClick} className={"ImgButton"}>
    <OutlineAddPhotoAlternate />
  </button>
</div>



.ImgButtonWrapper {
  display: inline-block;
}

.ImgButton {
  background: #fbfbfb;
  color: #888;
  font-size: 18px;
  border: 0;
  padding-top: 5px;
  vertical-align: bottom;
  height: 34px;
  width: 36px;
}

.ImgButton:hover,
.headlineButton:focus {
  background: #f3f3f3;
}


*/
