
import React, { Component } from 'react';

import Editor, { createEditorStateWithText, ContentState   } from 'draft-js-plugins-editor';
import { convertFromRaw, convertToRaw, createWithContent,EditorState} from 'draft-js';
import {Container, Text} from 'local_npm/clay-components';





import {
  ItalicButton,
  BoldButton,
  UnderlineButton,
  CodeButton,
  HeadlineOneButton,
  HeadlineTwoButton,
  HeadlineThreeButton,
  UnorderedListButton,
  OrderedListButton,
  BlockquoteButton,
  CodeBlockButton,
} from 'draft-js-buttons';



import createImagePlugin from 'draft-js-image-plugin';
import createInlineToolbarPlugin, { Separator } from 'draft-js-inline-toolbar-plugin';
//import createMarkdownPlugin from 'draft-js-markdown-plugin';





import editorStyles from './editorStyles.css';

import 'draft-js-image-plugin/lib/plugin.css';
import 'draft-js-inline-toolbar-plugin/lib/plugin.css';
import  './editorStyles.css';

import ImageAdd from './ImageAdd';
import { stateToMarkdown } from "draft-js-export-markdown";
import { markdownToDraft, draftToMarkdown  } from 'markdown-draft-js'


import {convertMdToDraft, convertDraftToMd} from "./md"



const imagePlugin = createImagePlugin();

const inlineToolbarPlugin = createInlineToolbarPlugin();


const { InlineToolbar } = inlineToolbarPlugin;


const plugins = [inlineToolbarPlugin, imagePlugin/* createMarkdownPlugin()*/];


export default class CustomInlineToolbarEditor extends Component {

  constructor(props){
  


  super();
    


  var initialContent;


  try
  {
    initialContent = convertFromRaw(JSON.parse(props.value.content));
  }
   catch(e){

     initialContent = convertFromRaw(markdownToDraft(props.value.markdown || "", {
      blockEntities: {
        IMAGE:  (item)=> {

          return {
            type: 'atomic',
            mutability: 'IMMUTABLE',
            data: {src: item.src}
          }
        }
      }
    }
    )
    )
   }



    

    this.state ={
      editorState: EditorState.createWithContent(initialContent),
      initial: true
    }





  }



  onChange = (editorState) => {

    const sty = JSON.stringify( convertToRaw(editorState.getCurrentContent()))
    const markdown = stateToMarkdown(editorState.getCurrentContent())


    this.setState({
      editorState,
    });



    if(this.props.onChange && !this.state.initial ){
  
      this.props.onChange({

            content: sty,
            markdown



      });

    }else if(this.state.initial){
      this.setState({initial:false})
    }



  };




  focus = () => {
    this.editor.focus();
  };

  render() {
    return (
      <div>
      <Container 
      defaultShadow
      corner="10px"
            css={`


              padding: 1.5rem;  
              font-weight: 500; 
              color: #212121; 
              font-size: 14px; 
              min-height: 400px;

              
                img,figure{
                  width:100%;
                  margin:0;
              

              }





              `}
            margin= "1rem auto"
                background="white" 
                onClick={this.focus}
        
                >
        <Editor
          placeholder={this.props.placeholder}
          editorState={this.state.editorState}
          onChange={this.onChange}
          plugins={plugins}
          ref={(element) => { this.editor = element; }}
        />
        <InlineToolbar>
          {
            // may be use React.Fragment instead of div to improve perfomance after React 16
            (externalProps) => (
              <div>

              
               <HeadlineOneButton {...externalProps} />
               <HeadlineTwoButton {...externalProps} />

                <BoldButton {...externalProps} />
                <ItalicButton {...externalProps} />
               
               
               
                <UnorderedListButton {...externalProps} />
                <OrderedListButton {...externalProps} />

               
                
              </div>
            )
          }

        </InlineToolbar>


      </Container>
      <br/>
      <br/>

        <ImageAdd
                 editorState={this.state.editorState}
                 onChange={this.onChange}
                 modifier={imagePlugin.addImage}
               />
      </div>
    );
  }
}