import React, {useContext} from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import {ModalsContext} from "App"


const ProductCard = ({
              src=require("static/pictures/Landing/Mockups y prototipos.png"),
              title = "Mockups y prototipos.", 
              description=`Explora, testea y comunica visualmente los módulos de tu aplicación para validar interacciones y mejoras con tus usuarios finales. En esta etapa, tomamos tus ideas y las trasladamos en un prototipo que parecerá tu producto final pero que puede ser modificado cuantas veces se necesite. `
            ,consultoria=false
          }) => {

  const {openMail} = useContext(ModalsContext);


  return (
             <React.Fragment>
              <Container
                onClick={openMail} 
                row 
                defaultBorder 
                margin="0 auto" 
                css="margin-top:50px; padding:3rem; border-radius:5px; position: relative; &:hover{border:1px solid #0059FA;} cursor: pointer;"
                 tabletCSS="padding: 2rem;"
                >
          
               <Img
                width="43%"
                height="372px"  
                src={src}
                img={{
                  css:"height:90%; width: auto;",
                  tabletCSS:"height:auto; width: 100%;"
          
              }}  
                tabletCSS={`
                      ${"width:100%; height:auto; padding-top: 2rem;"}
                    `}
                overflowHidden/>
          
                <Container
                  center 
                  width="56%" 
                  tabletCSS={`
                      ${"width:100%;"}
                    `}>
           
                  <Typography 
                    fontSize="18px"
              
                    weight="400"
                    marginB="10px"
                    css={`width:80%; line-height:28px;`}
                    tabletCSS={"width: 100%; margin-top: 4rem; font-size:14px; line-height:24px;"} 
                    color="#000"> 
          
                  <Typography 
                    fontSize="30px"
                    css="line-height:40px;"
                    weight="500"
                    marginB="20"
                    css={`width:80%;`}
                    tabletCSS="width: 100%; font-size:20px; line-height:30px;"
                    color="#000">{title}</Typography>
                        {description}
                      <br/><br/><br/>
                      <Button onClick={openMail} transparent>
          
                        <Container 
                          center 
                          row 
                          style={{fontWeight:"500"}}>
          
                          <Typography 
                            weight="500" 
                            align="left" 
                            marginR="10"> Cotizar servicio
                            </Typography><ArrowIcon/>  
          
                        </Container>
          
                      </Button>
                    </Typography>
          
                </Container>
          
                 {consultoria && <Typography 
                           fontSize="14px"
                            css="line-height:28px;"
                           weight="500"
                           align="center"
                           
                           css={`
                             padding: 0.5rem 1.2rem;
                             
                             background:#F9F9F9; 
                             border-radius:34px;
                             position: absolute;
                             top:0.4rem;
                             left: 0.4rem;
                             `}
                         
                           color="#000">Consultoría</Typography>}
              </Container>
              </React.Fragment>
          );}

export default ProductCard;