import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import styled from 'styled-components';


export const Nav = styled.nav`
background:white;


	height: 105px;
	width: 60%;
	margin: 0 auto;

 ${props=>props.theme.media.tablet`
      width: 100%;
      margin: 0 auto;
      overflow: hidden;
   
    `}


`;



export const IconItem = styled.div`
	width: 21px;
	height: 64px;
	${props=>props.theme.utils.centerContent()}
	text-align: left;
	color:black;
	
	a{
  		text-decoration: none;
  	 	color:black;
	}
	 ${props=>props.theme.media.tablet`
      width: 18px;
   
    `}
`;



export const Row = styled.div`
	width: 70%;
	margin: 0 auto;
	${props=>props.theme.utils.rowContent()}
	  ${props=>props.theme.media.tablet`
	  margin: 0 auto;
      width: 80%;
   
    `}
`;


const Presentation = ({color, title, subtitle, img, features=[]}) => (
  
  
	<Nav>
	<br/>
	<Row>
	  
	  	<IconItem >
			<a target="_blank" href="https://twitter.com/imdohnny">
				<img style={{width:"100%"}} src={require("static/Illustrations-modern/Iconos/sn/Twitter.png")}/>
			</a>
		</IconItem>
		 <IconItem  >
			<a target="_blank" href="https://github.com/dohnny">
				<img style={{width:"100%"}} src={require("static/Illustrations-modern/Iconos/sn/Github.png")}/>
			</a>
		</IconItem>
		 <IconItem >
			<a target="_blank" href="https://www.youtube.com/channel/UCgOCt1lJexQGosaTXqfAerg/featured">
				<img style={{width:"100%"}} src={require("static/Illustrations-modern/Iconos/sn/Youtube.png")}/>
			</a>
		</IconItem>
		 <IconItem >
			<a target="_blank" href="https://medium.com/@imdohnny">
				<img style={{width:"100%"}} src={require("static/Illustrations-modern/Iconos/sn/Medium.png")}/>
			</a>
		</IconItem>
		 <IconItem >
			<a target="_blank" href="https://www.facebook.com/ImDohnny/">
				<img style={{width:"100%"}} src={require("static/Illustrations-modern/Iconos/sn/Facebook.png")}/>
			</a>
		</IconItem>
		 <IconItem >
			<a target="_blank" href="https://dribbble.com/Dohnny">
				<img style={{width:"100%"}} src={require("static/Illustrations-modern/Iconos/sn/Dribbble.png")}/>
			</a>
		</IconItem>
		<IconItem >
			<a target="_blank" href="https://www.instagram.com/imdohnny_/">
				<img style={{width:"100%"}} src={require("static/Illustrations-modern/Iconos/sn/Instagram.png")}/>
			</a>
		</IconItem>
	  </Row>
	</Nav>

);


export default Presentation;