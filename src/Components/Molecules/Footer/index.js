import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Table from "./Table"
import IconsNavbar from "./IconsNavbar"
export const Title = styled.div`
  font-size: 18px;
  font-weight: 500;
  color: #202124;
  margin: 0 auto;
  text-align: left;
`;

export const Dot = styled.div`
 width: 8px;
 height: 8px;
 background: #677282;
 border-radius: 50%;
`;

export const Nav = styled.nav`
background:white;


	height: 105px;
	width: 100%;

 ${props=>props.theme.media.tablet`
      width: 90%;
      margin: 0 auto;
      overflow: hidden;
   
    `}


`;

export const NavItem = styled.div`
	min-width: 35px;
	height: 64px;
	${props=>props.theme.utils.centerContent()}
	text-align: left;
	color:#677282;
	a{
  		text-decoration: none;
  	 	color:#677282;
	}

	 ${props=>props.theme.media.tablet`
      width: 90%;
   
    `}
`;


export const IconItem = styled.div`
	width: 21px;
	height: 64px;
	${props=>props.theme.utils.centerContent()}
	text-align: left;
	color:black;
	
	a{
  		text-decoration: none;
  	 	color:black;
	}
	 ${props=>props.theme.media.tablet`
      width: 12px;
   
    `}
`;

export const Body = styled.div`
  width: 100%;
  margin: 0 auto;
  text-align: left;
  background: white;
  position: relative;
`;

export const Content = styled.div`
	width: 90%;
	padding: 3rem 0;

	  ${props=>props.theme.media.tablet`
	  	display: none;
	.absoluteItems{
		display: none;
	}
   
    `}

`;

export const Row = styled.div`
	width: 95%;


	${props=>props.theme.utils.rowContent()}
	  ${props=>props.theme.media.tablet`
	  margin: 0 1rem;
      width: 70%;
   
    `}
`;

export const Ul = styled.ul`
  color: #677282;
  font-size: 15px;
  width: 24%;
  display: inline-block;
  margin: 0 auto;
  text-align: left;
  li{ 
    margin: 1.5rem auto;
    list-style: none;
    color:#677282;
  }
  a{
  	text-decoration: none;
  	 color:black;
  
  }
    ${props=>props.theme.media.tablet`
      width: 100%;
     
    `}
`;

const Presentation = ({color, title, subtitle, img, features=[]}) => (
  
  <Body>
<Table />
    <Content>
	<Row>
		<Ul>
					<li>
						<Title>
						Servicios
						</Title>
					</li>
		

		
					<li>
						<Link to="/services#myp">
						Mockups y prototipos 
						</Link>
					</li>
						<li>
					<Link to="/services#dw">
						Desarrollo web 
					
						</Link>
					</li>

						<li>
						<Link to="/services#ma">
						 Metodologías agiles 
						</Link>
					</li>

						<li>
						<Link to="/services#ux">
						Estrategia de producto y UX
						</Link>
					</li>

						<li>
						<Link to="/services#t">
						Tecnologías
						</Link>
					</li>

				</Ul>


		<Ul>
			<li>
				<Title>
				Nosotros
				</Title>
			</li>


			<li>
				<Link to="/whyus">
				Nuestros procesos 
				</Link>
			</li>

			<li>
				<Link to="/whyus#teamx">
				Experiencia Equipo Dohnny
				</Link>
			</li>

			<li>
				<Link to="/about#team">
		 Equipo Dohnny
				</Link>
			</li>			      
		</Ul>
	
	<Ul>

		<li>
			<Title>
			Compañia 
			</Title>
		</li>

		<li>
		<a  href="https://www.dohnny.com/" target="_blank">
			Dohnny App 
			</a>
		</li>

			<li>
				<a  href="https://agency.dohnny.com/" target="_blank">
			Dohnny Agency
			</a>
		</li>

			<li>
				<a  href="https://creative.dohnny.com/" target="_blank">
			 Dohnny Creative
			</a>
		</li>   
	  </Ul>  

	  <Ul>
	  <li>
			<Title>
			Legal 
			</Title>
		</li>
    


      <li>
        <a  href="https://www.dohnny.com/terms" target="_blank">
      Términos y Condiciones 
      </a>
    </li>

      <li>
        <a  href="https://www.dohnny.com/privacy" target="_blank">
      Privacidad
      </a>
    </li>
          
    </Ul>
	</Row>
  </Content>

  <hr style={{opacity: "0.2"}}/>
	<IconsNavbar/>
  </Body>
);


export default Presentation;