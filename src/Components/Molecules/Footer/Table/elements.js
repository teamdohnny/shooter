import styled from 'styled-components';
export const Dot = styled.div`
 width: 8px;
 height: 8px;
 background: black;
 border-radius: 50%;
`;
export const Row = styled.div`
	width: 90%;
	margin: 0 auto;

	${props=>props.theme.utils.rowContent()}
	 
`;

export const NavItem = styled.div`

	height: 64px;
	${props=>props.theme.utils.centerContent()}
	text-align: left;
	color:black;
	a{
  		text-decoration: none;
  	 	color:black;
	}

	
`;


export const Ul = styled.ul`
  color: black;
  font-size: 15px;
  width: 22%;
  display: inline-block;
  margin: 0 auto;
  text-align: left;
  li{ 
    margin: 1.5rem auto;
    list-style: none;
    color:black;
  }
  a{
  	text-decoration: none;
  	 color:black;
  	 opacity:0.7;
  }
    ${props=>props.theme.media.tablet`
      width: 100%;
     
    `}
`;
export const Title = styled.div `
	
	font-size: 25px;
	color: black;
	margin-bottom: 59px;
`

export const Text = styled.div `

	font-size: 18px;

	color: black;
	span {
		a {
			color: black;
			text-decoration: underline;
		}
	}
`

export const QuestionContainer = styled.div `
	width: 100%;
	margin-bottom: 72.5px;
	display: flex;
	flex-direction: column;
`

export const Question = styled.div `
	border-bottom: 1px solid black;
	margin-bottom: 34.5px;
	box-sizing: border-box;
	cursor: pointer;
`

export const Top = styled.div `
	display: flex;
	justify-content: space-between;
`

export const QuestionTitle = styled.div `
	
	font-size: 16px;
	margin-bottom: 25px;
	font-weight: 500;
	color: black;
`

export const Answer = styled.div `
	margin-bottom: 26.5px;
	${Text} {
		font-size: 16px;
		color: black;
	}
`

export const Content = styled.div `
	width: 50%;
	height: auto;
	margin-top: 129px;
	margin-bottom: 50px;
`

export const Container = styled.div `
	width: 100%;

	display: none;


	${(props) => props.theme.media.tablet `
		display: inline-block;
		height: auto;
		padding: 0 10%;
		${Content} {
			width: 100%;
		}
	`}
`
