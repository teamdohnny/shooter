import React, {Component, Fragment} from 'react';
import {Dot,
Row,
NavItem, Ul, Container, Content, Title, Text, QuestionContainer, Question, Top, QuestionTitle, Answer } from './elements';
import BaselineKeyboardArrowUp from "react-md-icon/dist/BaselineKeyboardArrowUp";
import BaselineKeyboardArrowDown from "react-md-icon/dist/BaselineKeyboardArrowDown";
import {Link} from "react-router-dom"
class FAQ extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAnswer: false,
    }
  }

  toggleAnswer() {
    let toggle = !this.state.showAnswer;
    this.setState({showAnswer: toggle});
  }

  render() {
    return(
      <Question onClick={this.toggleAnswer.bind(this)}>
        <Top>
          <QuestionTitle>{this.props.question}</QuestionTitle>
          {this.state.showAnswer? <BaselineKeyboardArrowUp style={{
            fontSize: "24px",
            color:"black"
          }}/> : <BaselineKeyboardArrowDown style={{
            fontSize: "24px",
            color:"black"
          }}/>}
        </Top>
          {
            this.state.showAnswer?
            <Answer>
            {this.props.answer === "1" && 
          
          <Ul>
            <li>
            <Link to="/services#myp">
            Mockups y prototipos 
            </Link>
            </li>
            <li>
            <Link to="/services#dw">
            Desarrollo web 

            </Link>
            </li>

            <li>
            <Link to="/services#ma">
             Metodologías agiles 
            </Link>
            </li>

            <li>
            <Link to="/services#ux">
            Estrategia de producto y UX
            </Link>
            </li>

            <li>
            <Link to="/services#t">
            Tecnologías
            </Link>
            </li>
          </Ul>


          }

            {this.props.answer === "2" && 

          <Ul>

              <li>
        <Link to="/whyus">
        Nuestros procesos 
        </Link>
      </li>

      <li>
        <Link to="/whyus#teamx">
        Experiencia Equipo Dohnny
        </Link>
      </li>

      <li>
        <Link to="/about#team">
     Equipo Dohnny
        </Link>
      </li>


          </Ul>

          }



          {this.props.answer === "3" &&

          <Ul>
    


      <li>
        <a  href="https://www.dohnny.com/" target="_blank">
      Dohnny App 
      </a>
    </li>

      <li>
        <a  href="https://agency.dohnny.com/" target="_blank">
      Dohnny Agency
      </a>
    </li>

      <li>
        <a  href="https://creative.dohnny.com/" target="_blank">
       Dohnny Creative
      </a>
    </li>

    <li>
      <a  href="https:// brands.dohnny.com/" target="_blank">
       Dohnny Brands
      </a>
    </li>          
    </Ul>


        }


          {this.props.answer === "4" &&

          <Ul>
    


      <li>
        <a  href="https://www.dohnny.com/terms" target="_blank">
      Términos y Condiciones 
      </a>
    </li>

      <li>
        <a  href="https://www.dohnny.com/privacy" target="_blank">
      Privacidad
      </a>
    </li>
          
    </Ul>


        }
                  
            </Answer>:

            <Fragment>
            

      </Fragment>
    }
          
      </Question>
    )
  }
}

const FAQstructure = () => (
  <Container>
    <Content>
    
      <QuestionContainer>
        <FAQ question="Servicios" answer="1"/>
       {/* <FAQ question="Community" answer="2"/>*/}
        <FAQ question="Nosotros" answer="2"/>
        <FAQ question="Compañia" answer="3"/>
         <FAQ question="Legal" answer="4"/>
      </QuestionContainer>
   
  
    <Row 
      className="absoluteItems"
      style={{
        width: "90%", 
        marginLeft:"23%"
        }}>
    
  
    </Row>
    </Content>

  </Container>
)

export default FAQstructure;
