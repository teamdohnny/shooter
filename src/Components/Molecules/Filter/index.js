import React, {useState, useContext} from "react"
import {Container, Text} from 'local_npm/clay-components';
import OutlineMoreVert from "react-md-icon/dist/OutlineMoreVert"
import Dropdown from "Components/Primitives/Dropdown"
import IconEdit from "react-md-icon/dist/RoundWhatshot";
import IconRemove from "react-md-icon/dist/BaselineCheck";
import BaselineArrowDropDown from "react-md-icon/dist/BaselineArrowDropDown";

import {Link} from "react-router-dom"
import {ModalContexts} from "Components/Providers/Modals"
import firebase from 'firebase';
import BaselineTune from "react-md-icon/dist/BaselineTune";
import {Button,ClayButton} from "Components/Primitives/Button";


const Item = (props)=>{

   return <Container
              {...props}
              center height="34px"
               

              css={`
                z-index:2;

                color: #80868B;
                ${props.disabled?"color:black;":""}


                 padding: 0 1rem;

                text-align:center;
               ${!props.disabled? `
                &:hover{
                  background:#F5F5F5;
                  color: black;
                }`:""
               }


               ${props.icon && !props.disabled?"background:#F5F5F5; color:#212121;":""}


                

              `}
              style={{fontWeight:"500"}}>
<Container row>

          
            
         
            <Text 
            weight="500"
              fontSize="14px"
              weight="600" 
              align="left" 
              marginL="10"
              > 
                {props.title}
            </Text> 


             {props.icon}




            </Container>

          </Container>



}


 

const Options = (props)=> {

  const Elements = ()=>{ 


  return   <Container 
            center={false}   
            height="120px">


           <Container css="padding-top:15px;">
              <Item 

                disabled 
                title="Estado"  
                icon={<BaselineArrowDropDown style={{fontSize:"22px"}} />}

                />
                <hr/>
          </Container>

          <Container>
              <Item  
                title="Todos"  
                onClick={()=>{props.setStatus("")}}
                icon={props.status === ""?<IconRemove style={{fontSize:"22px"}} />:null}

                />
          </Container>


          <Container>
              <Item  
               onClick={()=>{props.setStatus("published")}}
                title="Publicados"  
                icon={props.status === "published"?<IconRemove style={{fontSize:"22px"}} />:null}

                />
          </Container>



             <Container>
              <Item  
              onClick={()=>{props.setStatus("draft")}}
                title="Borradores"  
                   icon={props.status === "draft"?<IconRemove style={{fontSize:"22px"}} />:null}

                />
          </Container>


          <Container css="padding-top: 15px;">
              <Item 
              disabled 
                title="Fecha"  
                icon={<BaselineArrowDropDown style={{fontSize:"22px"}} />}

                />
                <hr/>
          </Container>


          <Container>
              <Item  
                title="Hoy"  
                icon={props.date===24?<IconRemove style={{fontSize:"22px"}} />:null}
                onClick={()=>props.setDate(24)}

                />
          </Container>


          <Container>
              <Item  
                title="Últimos 7 días"  
                 icon={props.date===24*7?<IconRemove style={{fontSize:"22px"}} />:null}
                onClick={()=>props.setDate(24*7)}

                />
          </Container>

           <Container>
              <Item

                title="Este mes"  
                 icon={props.date===24 * 30.5?<IconRemove style={{fontSize:"22px"}} />:null}
                onClick={()=>{props.setDate(24*30.5)}}

                />
          </Container>

          <Container>
              <Item  
                title="Todos los tiempos"  
                 icon={props.date===0?<IconRemove style={{fontSize:"22px"}} />:null}
                onClick={()=>props.setDate(0)}
                />
          </Container>







  <a>
           <Container
          
               center
              row 
              height= "40px"
                css={`color: #80868B;   padding: 0 1rem; text-align:left;
                &:hover{
                  background:#F5F5F5;
                  color: black;
                }


              `}
              style={{fontWeight:"500"}}>


           
  <Container  width="80%" >
            <Text 
              fontSize="14px"
              weight="500" 
              align="left"
              marginL="10"
              weight="600"
              > 
               
            </Text>

          </Container> 

          </Container>
          </a>
          
        </Container>

  }



  return (<Dropdown
            elements={<Elements />}
            css="z-index:2; padding:10px 0;"
            background="white" 
            corner="10px"
            shadow={"0px 0.5px 6px rgba(0,0,0,0.4)"}
            position="absolute" 
            distance="auto auto -415px -150px" 
            height="405px"

            width="260px">
            <b>
            

    <Button transparent css="&:focus {outline: none; }" >
    <Container

      center 
      row 

      css="color: #80868B; padding: 0.6rem 2rem;"
      
 

      style={{fontWeight:"500"}}>
    
      <Text 
        fontSize="14px"
        weight="500" 
        align="left" 
        marginR="16"> 
          Filtro
        </Text>

        <BaselineTune style={{fontSize:"22px"}}  /> 
    
        </Container>
      </Button>



            </b>
          </Dropdown>
      )
}

export default Options;



