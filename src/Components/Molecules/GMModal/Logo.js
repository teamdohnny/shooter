import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';


import { 
  Redirect, 
  Link,
  BrowserRouter, 
  Route, 
  Switch, 
  withRouter} from 'react-router-dom';


 const LogoTitle = styled.div`
    font-size: 25px;
`;

 const Logo = styled.img`
    padding-top:2px;
    width: 23px;
    margin-left: -10px;
`;

 const Nav = styled.nav`
    top:0;
    left:0;
    position: fixed;
    overflow: hidden;
    width: 100%;
    background: white;
    z-index:999999999;
   
  
`;

 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent}
    width: 91.5%;
    margin: 0 auto;
   //background: red;
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;
`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;

 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
`;

 const Separator = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;

const Icon = styled.i`
  vertical-align: bottom;
`;


const NavsContainer = styled.div`
  z-index:2;

  ${props=> props.shadow && ` 
    box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);
    position: fixed;
    top:0;
    width: 100%;
    height: ${65*2}px;
  `}
`;

const TabSet = styled.div`
  border: 1px red solid;
  display: ${props=>props.notNavbar?"none": "inline"};
  @media (max-width: 700px) {
    display: none;
  }
`;

 const Item = styled.div`
    position: relative; 
    height: 65px;
    cursor: pointer;
    min-width: 45px;
    font-size: 15px;
    font-weight: 500;
    color:${props=> props.active?"#677282":"#5F6469" };
    ${props=>props.theme.utils.centerContent()}
    ${props=>props.cursor && "cursor: pointer;"}
    ${props=>props.margin && "margin: 0 1.2rem;"}
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 700px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}

    ${props=>props.hide && `
        @media (max-width: 700px)
        {
          display: none;
        }
    `} 

    a{
      text-decoration: none;
      color:${props=> props.active?props.theme.colors.green:"black" };
      font-weight: 500;
    }
`;

const TabItem = ({children, active,hide ,onClick})=>{

  const TabSelector = styled.div`
    background: ${(props)=>props.theme.colors.green};
    position: absolute;
    width: 65%;
    height: 4px;
    font-size: 15px;
    border-radius: 5px 5px 0 0;
    bottom: 0;
    left:17.5%;
  `;

  return(
    <Item 
    hide
      style={{margin:"0 0.8rem"}}
      onClick={onClick} 
      active={active}
      cursorPointer>
      {children}
      {active &&  <TabSelector/>}
    </Item>
    )
}


 const ButtonMore = styled.button`
  background: white;
  color: ${props=>props.theme.colors.green};
  border : none;
  box-shadow: 0 0.5px 5px rgba(0,0,0,0.2);
  width: 120px;
  position: relative;
  padding: 0.6rem 0.8rem;
  margin: 0 0.5rem;
  border-radius: 20px;
  overflow: hidden;
  cursor: pointer;
  font-size: 16px;
`;

const logo = require("static/logo/logo.png")

class Header extends Component
{
 
  render() {
    
    return (

          <Nav>
          <NavContent>
          <AlignStart>
            <Item style={{ 
              
              marginRight:"0.1rem",
             
             
              }}>
              <Link to="/">
                <Logo  src={logo}/>
              </Link>
            </Item>

            <Item
             
              style={{
                marginRight:"0.2rem",
                marginLeft: "-0.3rem" }}>
              
              <Link to="/"
                style={{ 
                fontSize: "22px",
                color: "#202124"
                }}>
                  Dohnny 
              </Link>
            </Item>


            <Item 
            
              style={{
                marginRight:"1rem",
                marginLeft: "0.3rem" }}>
              
              <Link to="/"
                style={{ 
                fontSize: "22px",
                color:"#677282", 
                fontSize:"20px",
                fontWeight:"300", 
                color:"#0059FA"}}>
                  Agency  
              </Link>
            </Item>       
  </AlignStart>
 
            </NavContent>
            </Nav> 
);
  }
}

export default withRouter(Header)



//width: "100%";