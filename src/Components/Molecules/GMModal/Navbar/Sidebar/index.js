import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import { Redirect, Link,BrowserRouter, Route, Switch, withRouter} from 'react-router-dom';
//import MorePopover from "./MorePopover"
import HelpIcon from "react-md-icon/dist/RoundHelpOutline";
import { Button } from 'Components/Primitives/Button';
import RoundMenu from "react-md-icon/dist/RoundMenu";
import RoundClose from "react-md-icon/dist/RoundClose";
const _ = require('lodash');

const UserCirlce = styled.div`
  height: 34px;
  width: 34px;
  border-radius: 50%;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  background-image: ${props => props.src? "url(" +props.src+ ")": "url('http://placehold.it/50x50')"};
`;

 const LogoTitle = styled.div`
    font-size: 25px;
`;

 const Logo = styled.img`
    padding-top:2px;
    width: 23px;
    margin-left: -10px;
`;

 const Nav = styled.nav`
    top:${65*2}px;
    left:0;

    position: fixed;
    overflow: hidden;
    width: 100%;
    background: white;
    text-aling: left;
    
    ${props=>!props.noShadow && "box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);"};
    
    color: ${props=>props.theme.color.navbarText};
 width: 100%;

    z-index:999;
`;

 const NavContent = styled.nav`
  
    width: 90%;
    margin: 2rem auto;
`;


 
 const Item = styled.div`
    position: relative; 
    text-aling: left;
 width: 100%;
 margin-top: 0.5rem;
    cursor: pointer;
    width: 100%;
    color:${props=> props.active?"black":"#5F6469" };

    ${props=>props.cursor && "cursor: pointer;"}
  
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 700px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}
    ${props=>props.hide && `
        @media (max-width: 700px)
        {
          display: none;
        }
    `}   
    a{
        text-decoration: none;
         color:${props=> props.active?props.theme.colors.green:"#5F6469" };
 
       font-weight: 500;
    }
`;





class Header extends Component
{
  constructor(props) 
  {
      super(props)
      this.state = {
        sidebar: false,
        tutorial: false,
      }
  }
  
  render() {
    const tab = this.props.location.pathname
    return (  
    <React.Fragment>  
          <Nav>
          <NavContent>

            <Item>
                
                  <Link 
                    style={{ 
                    textDecoration:"none",
                    color:"gray"
                    }}
                    to="/login"><Button style={{ width: "90%", height:"50px"}}>Early access</Button>
                  </Link>
              </Item>
       
           
              <Item>
                   <a target="_blank" href="https://medium.com/@imdohnny/dohnny-an-agile-online-ide-to-maintain-react-components-fe8c86a0fc8a" 


          
                    style={{ 
                    textDecoration:"none",
                    color:"gray"
                    }}
                   > 
                    <Button style={{  width: "90%", height:"50px"}} transparent>Learn more</Button>
                  </a>
              </Item>

            

            </NavContent>
            </Nav>  
          </React.Fragment>
      );
  }
}

export default withRouter(Header)

