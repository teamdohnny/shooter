import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import Sidebar from "./Sidebar"

import { 
  Redirect, 
  Link,
  BrowserRouter, 
  Route, 
  Switch, 
  withRouter} from 'react-router-dom';
import HelpIcon from "react-md-icon/dist/RoundHelpOutline";


import RoundMenu from "react-md-icon/dist/RoundMenu";
import RoundClose from "react-md-icon/dist/RoundClose";
import {ModalsContext} from "App";




const _ = require('lodash');



export const Button = styled.button`
  position: relative;
  padding: 0.8rem 1.2rem;
  margin: 0 0.5rem;

  border-radius: 3px;
  overflow: hidden;
  background: ${props => (!props.transparent ? "#0059FA" : "transparent")};
  color: ${props => (!props.transparent ? "white" : "#0059FA")};
  cursor: pointer;
  font-weight: 500;
  font-size: 14px;
  border: ${props => (!props.transparent ? "0" : `solid 1px #CBCBCB`)};


  ${props=>props.disabled && `
      opacity: 0.2;

    `}
`;




const UserCirlce = styled.div`
  height: 34px;
  width: 34px;
  border-radius: 50%;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  background-image: ${props => props.src? "url(" +props.src+ ")": "url('http://placehold.it/50x50')"};
`;

 const LogoTitle = styled.div`
    font-size: 25px;
`;

 const Logo = styled.img`
    padding-top:2px;
    width: 23px;
    margin-left: -10px;
`;

 const Nav = styled.nav`
    bottom:0;
    left:0;
    position: fixed;
    overflow: hidden;
    width: 100%;
    background: white;
    ${props=>!props.noShadow && "box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);"};
    color: ${props=>props.theme.color.navbarText};
    height: 65px;
    ${props=>props.theme.utils.rowContent}
      z-index:9999999999999999999999999999999;
  
`;

 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent}
    width: 91.5%;
    margin: 0 auto;
   //background: red;
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;
`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;

 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
`;

 const Separator = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;

const Icon = styled.i`
  vertical-align: bottom;
`;


const NavsContainer = styled.div`
  z-index:2;

  ${props=> props.shadow && ` 
    box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);
    position: fixed;
    bottom:0;
    width: 100%;
    height: ${65*2}px;
  `}
`;

const TabSet = styled.div`
  border: 1px red solid;
  display: ${props=>props.notNavbar?"none": "inline"};
  @media (max-width: 700px) {
    display: none;
  }
`;

 const Item = styled.div`
    position: relative; 
    height: 65px;
    cursor: pointer;
    min-width: 45px;
    font-size: 15px;
    font-weight: 500;
    color:${props=> props.active?"black":"#5F6469" };
    ${props=>props.theme.utils.centerContent()}
    ${props=>props.cursor && "cursor: pointer;"}
    ${props=>props.margin && "margin: 0 0.5rem;"}
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 700px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}

    ${props=>props.hide && `
        @media (max-width: 700px)
        {
          display: none;
        }
    `} 

    a{
      text-decoration: none;
      color:${props=> props.active?props.theme.colors.green:"#5F6469" };
      font-weight: 500;
    }
`;

const TabItem = ({children, active,hide ,onClick})=>{

  const TabSelector = styled.div`
    background: ${(props)=>props.theme.colors.green};
    position: absolute;
    width: 65%;
    height: 4px;
    font-size: 15px;
    border-radius: 5px 5px 0 0;
    bottom: 0;
    left:17.5%;
  `;

  return(
    <Item 
    hide
      style={{margin:"0 0.8rem"}}
      onClick={onClick} 
      active={active}
      cursorPointer>
      {children}
      {active &&  <TabSelector/>}
    </Item>
    )
}
 

 const ButtonMore = styled.button`
  background: white;
  color: ${props=>props.theme.colors.green};
  border : none;
  box-shadow: 0 0.5px 5px rgba(0,0,0,0.2);
  width: 120px;
  position: relative;
  padding: 0.6rem 0.8rem;
  margin: 0 0.5rem;
  border-radius: 20px;
  overflow: hidden;
  cursor: pointer;
  font-size: 16px;
`;
 const Row = styled.div`
    width: 100%;
   ${props=>props.theme.utils.rowContent()}
    @media (max-width: 700px) {
    display: block;
  }

`;

 const Navigator = styled.nav`
 position:absolute;
 width: 200px;
 left: 50%;
 margin-left -100px;
 height: 65px;
 top:0;
 ${props=>props.theme.utils.centerContent()}
 z-index:99999;


   ${props=>props.theme.media.tablet`
   display: none; 
    `}
`;


 const NavigatorDot = styled.div`
 width: 15px;
 height: 15px;
 border-radius: 50%;
cursor: pointer;
 display: inline-block;
 margin: 0 0.3rem;
   @media (max-width: 700px) {
          margin: 0 0.25rem;
         }
 
  ${props=> (props.val === props.current) && `
       background: white;
       border: solid 1px blue;

    `}

     ${props=> (props.val < props.current) && `
       background: blue;
       

    `}
     ${props=> (props.val > props.current) && `
        background: rgba(0,0,0,0.2);
      
       

    `}


`;

const logo = require("static/logo/logo.png")

class Header extends Component
{
  constructor(props) 
  {
      super(props)
      this.state = {
        sidebar: false,
        tutorial: false,
      }
  }
  
  render() {
    const tab = this.props.location.pathname
    return ( 
    <ModalsContext.Consumer>
    {value=> 
    <React.Fragment>  

    <NavsContainer
      shadow={this.state.sidebar}>

      <Nav>

          <NavContent>
          <AlignStart>


            {this.props.current !== 0 && 
              <Item>
                          <Button 
                          onClick={()=>{this.props.changeCurrent(this.props.current - 1)}}
                          style={{  width: "200px", fontSize:"14px", border:"none", color:"#677282"}} transparent>Atras</Button>  
                        </Item>
                    }

 

          </AlignStart>

          <Navigator onClick={()=>{}}>
            <Row>
            <NavigatorDot val={0} current={this.props.current}  onClick={()=>{if(this.props.current > 0 )this.props.changeCurrent(0)}} />
            <NavigatorDot val={1} current={this.props.current}  onClick={()=>{if(this.props.current > 1 || this.props.form.size )this.props.changeCurrent(1)}}  />
            <NavigatorDot val={2} current={this.props.current}  onClick={()=>{if(this.props.current > 2 || this.props.form.type )this.props.changeCurrent(2)}} />
            <NavigatorDot val={3} current={this.props.current}  onClick={()=>{if(this.props.current > 3 || this.props.form.name || this.props.form.company || this.props.form.mail || this.props.form.numbe )this.props.changeCurrent(3)}} />
            </Row>
           </Navigator>

            <AlignEnd> 
       

              <Item >
             
                {this.props.current === 3?
                              <Button 
                                  disabled={!this.props.form.name || !this.props.form.company || !this.props.form.mail || !this.props.form.number}
                                  onClick={()=>{this.props.submitMail()}}
                                  style={{  width: "150px", fontSize:"14px"}}>Finalizar</Button>: 

                                  <Button 
                                  disabled={
                                    (!this.props.form.service &&  this.props.current === 0) || 
                                    (!this.props.form.size &&  this.props.current === 1) ||
                                    (!this.props.form.type &&  this.props.current === 2) 

                                  }
                                  onClick={()=>{this.props.changeCurrent(this.props.current + 1)}}
                                 style={{  width: "150px", fontSize:"14px"}}>Siguente</Button>}
                  
               
          
              </Item>

            </AlignEnd>
            </NavContent>
            </Nav> 



       

</NavsContainer>



            {this.state.sidebar && <Sidebar/>}
          </React.Fragment>}

          </ModalsContext.Consumer>
    
      );
  }
}

export default withRouter(Header)



//width: "100%";