import React, {useContext, useState} from 'react';

import styled, {createGlobalStyle} from 'styled-components';
import { Button, PlayIcon, ArrowIcon} from 'Components/Primitives/Button';
import Typography from 'local_npm/react-styled-typography';

import Footer from "Components/Molecules/Footer";
import BaselineClose from "react-md-icon/dist/BaselineClose";




import {Container, Img as RealImg} from 'local_npm/react-container';


const SentMessage = ({close}) => {



return(
  <React.Fragment>
  <Container 
  row 
  margin="0 auto" 
  css="margin-top:200px; padding:0rem 3rem; border-radius:5px; position: relative;">

    <RealImg
      width="33%"
      height="372px"  
      src={require("static/pictures/Primeros pasos/Primeros pasos 2.png")}
      img={{
      css:"height:90%; width: auto;",
      tabletCSS:"height:auto; width: 90%;"
      }}  
      tabletCSS={` 
      width:100%; 
      height:auto;
      `}
      overflowHidden/>

    <Container
      center 
      width="66%" 
      tabletCSS={`width:100%;`}>

    <Typography 
      fontSize="70px"
      css="line-height:80px;"
      tabletCSS="width:100%; font-size:45px; line-height:55px;"
      weight="500"

      marginB="20"
      css={`width:80%;`}
      color="#000">Enviado.
    </Typography>

    <Typography 
      fontSize="20px"
      weight="400"
      marginB="10px"
      css={`
      width:80%; 
      line-height:30px;
      `}
      tabletCSS="width:100%; font-size:16px; line-height:26px;" 
      color="#000"> 
      Tu formulario fue enviado con éxito, en aproximadamente 24 horas te enviaremos una respuesta vía email, pero es probable que también alguien de nuestro equipo te haga una pequeña llamada.
    <br/><br/><br/>
    <Button 
      onClick={()=>{close()}}
      transparent 
      style={{
      border:"none", 
      marginLeft:"-1rem"}}>
    <Container 
      center 
      row 
      style={{
      fontWeight:"500"
      }}>

    <Typography 
      weight="500" 
      align="left" 
      marginR="10"> Volver al inicio 
    </Typography>
    <ArrowIcon/>  
    </Container>
    </Button>
    </Typography>
    </Container>
  </Container>
</React.Fragment>

);
}

export default SentMessage