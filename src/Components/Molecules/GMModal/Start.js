import React, {useContext, useState} from 'react';

import { Button, PlayIcon, ArrowIcon} from 'Components/Primitives/Button';
import Typography from 'local_npm/react-styled-typography';


import {Container, Img as RealImg} from 'local_npm/react-container';




const StartMessage = ({changeCurrent}) => {



  return(


    <React.Fragment>

<Container 
      row 
       
      margin="0 auto" 
      css="margin-top:200px; padding: 0 3rem; border-radius:5px; position: relative;">

     <RealImg
      width="33%"
      height="372px"  
      src={require("static/pictures/Primeros pasos/Primeros pasos 1.png")}
      img={{
        css:"height:90%; width: auto;",
        tabletCSS:"height:auto; width: 90%;"

    }}  
      tabletCSS={`
            ${"width:100%; height:auto;"}
          `}
      overflowHidden/>

      <Container
        center 
        width="66%" 
        tabletCSS={`
            ${"width:100%;"}
          `}>


        <Typography 
          fontSize="70px"
          
          weight="500"
          marginB="20"
          css={`width:80%;`}
          tabletCSS="width:100%; font-size:45px; line-height:55px;"
          color="#000">Primeros pasos.</Typography>

        <Typography 
          fontSize="20px"
      
          weight="400"
          marginB="10px"
          css={`width:80%; line-height:30px;`}
          tabletCSS="width:100%; font-size:16px; line-height:26px;"
          color="#000"> 

              Responde el siguiente formulario para entender mas acerca de tus necesidades, pero si no tienes ganas, puedes llamarnos o enviarnos un email. En nuestra sección de contacto los encontraras.
            <br/><br/><br/>
            <Button 

            onClick={()=>{changeCurrent(0)}}

            transparent style={{border:"none", marginLeft:"-1rem"}}>

              <Container 
                center 
                row 
                style={{fontWeight:"500"}}>

                <Typography 
                  weight="500" 
                  align="left" 
                  marginR="10"> Iniciar formulario </Typography><ArrowIcon/>  

              </Container>

            </Button>
          </Typography>

      </Container>

   
    </Container>


</React.Fragment>

);
}

export default StartMessage;