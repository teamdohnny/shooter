import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import { Redirect, Link,BrowserRouter, Route, Switch, withRouter} from 'react-router-dom';
//import MorePopover from "./MorePopover"
import HelpIcon from "react-md-icon/dist/RoundHelpOutline";
import { Button } from 'Components/Primitives/Button';
const _ = require('lodash');
 
 const Nav = styled.nav`
  position: relative;
  overflow: hidden;
  background: white;
  border: 1px solid #DADCE0;
  border-radius:5px;
  overflow: hidden;
  width: ${props=>100*props.size}px;
  height: 40px;
  margin: 0 auto;  
`;
 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent}
    width: ${props=>100*props.size}px;
`;

 const Item = styled.div`
    position: relative; 
    height: 40px;
    border-left: 1px solid #DADCE0;
    cursor: pointer;
    width: 100px;
   
    background:${props=> props.active?"#0059FA":"transparent" };
    ${props=>props.theme.utils.centerContent()}
    a,&{
        text-decoration: none;
         color:${props=> props.active?"white":"#5F6368" };
 
       font-weight: 500;
    }


    ${props=>props.noborder && "border: none;"}
  
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 700px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}

    ${props=>props.hide && `
        @media (max-width: 700px)
        {
          display: none;
        }
    `}

    
`;

 const Separator = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;

const Icon = styled.i`
  vertical-align: bottom;
`;



class Header extends Component
{
  
  render() {
    var tab = this.props.location.pathname
    if(tab === "/about/")
      tab = "/about"
    
    return (  
    <React.Fragment>  
          <Nav size={this.props.data.length}>
          <NavContent size={this.props.data.length}>
            
          {this.props.data.map((item, key)=>{


            return ( <Item
              key={key}
            noborder={!key=== 1} 
            margin 
            active = {tab===item.active}
            style={{ 
              fontSize:"16px", 
            }}>

            <Link to={item.url}>
            {item.name}
            </Link>

            </Item>)
          })

    
          }


            </NavContent>
            </Nav>  
          </React.Fragment>
      );
  }
}

export default withRouter(Header)

