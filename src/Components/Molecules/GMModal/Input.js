import React, { Component } from 'react';
import styled from 'styled-components';

const _ = require('lodash');

export const SelectStyles = styled.select`
    padding: 0 1rem;
    
    font-size: 18px;
    margin: 0.5rem auto;
    width: 100%;
    height: 50px;
    border-radius: 5px;
    position: relative;
    background: white;
    color: black;
    border: 1px solid #E5E5E5;
     &:focus {
    outline: none !important;
    border:1.5px solid ${props=>props.theme.colors.green};
    
    }
  
`;


const InputStyles = styled.input`
    padding:  1rem;
 
    font-size: 16px;
    width: 100%;
    height: 50px;
    border-radius: 5px 5px 0 0;
    position: relative;
    background: #F8F9FB;
    color: #212121;
    border: none;
    border-bottom: 1px solid #E5E5E5;  
    transition: 0.2s;
    &:focus {
    padding-top: 2.2rem;
    padding-bottom: 1.3rem;
    outline: none !important;
    border-bottom:1.5px solid ${props=>props.theme.colors.green};
    
    }
    margin: 0.5rem auto;

 
`;
const Body = styled.div`
  
    position: relative;
  width: 100%;
   margin: 0 auto;
`;
const Tag = styled.label`
    position: absolute;
    color:${props=>props.theme.colors.green};

    padding:0 0.1rem;
    border-radius:2px;
    font-size: 12px;
    font-weight: 500;
    top: 0.8rem;
    left: 0.71rem;
`;
export class Input extends Component{
  constructor(){
    super();
    this.state = {
      focus: false,
      value: ""
    }
  }
  
  
  render(){
      
    return (
    <Body style={this.props.style}>
        <InputStyles 

            onBlur = {()=>{this.setState({focus: false})}}
            onFocus = {()=>{
            this.setState({focus: true});
        }}
        type={this.props.type || "text" }
        maxlength={this.props.maxlength}
        onChange1={(e)=>{this.setState({value:e.target.value})}}
        onChange={(e)=>{this.props.onChange(e.target.value)}}
        value = {this.props.value} 
        placeholder = {this.props.placeholder}
        /> 
    {(this.state.focus || this.props.value) && <Tag>{this.props.placeholder}</Tag>}
    
    </Body>)
      
  }
    
}
 

export const Select = (props)=>(

<Body>
    <SelectStyles
    value={props.value}
    onChange={(e)=>{props.onChange(e.target.value)}}
   > 
       {props.options && props.options.map((item, id)=>(<option 
       key={id}
       value={item.value}>{item.title}</option>))}
    </SelectStyles> 
    {/*props.value && <Tag>{props.placeholder}</Tag>*/}
</Body>)