import React, {useContext} from 'react';
import styled from 'styled-components';
import Footer from "Components/Molecules/Footer";

import OutlineArrowForward from "react-md-icon/dist/OutlineArrowForward";
import {ModalsContext} from "App"


const _ = require("lodash");

export const ImgContainer = styled.div`
  width: 100%;
`;

export const Img = styled.img`
  width: 100%;
  display: inline-block;
`;

export const CardWrapper = styled.div`
  width: ${(100/3)}%;
  padding: 1rem;
  text-decoration: none;
  
    ${props=>props.theme.media.tablet`
    width: 100%;
    margin-top: 1.5rem;
    padding: 1rem 0;
 
 
  
  `}
`;


export const Card = styled.div`
  text-decoration: none;
  position: relative;
  width: 100%
  height: 300px;
  text-align: center;
  border-radius: 5px;
  border: 1px #E3E4E7 solid;
  padding: 1rem 0;
  background: white;
  overflow: hidden;
  &:hover{border: 1px #0059FA solid;}

      ${props=>props.theme.media.tablet`
    width: 100%;
    padding: 2rem;
    height: auto;
    min-height: 270px;
  `}

`;


export const CardTitle = styled.div`
  font-size: 28px;
  color: #202124;
  font-weight: 500;
  width: 80%;
  margin: 1rem auto;
  line-height: 38px;
  text-align: left;
     ${props=>props.theme.media.tablet`
         margin: 0;
      width: 100%;
       font-size: 30px;
    `}
`;

export const CardSubtitle = styled.div`
 font-size: 15px; 
 color: #677282;
  width: 80%;
  text-align: left;
  margin: 0rem auto;
  line-height: 23px;
  font-weight: 400;
  ${props=>props.theme.media.tablet`
      width: 100%;
    `}
`;




export const FingerBanner = styled.div`
  width: 95%;
  left: 0 ;
  position: absolute;
  bottom: 2rem;
  height: 70px;
  border-radius: 0 30px 30px 0;
  background: white;



`;


export const FingerBannerItem = styled.div`
  height: 70px;
  ${props=>props.theme.utils.centerContent()}
`;

export const FingerBannerTitle = styled.div`
  font-size: 25px;
  color: #202124;
  font-weight: 500;
 
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;
`;

 const Row = styled.div`
  width: 70%;
  margin: 0 auto;
  ${props=>props.theme.utils.rowContent()}
`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;

const CardWithImage = () => {

  const modal = useContext(ModalsContext);

  return (
    <React.Fragment>
    <CardWrapper>
     <Card 
      style={{padding:0, display: "inline-block"}} 
      onClick={()=>{modal.openMail()}}>

      <ImgContainer>
        <Img src={require("static/imgs/Primeros-pasos.png")}/>
      </ImgContainer>
      
      <FingerBanner>
        <Row>
          <FingerBannerItem>
            <FingerBannerTitle>Primeros pasos</FingerBannerTitle>
          </FingerBannerItem>
          <FingerBannerItem>
            <OutlineArrowForward  style={{color:"#0059FA", fontSize:"28px"}}/>
          </FingerBannerItem>
      </Row>
      </FingerBanner>
    </Card>
    </CardWrapper>
    </React.Fragment>
  )}

export default CardWithImage;