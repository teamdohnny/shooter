import React, {useState, useContext} from "react"
import {Container, Text} from 'local_npm/clay-components';
import OutlineMoreVert from "react-md-icon/dist/OutlineMoreVert"
import Dropdown from "Components/Primitives/Dropdown"
import IconEdit from "react-md-icon/dist/OutlineEdit";
import IconRemove from "react-md-icon/dist/OutlineDelete";
import {Link} from "react-router-dom"
import {ModalContexts} from "Components/Providers/Modals"
import firebase from 'firebase';









const Item = (props)=>{

   return <Container
              center 
              height="40px"
              css={`color: #80868B;   padding: 0 1rem; text-align:center;
                &:hover{
                  background:#F5F5F5;
                  color: black;
                }
              `}
              style={{fontWeight:"500"}}>


          <Container row>
            {props.icon}
          <Container 
            width="80%">
            
            <Text 
            weight="500"
            fontSize="14px"
            weight="600" 
            align="left" 
            marginL="10"
            > 
            {props.title}
            </Text> 

          </Container>
          </Container>
          </Container>



}








const Options = (props)=> {

const {openModal, closeModal} = useContext(ModalContexts)






const deleteItem = ()=>{

 firebase.database().ref(`shooter/siac/data/${props.id}`).remove().then(()=>{


window.location.reload()

 })


  
}


const openConfirmationModal = ()=>{


openModal(<Container fullscreen center>
<Container fullscreen onClick={closeModal}/>

<Container height="180px" width="330px" defaultCorner defaultShadow 
position="relative"
background="white" css="padding: 2rem; ">
  
  <Text marginT="150px"  weight="500" fontSize="22px">
  Borrar post

  </Text>


   <Text  weight="400" align="left" marginT="10">
   Esta acción eliminara la publicación de su sitio.

  </Text>

 

  <Container row width="50%" css="position: absolute; bottom: 2rem; right:2rem;">
  <Container width="50%" onClick={closeModal} css="cursor:pointer;">
Cancelar
</Container>
<Container width="50%" css="cursor:pointer;" onClick={deleteItem}>
<b>Borrar</b>
</Container>

  </Container>
</Container>



  </Container>

  )




}





  return (<Dropdown

          elements={



       <Container 
          center={false}   
          height="80px">



<Link to={ props.url} style={{textDecoration: "none"}}>






 
              <Item  
                title="Editar"  
                icon={<IconEdit style={{fontSize:"22px"}} />}

                />
    



       
        </Link>

         <Container
         onClick={openConfirmationModal}>



             <Item  
                title="Eliminar"  
                icon={<IconRemove style={{fontSize:"22px"}} />}

                />



        </Container>
        
        
      </Container>
    }
              onClick={(e)=>{
                  e.preventDefault();
                }}
             
               css="z-index:99999999999; padding:10px 0;"
            background="white" 
            corner="10px"


                background="white" 
              
              shadow={"0px 0.5px 6px rgba(0,0,0,0.4)"}
                position="absolute" 
                distance="auto auto -80px -110px" 
                height="100px" 
                width="160px">


            <b><OutlineMoreVert className="myIcons"/> 

            </b> 


            </Dropdown>
      )
}

export default Options;

// <Text fontSize="14px" weight="300" color="#5F6368">{props.subtitle}</Text>


