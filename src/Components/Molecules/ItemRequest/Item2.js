import React, {useState, useContext} from "react"
import {Container, Text} from 'local_npm/clay-components';
import styled from 'styled-components';
import BaselineMoreVert from "react-md-icon/dist/BaselineMoreVert"
import BaselineCheckBoxOutlineBlank from "react-md-icon/dist/BaselineCheckBoxOutlineBlank"
import BaselineCheckBox from "react-md-icon/dist/BaselineCheckBox"
import Options from "./Options"
import SharpArrowBack from "react-md-icon/dist/SharpAdd";
import BaselineTrendingUp from "react-md-icon/dist/BaselineTrendingUp";

import {Link} from "react-router-dom"
import moment from "moment"

const Title = {
 "story": "Historias de éxito",
  "blog": "SIAC Blog",
}



const NAVBARHEIGHT = "180px"

const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
   
    
     white-space: nowrap;
`;

export const AlignStart = styled(_Section)`
    justify-content: flex-start;
   width: 50%;
`;


export  const AlignEnd = styled(_Section)`
    justify-content: flex-end;
       width: 50%;
      
`;



/*

export const AlignCenter = styled(_Section)`
    justify-content: flex-center;
   width: 30%;
`;


<AlingNav direction="start">
<AlingNav>

<AlingNav direction="center">
<AlingNav>

<AlingNav direction="end">
<AlingNav>


<ItemNav active>

<TabNav active>



*/


const Item = (props)=>{
  return (<Container
        width="auto"
        {...props}
        center
        height={"60px"}>
        {props.children}
      </Container>)
}


const Nav = (props)=> {
  const {children, top="0"} = props

  return(<Container
          row
        
          

          {...props}
          width = "100%"
         
          >
          {children}
        </Container>)

        /*
              position= "fixed"
              distance ={ `${top} 0 auto 0`}

        */
}


const ItemRequest = (props)=> {

  const {children, top="0"} = props

  return (
    <Container
      overflowHidden
      background="white"
      margin="2rem 0"
      overflow="hidden"
     
      corner="10px"
      border="#DADCE0 1px solid"

      css={`

       

        min-height:${(180 + 64)}px;

        
        position: relative;
        color: black;
        transition: 0.4s;
        cursor:pointer; 
        .myIcons{       
          font-size:26px; 
          color:#BDC1C6;
        } 
        &:hover{
          
          box-shadow:0 0.5px 5px rgba(0,0,0,0.3);}`}



      tabletCSS="height: auto;"

      >


      <Link style={{textDecoration:"none"}} to={`/open/editor/${props.item.type || "blog"}/${props.item.id}`}>

      <Container row css="padding: 0 1rem; ">



  
      <Container tabletCSS="display: none;" width="30%"  css={`padding:2rem 1rem;

          min-height:${NAVBARHEIGHT};

        `}>

      <Container backgroundImg={`"${props.cover}"`} height="100%" >






     
      </Container>

      </Container>

     
        
   <Container 
            width="70%" 
            position="relative"
            css={`padding:2rem 1rem; 

          min-height:${NAVBARHEIGHT};

        `}
             
            tabletCSS="width: 100%;">

          
<Container  css=" padding-bottom: 1rem; padding-right:10%; " >
            <Text 

              fontSize="20px" 
              weight="500" 
              color="black"
              marginB="10" 
             >{props.titulo}</Text>
           
          <Text fontSize="14px" weight="300" color="#5F6368" marginB="20">{props.descripcion} </Text>
          </Container>

      <Container css={`

        position: absolute;
        bottom:2rem;
        left: 1rem;


        `} >
        <Text height="100%" fontSize="12px" weight="300" color="#BDC1C6">En {Title[props.item.type]}, {moment(props.item.created).locale("es").fromNow() } </Text>
      </Container>

           
      </Container> 
   
      </Container> 


        </Link> 
      


          <Container background="#F9F9F9" height="64px" row css="padding: 0 2rem;" >
          <AlignStart>



          

           <Container width="auto" center height="64px"  css="padding-right: 1rem;">
            <Text>
              
              <BaselineTrendingUp style={{fontSize:"24px"}}/>
            </Text>

          </Container>

             <Container width="auto" center height="64px" css="padding-right: 1rem;">
            <Text fontSize="20px" weight="500">
              
              {props.item.count || 0}
            </Text>

          </Container>
          <Container width="auto" center height="64px">
            <Text fontSize="14px" color="#5F6368">
              
              Vistas
            </Text>

          </Container>
             </AlignStart>



          

          </Container>

 </Container>

  )
}

export default ItemRequest;

// <Text fontSize="14px" weight="300" color="#5F6368">{props.subtitle}</Text>