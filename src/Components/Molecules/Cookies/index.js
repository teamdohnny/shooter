import React, { Fragment } from 'react';
import { Container, Text, Button } from './elements';

class Cookies extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			show: this.getLocalStorage(),
		}
	}

	handleClick() {
		this.setState({show: true});
	}

	componentDidUpdate(props, state) {
		if(state.show !== this.state.show) {
			localStorage.setItem('agreed', this.state.show)
		}
	}

	getLocalStorage() {
		return localStorage.agreed;
	}
	render() {
		return(
			<Fragment>
				{
					(this.state.show === 'false') || (this.state.show === undefined) &&
					<Container>
						<Text>This website uses cookies to store information on your device. By visiting this website you agree to the placement of these cookies.</Text>
						<Button onClick={this.handleClick.bind(this)} >I Agree</Button>
					</Container>
				}
			</Fragment>
		)
	}
}

export default Cookies;
