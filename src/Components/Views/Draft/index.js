import React, {useState, useContext, useEffect} from 'react';
import styled from 'styled-components';
import Navbar from "Components/Molecules/Navbar"

import ItemList from "./ItemList";

import {Button,ClayButton} from "Components/Primitives/Button";
import {Container, Text} from 'local_npm/clay-components';
import SharpArrowBack from "react-md-icon/dist/SharpAdd";
import {Link} from "react-router-dom"

import Options from "./Sidebar/Options"
import firebase from 'firebase';
import Rebase from "re-base";

const base = Rebase.createClass(firebase.database());

const ContainerStyle = `
  text-align: center;
  border-radius:10px;
`;


const Tag = ({children, active=false, onClick}) => {



  return <Container
  onClick={onClick} 
          height="18px"
          margin="15px auto"
          css={`
            cursor:pointer;
            font-size: 14px;
            padding: 0rem 1rem;
            width: 150px;
            text-align: left; 
            `}>
 <Text  color={active?"#212121":"#80868B"}>
    {children}
    </Text>
          

          {active && <Container 
            width="3px"
            background="#212121"
            css={` 
              position: absolute;
              left: 0;
              top:0;
              bottom:0;
              border-radius: 0 20px 20px 0px; 
            `}/>
          }
</Container>
}



const Sidebar = ({status, setType, type}) => {
  return ( <Container

    tabletCSS="display: none;"
   css={` 
      width: 25%;
      position: fixed;
      left:0;
      top: 130px;
      text-align: center;
    `}>


    <Container width={"150px"} margin="0 auto">

      <Options/>
      </Container>





<Container width="40%" margin="1rem auto">

  <Tag active={type==="story"} onClick={()=>{setType("story")}}>
    Historias de éxito
  </Tag>

  <Tag active={type==="blog"} onClick={()=>{setType("blog")}}> 
    SIAC Blog
  </Tag>
</Container>

  </Container>
)
}

  

const Workspace =  ({match}) => {

  const [items, setItems] = useState([])
  const status = match.params.status || "published"
  const [type, setType] = useState("story")
  const [loading, setLoading] = useState(true)


    useEffect( ()=>{

      setLoading(true)
    //const types = ["blog", "stories", "email", "modals"]


     base.fetch('shooter/siac/data',{
      asArray: true,
      queries: {orderByChild: 'status', equalTo: `draft`,
        limitToLast: 20}
     }).then((data)=>{

          setItems(data);
         // console.log("Status:", status,data, data[0]["&created_status"])
          setLoading(false)
         
        });


    }, [type, status, match])

  





  return(
  <React.Fragment>
   <Navbar hideMiddle goToRoot subtitle="Borradores."/>


     <Container
     	  row
      	width="50%"
        margin="60px auto" 
        style={{
          borderRadius:"0px 0px 10px 10px", 
          paddingBottom:"4rem"
        }} 
        css={ContainerStyle}
        tabletCSS="width: 90%;"
        >


        
    {loading?
        <Container center height="300px">
              <h3>Cargando..</h3>
        </Container>:

        <ItemList 
          items={items.sort((a, b) => {
          if(a["created"] < b["created"]) return -1;
          if(a["created"] > b["created"]) return 1;
            return 0;
          }).reverse()} 
          title={"Borradores."} />}


        
      </Container>
  </React.Fragment>
  )
}
export default Workspace;

