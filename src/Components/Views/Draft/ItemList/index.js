import React, {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import Navbar from "Components/Molecules/Navbar"
import {Container, Text} from 'local_npm/clay-components';
import {Img} from 'local_npm/react-container';
import ItemRequest from "Components/Molecules/ItemRequest/Item";
import Filter from "Components/Molecules/Filter"
const _ = require('lodash');

const Title =  {
  "story":"Historias de éxito.",
  "blog":"SIAC blog."
}

const Topic = ({ 
  title="", 
  subtitle="", 
  items=[] 
    }) => {


  return(
  <React.Fragment>
   <Navbar />
     <Container margin="50px auto">
        <Container
          css="border-bottom: 1px solid black; margin-bottom:30px;">

        <Text 
          fontSize="40px" 
          weight="500" 
          marginT="10" 
          marginB="10">
        {title}
        </Text>

        </Container>


        {_.map(items,(item, key)=>{
           return  <ItemRequest {...item.data} item={item} key={key} />

        })}



        
      </Container>
      
  </React.Fragment>
  )
}

export default Topic;