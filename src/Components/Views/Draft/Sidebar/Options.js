import React, {useState, useContext} from "react"
import {Container, Text} from 'local_npm/clay-components';
import OutlineMoreVert from "react-md-icon/dist/OutlineMoreVert"
import Dropdown from "Components/Primitives/Dropdown"
import IconEdit from "react-md-icon/dist/RoundWhatshot";
import IconRemove from "react-md-icon/dist/OutlineDelete";
import {Link} from "react-router-dom"
import {ModalContexts} from "Components/Providers/Modals"
import firebase from 'firebase';
import SharpArrowBack from "react-md-icon/dist/SharpAdd";
import {Button,ClayButton} from "Components/Primitives/Button";


const Item = (props)=>{

   return <Container
              center 
              height="60px"
              css={`color: #80868B;   padding: 0 1rem; text-align:center;
                &:hover{
                  background:#F5F5F5;
                  color: black;
                }
              `}
              style={{fontWeight:"500"}}>


          <Container row>
            {props.icon}
          <Container 
            width="80%">
            
            <Text 
            weight="500"
            fontSize="14px"
            weight="600" 
            align="left" 
            marginL="10"
            > 
            {props.title}
            </Text> 

          </Container>
          </Container>
          </Container>



}




const Options = (props)=> {

  const Elements = ()=>{ 


  return   <Container 
            center={false}   
            height="120px">



          <Link to={"/editor/story"} style={{textDecoration: "none"}}>
              <Item  
                title="Historia de éxito"  
                icon={<IconEdit style={{fontSize:"22px"}} />}

                />
          </Link>



             <Link to={"/editor/blog"} style={{textDecoration: "none"}}>
              <Item  
                title="Post en blog"  
                icon={<IconRemove style={{fontSize:"22px"}} />}

                />
          </Link>







  <a>
           <Container
          
               center
              row 
              height= "40px"
                css={`color: #80868B;   padding: 0 1rem; text-align:left;
                &:hover{
                  background:#F5F5F5;
                  color: black;
                }


              `}
              style={{fontWeight:"500"}}>


           
  <Container  width="80%" >
            <Text 
              fontSize="14px"
              weight="500" 
              align="left"
              marginL="10"
              weight="600"
              > 
               
            </Text>

               </Container> 

          </Container>
          </a>
          
        </Container>

  }



  return (<Dropdown
            elements={<Elements/>}
            css="z-index:2; padding:10px 0;"
            background="white" 
            corner="10px"
            shadow={"0px 0.5px 6px rgba(0,0,0,0.4)"}
            position="absolute" 
            distance="auto auto -150px auto" 
            height="140px"

            width="210px">
            <b>
            

    <Button transparent css="&:focus {outline: none; }">
    <Container 
      center 
      row 
      defaultShadow
      css="color: white; padding: 0.6rem 2rem;"
      background="#212121"
      defaultShadow
      corner= "27px"

      style={{fontWeight:"500"}}>
      <SharpArrowBack style={{fontSize:"22px"}}  />
      
      <Text 
        fontSize="14px"
        weight="500" 
        align="left" 
        marginL="10"> 
          Nuevo
        </Text> 
    
        </Container>
      </Button>
            </b>
          </Dropdown>
      )
}

export default Options;



