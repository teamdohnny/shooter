import React, {Component, Fragment} from 'react';
import Navbar from 'Components/Molecules/Navbar';
//import Footer from 'Components/Molecules/Footer';




import { Content, Section } from './elements'
class Terms extends Component {
	componentDidMount() {
		window.scrollTo(0, 0)
	}

	render() {
		return(
			<Fragment>
				<Navbar/>
				<div>
					<Content>
						<div>
						<Section>
			 <h1> Terms and conditions </h1>

			Thanks for using Dohnny;
						Dohnny ("us") values ​​the trust you ("user") have in us (Dohnny) when you use (you /user) Dohnny.com. If you are using our Service on behalf of an organization or entity ("Organization"), you are accepting these Terms in name of that Organization and represents and warrants that it has the authority to bind the Organization to these Terms. In that case, "you" and "your" refers to that Organization. If you become a subscriber to Dohnny's service, your use of the
						Service will be governed by our Subscription Agreement.Read this Agreement of Terms and Conditions carefully before accessing or using Dohnny. Because it is a contract So important between us and our users, we have tried to make it as clear as possible.

			<h3> Index: </h3>
			    <ul>
			    <li> Definitions </li>
			    <li> Description of services </li>
			    <li> Terms of the account </li>
			    <li> Acceptable use </li>
			    <li> User generated content </li>
			    <li> Intellectual property notice </li>
			    <li> Payments </li>
			    <li> Guarantees </li>
			    <li> Changes to these terms and conditions </li>
			    </ul>

			<h2> Definitions </h2>

			<ol>

			<li> The "Agreement" refers, collectively, to all the terms, conditions, notices contained or referenced in this
			document (the "Terms and Conditions" or the "Terms") and all other operational rules, policies (including
				Dohnny's Privacy Statement, and the procedures we may post from time to time on the website. </li>

			<li> The "Service" refers to the applications, software, products and services provided by Dohnny. </li>

			<li> The "website" refers to Dohnny's website located on dohnny.com, and to all content, services and products
			provided by Dohnny on the website or through it. It also refers to the subdomains owned by Dohnny
			from dohnny.com </li>

			<li> "The User", "You" and "Your" refer to the person, company or individual organization you visited or are using
			the Website or the Service; who accesses or uses any part of the account; or who directs the use of the account in performance
			of its functions. A user must be at least 13 years old. Special terms may apply for accounts
			commercial or governmental. </li>

			<li> "Dohnny", "We" and "Us" refer to Dohnny, as well as our affiliates, directors, subsidiaries, contractors,
			licensors, officials, agents and employees. Also to our pet. </li>

			<li> "Content" refers to the content presented or displayed through the website, which includes, among others, text, data,
			articles, images, photographs, graphics, software, applications, designs, features and other available materials
			on the website or available through the website. Service. "Content" also includes services. The "Generated content
			by the user "is the content, written or not, created or uploaded by our users." Its content "is content that
			you create or own. </li>


			</ol>


			<h2> Description of services </h2>

			Dohnny offers a service and tools that facilitate the development and design of web applications that use certain
			technologies and certain auxiliary applications, analysis, documentation and services for the client, all as described
			particularly on dohnny.com (the "Service"), all within a development environment.

			    <ol>


			<li> Free Service </li>
			  
			<p> You can use all of Dohnny's services totally free in your first project. You will not be able to erase and redo a new
			project to continue using the free version. The size of the free project created will be limited. </p>
			 
			<li> Premium Service </li>
			  
			<p> You will continue to use all of Dohnny's services in your second project which has a cost, henceforth you will pay for
			project created, depending on the project size will be the price to pay, also influences team members.
			Your project is private. </p>

			<li> Business service </li>

			<p> If you have a software company and you are interested in using Dohnny's services, contact us and we will
			we license the use of technology at a more accessible and convenient cost for you. </p>


			<li> Your obligations </li>

			<ul>
			<li> You must provide accurate information when creating your Dohnny account. </li>
			<li> You are responsible for protecting the password and all activities that occur in your account.
			      You must notify Dohnny </li>
			<li> immediately if you become aware of any breach of security or unauthorized use of your account. </li>
			<li> You must comply with our Acceptable Use Policy at all times when using the Service. </li>
			<li> You may not disassemble, decompile or reverse engineer the Service or attempt or assist another person to
			      to do so, unless such restriction is prohibited by law. </li>

			In the remainder of this document of terms and conditions the other obligations are reflected by you.
			</ul>
					<li> Right to eliminate </li>

			<p> Dohnny has the right, in its sole discretion, to remove or block any text, image, design,
			technology and other content, data, information, materials and other elements provided or placed
			Dohnny's disposition or stored or loaded into the Service by the Customer ("Customer Materials")
			at any time when (a) said Customer Materials violate laws, regulations, orders
			or violate Dohnny's applicable policies and procedures, including, but not limited to, policies
			of acceptable use; (b) the removal or blocking is necessary due to extreme circumstances or for
			protect the security, protection or integrity of the Service, Dohnny or any third party; or (c) for
			respond to the application of the law or any other governmental authority. </p>

			<li> Availability of services </li>

			<p> Dohnny will use commercially reasonable efforts to make the Service available to the Customer twenty-four
			hours of the day, seven days a week, three hundred and sixty-five days a year, except for certain services and
			scheduled maintenance or in case of emergency or force majeure events. Notwithstanding the above, Dohnny will not be
			responsible for any downtime or non-compliance with said service availability objectives. Dohnny
			will make good faith efforts to perform service and maintenance of the Service outside of peak hours of use. He
			The client acknowledges that the availability of the service may be affected by: (i) the activity or capacity of the
			telecommunications; (ii) hardware failures; and /or (iii) compatibility with third-party communication equipment, software
			of Internet access and /or browsers that do not comply with the requirements of the Service. Dohnny denies any responsibility
			for any interruption of the service in relation to said activity, capacity, failure and /or compatibility.
			      The client is responsible for providing all the telecommunications equipment and services necessary to access
			service. </p>

			<li> Modifications to the service </li>

			<p> Dohnny can change the Service (content, appearance, design, functionality and all other aspects), procedures of
			access, tools, web technologies, documentation, format requirements, communication protocols and services
			offered at any time and for any reason. </p>

			<li> Customer service and technical support </li>
			  
			<p> Dohnny will provide Customer Customer support for the Service through our online community forums and
			support portal. Dohnny will also provide Customer with reasonable customer service by mail
			electronic mail during Dohnny's business hours. Dohnny does not guarantee that the support is available or that Dohnny
			will respond within a certain period or that Dohnny will make the Service work for the Client's purposes,
			in the Customer's system or solve all the problems related to it. </p>


			  </ol>

			<h2> Terms to create an account </h2>

			  A human must create his account; you must be 13 or older; you must provide an email address
			  valid; and you can not have more than one free account. You are solely responsible for your account and everything that
			  happen while you are registered or using your account. You are responsible for keeping your account secure.

			    <ol>
			  
			  <li> Required information </li>
			  
			  <p> You must provide a valid email address to complete the registration process. Any other
			  requested information, such as your real name, is optional, unless you accept these terms on behalf of an entity
			  legal (in which case we need more information about the legal entity) or if you opt for a paid account, in which you married
			  Additional information will be necessary for billing purposes.
			  You can create an account directly using your social networks. </p>

			  <li> Account requirements </li>
			  
			  Simple rules for accounts in the Dohnny Service.
			  
			    <ul>

			  <li> Must be human to create an account. Accounts registered by "bots" or other automatic methods are not
			  allowed. A person or legal entity can maintain no more than one free account. </li>

			  <li> You must be 13 years old or older. We must comply with the laws of the United Mexican States. Dohnny does not direct
			  Our Service to children under 13 years of age, and we do not allow any User under the age of 13 in our Service.
			  If we know of any User under the age of 13, we will cancel that User's account immediately. If you are
			  resident of a country outside of Mexico, the minimum age of your country may be higher; In such a case, you are
			  responsible for complying with the laws of your country. </li>

			  <li> Your login can only be used by one person, that is, a single login may not be
			  shared by several people. A paid organization account can create separate logins for
			  as many users as your subscription allows. </li>

			    </ul>

			  <li> User account security </li>
			  
			<ul>

			    <li> You are responsible for keeping your account secure while using our Service. We offer tools such as
			    two-factor authentication to help you maintain the security of your account, but the content of your account and
			    Your safety depends on you. </li>
			    <li> You are responsible for all published content and the activity that occurs in your account (even when the
				          content is published by others who have accounts in your account). </li>
				    <li> You are responsible for maintaining the security of your account and password. Dohnny can not and will not be responsible for
				    no loss or damage due to breach of this security obligation </li>

			    <li> You will immediately notify Dohnny if you are aware of any unauthorized use or access to our Service through your
			    account, including unauthorized use of your password or account. </li>

			    </ul>

			  <li> Additional terms </li>

			<p> In some situations, third-party terms may apply to your use of Dohnny. For example, it can be
			member of an organization in Dohnny with its own terms or license agreements; you can download a
			application that integrates with Dohnny; or you can use Dohnny to authenticate yourself in another service. Keep in mind
			that, while these Terms are our entire agreement with you, the terms of other parties govern your relationship with you. </p>

			  </ol>

				<h2> Acceptable use of Dohnny </h2>


			<ol>

			While using the service, you must follow this Acceptable Use Policy, which includes some restrictions on the
			Content that you can post, perform on the service and other limitations. In short, be excellent for each other.

			<li> Compliance with laws </li>

			<p> Your use of the website and the service should not violate any applicable law, including the laws of copyright or trademarks
			commercial, export control laws or other laws in your jurisdiction. You are responsible for making sure that
			Your use of the Service complies with applicable laws and regulations. </p>

			<li> Compliance with laws and regulations </li>
			  
			You agree that under no circumstances will you upload, post, host or transmit any content that:
			  
			  <ul>

			        <li> It is illegal or promotes illegal activities; </li>
			        <li> Is or contains sexually obscene content; </li>
			        <li> Is libelous, defamatory or fraudulent; </li>
			        <li> It is discriminatory or abusive towards any individual or group; </li>
			        <li> Violate any property rights of any party, including patents, trademarks, secrets
			             commercial, copyright, right of publicity or other rights. </li>

			  </ul>

			    <li> Behaviors that are restricted </li>
			  
			    While using Dohnny you agree that under no circumstances:

			  <ul>

			<li> Harass, harass, threaten or incite violence towards any individual or group, including employees,
			officers and agents of Dohnny or other users of Dohnny; </li>
			<li> Attempt to alter or manipulate Dohnny in a way that could damage our website or service. </li>
			<li> Violate the privacy of any third party, such as posting personal information of another person without their prior knowledge;
			Do not be a patan. </li>

			  </ul>


			  <li> Limit of use of our services </li>

			  <p> You can use our services for your personal lucrative purposes, such as selling applications or content that you can
			  generate in Dohnny.
			  But you can not duplicate, reproduce, resell or exploit any part of this service, or charge someone for the use of
			  our free services. Do not be a boor, any business you want to make using Dohnny outside of the allowable,
			  You must communicate it to us. </p>


			<h2> User generated content </h2>

			    You own the content that you create, but you grant us certain rights so that we can display and share the content that you
			    publish when you use the free version. You still have control over your content and your responsibility, and the rights that we
			    grants are limited to those we need to provide the service. We have the right to remove content or close
			    accounts if necessary. Do not be scared, we will do it only if you violate our terms and conditions.
			</ol>
			<ol>

			    <li> Public projects </li>

			<p>
			    When starting to work with Dohnny, the code, images, graphics, animations and everything that generates will be public;
			    This means that we can use it to display it on our websites owned by Dohnny. Do not be scared, in any way
			    We will show private information about you.
			    But the other people inside Dohnny or outside will know that you are responsible for having created that project.


			    This includes the right to do things like copy it into our database and make backup copies; show it to you
			    and to other users; analyze it in a search index or analyze it in our servers; share it with other users;
			    and do it, in case its content is something like music or video.

			    This license does not give Dohnny the right to sell its content or distribute it or use it outside of our provision
			    from service.
			    It may be used for marketing purposes and promotion of Dohnny's services.
			    Similarly, when using Dohnny and the services, the content generated will be used for our training purposes.
			    of Dohnny's algorithms.
			</p>


				 <li> Private projects </li>

			    <p>
			    
			    Dohnny believes that the content of private projects is confidential to you. Dohnny will protect the content of
			    repositories
			    unauthorized use, access or disclosure in the same way we use to protect our own information
			    confidential nature of a similar nature and in no case with a degree of care that is not reasonable.

			    Dohnny employees can only access the content of their private repositories in the following situations:

			    With your consent and knowledge, for support reasons. If Dohnny accesses a private repository for support reasons,
			    We will only do it with the consent and knowledge of the owner.
			    When access is required for security reasons.

			    You can choose to enable additional access to your private projects. For example:

			    You can grant a third-party application authorization to use, access and disseminate the content of your projects
			    private. Your use of third-party applications is at your own risk; Dohnny is not responsible for disclosures to
			    third parties that you authorize to access a private project.

			    If we have reason to believe that the contents of a private repository violate the law or these Terms, we have
			    right to access, review and eliminate them. In addition, we may be required by law to disclose the contents of your
			    private projects.

			    </p>

			    <li> Content ownership, right to publish and license grants </li>

			    <p>
			    You retain ownership and responsibility for the Content that you create or possess ("Your content"). If you publish something that does not
			    created by yourself or that does not belong to you, accepts that you are responsible for any Content that you post; that you will only send
			    Ontenido that you have the right to publish; and that you will fully comply with third-party licenses related to the Content
			    publish
			    We are not responsible if you inflict these laws and create something that is violating any copyright law.
			    </p>
			</ol>


			<h2> Intellectual property notice </h2>

			    The Service or constitutes the intellectual property of Dohnny will remain the exclusive property of Dohnny and its licensors.
			    Any feedback, comment or suggestion that you may provide in relation to the Service is entirely
			    volunteer and we will be free to use such feedback, comments or suggestions as we deem appropriate and without any
			    obligation to you.

			    We own the service and all our content. In order for you to use our content, we grant you certain rights
			    about it, but you can only use our content in the way we allow it.

			    Our brand and logo may only be used for marketing purposes by you, we agree that you are likely to be
			    fall in love with us and we with you, you can use our brand to show off and show off with your colleagues, always and
			    when you do not generate income by selling our brand and logos.
			    Any questions contact us at teamdohnny@gmail.com
			<h2> Payments </h2>

			You are responsible for any fees associated with your use of Dohnny. We are responsible for communicating those rates in a
			clear and precise, and to inform you in advance if these prices change.
			Our prices and payment terms are available at dohnny.com/pricing.

			  <ol>

			<li> Prices </li>

			<p>
			If you accept a subscription price, that will remain your price during the term of the payment term; However, the
			Prices are subject to change at the end of a payment term.
			</p>

			<li> Updates and changes </li>

			<p>
			You can change your service level at any time by selecting a plan option or going to the configuration.
			If you choose to degrade your account, you may lose access to the Content, features or capacity of your account. Check our
			Cancellation section for information on how to obtain a copy of that Content.
			We can update the prices of services suddenly and without prior notice; if you already have a plan
			old, no problem, you respect that plan, but as soon as you finish you will have to adjust to the new prices.
			</p>

			<li> Billing </li>

			    <p> We bill you immediately when you upgrade the free plan to any payment plan. </p>
			    <p> If you upgrade to a higher level of service, we will send you an invoice for the updated plan in less than 24 hours.
			    hours. </p>
			    <p> You can change your service level at any time by selecting a plan option or going to the configuration of
			        billing If you choose to degrade your account, you may lose access to the Content, features or capacity of your account.
			        See our section on Cancellation for information on how to obtain a copy of that Content. </p>

			<li> Authorization </li>

			    <p> By accepting these Terms, you authorize us to charge your credit card, PayPal account or other payment methods
			    approved by the rates you authorize for Dohnny. </p>

			<li> Payment responsibility </li>

			    <p>
			    You are responsible for all fees, including taxes, associated with your use of the Service.
			    By using the Service, you agree to pay Dohnny any charges incurred in connection with your use of the Service.
			    If you dispute the matter, contact Dohnny Support. You are responsible for providing us with a valid means of payment
			    for paid accounts. Free accounts are not necessary to provide payment information.
				          </p>
			<h3> Cancellation and termination </h3>

			You can close your account at any time. If you do so, we will treat your information responsibly.

			<ol>
			    <li> Account cancellation </li>
			    
			  <p> It is your responsibility to properly cancel your account with Dohnny. You can cancel your account at any
			  moment accessing Settings. The account provides a simple cancellation link, without questions.
			  We can not cancel accounts in response to a request by email or phone. </p>

			    <li> What happens when I cancel the account? </li>

			  <p>

			  We will retain and use your information as necessary to comply with our legal obligations, resolve
			  Disputes and enforce our agreements, but barring legal requirements, we will delete your full profile and Content
			  of its repositories within 90 days of cancellation or termination (although some of the information may remain
				        in encrypted backup copies). This information can not be retrieved once your account is canceled.

			  We will not delete Content that has contributed to Dohnny.
			  If you created a free project, we'll keep it.

			  Upon request, we will make a reasonable effort to provide the owner of the account with a copy of your
			  legal content and non-infringing after the cancellation, termination or reduction of the account. Must perform
			  this request within 90 days after cancellation, termination or withdrawal.

			  </p>

			    <li> If I already pay and I want to cancel? </li>

			     
			    <p> We hope this never happens, but if so, if you already made the payment, there are no refunds for any reason. </p>

			    <li> We can finish it </li>

			    <p>
			    We previously mentioned that if you violate any legal law or these terms and conditions, we can close your
			    account without prior notice. Regardless if you already paid, avoid the pain of having to do something like that; I do not know yet
			    bad boy, use Dohnny's services for good.
			    There is no refund if we close your account for the above reasons written. </p>

			</ol>
			<h2> Guarantees </h2>

			    THE SERVICE IS PROVIDED "AS IS" AND "AS AVAILABLE" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
			    INCLUDING BUT NOT LIMITED TO IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT.
			    DOHNNY IS NOT RESPONSIBLE FOR ANY THIRD PARTY CONTENT THAT YOU DOWNLOAD OR OBTAIN OTHERWISE THROUGH THE USE OF THE
			    SERVICE OR FOR ANY DAMAGE OR LOSS OF DATA THAT MAY RESULT. WE DO NOT GUARANTEE, ENDORSE OR ASSUME RESPONSIBILITY
			    FOR ANY APPLICATION OR SERVICE OF THIRD PARTIES THAT PROVIDES ACCESS TO OUR SERVICE.

			    The Service is controlled, operated and hosted from the United Mexican States. We do not make any representation that
			    the Service is appropriate or available for use in other places. Those who access or use the Service from
			    other jurisdictions do so at their own risk and are responsible for compliance with all laws and regulations
			    applicable laws of the United Mexican States and local, including but not limited to export regulations and
			    import.

			<h2> Changes to these terms and conditions </h2>

			    <p> We reserve the right, at our sole discretion, to modify these Terms of Service at any time and
			    We will update these Terms of Service in the case of such modifications. We will notify our Users about
			    substantial changes to this Agreement, such as price changes, at least 30 days before the change enters into
			    validity by posting a notice on our website. For non-material modifications, its continued use
			    of the website constitutes an agreement with our revisions of these Terms of Service.

			    We reserve the right at any time and occasionally modify or interrupt, temporarily or permanently,
			    the website (or any part of it) with or without prior notice. </p>




			 
			   </ol>
							</Section>
						</div>
					</Content>
					{/*<Footer/>*/}
				</div>
			</Fragment>
		)
	}
}

export default Terms;
