import React, {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import Navbar from "Components/Molecules/Navbar"
import {Container, Text} from 'local_npm/clay-components';
import {Img} from 'local_npm/react-container';
import ItemRequest from "Components/Molecules/ItemRequest/Item2";

const _ = require('lodash');

const Title =  {
  "draft":"Borradores.",
  "published":"Publicados."
}

const Topic = ({ 
  title="", 
  subtitle="", 
  items=[] 
    }) => {


  return(
  <React.Fragment>
   <Navbar />
     <Container margin="50px auto">
    

        {_.map(items,(item, key)=>{
           return  <ItemRequest {...item.data} item={item} key={key} />

        })}



        
      </Container>
      
  </React.Fragment>
  )
}

export default Topic;