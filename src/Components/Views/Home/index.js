import React, {useState, useContext, useEffect} from 'react';
import styled from 'styled-components';
import Navbar from "Components/Molecules/Navbar"



import {Button,ClayButton} from "Components/Primitives/Button";
import {Container, Text} from 'local_npm/clay-components';
import SharpArrowBack from "react-md-icon/dist/SharpAdd";
import {Link} from "react-router-dom"


import firebase from 'firebase';
import Rebase from "re-base";

import ItemList from "./ItemList"

const base = Rebase.createClass(firebase.database());

const ContainerStyle = `
  text-align: center;
  border-radius:10px;
`;



  

const Workspace =  ({match}) => {

  const [items, setItems] = useState([])
  const [status, setStatus] = useState("published")
  const [loading, setLoading] = useState(false)

//http://siacpage.herokuapp.com/story/
    useEffect( ()=>{

    
    //const types = ["blog", "stories", "email", "modals"]


     
     base.fetch('shooter/siac/data',{
      asArray: true,
      queries: {orderByChild: 'status', equalTo: `published`,
      limitToLast: 10
      }
     }).then((data)=>{

          setItems(data);
         // console.log("Status:", status,data, data[0]["&created_status"])
          setLoading(false)
         
        });

   

  



    }, [])




  




  return(
  <React.Fragment>
   <Navbar />

     <Container
     	  
      	width="50%"
        margin="100px auto" 
        style={{
          borderRadius:"0px 0px 10px 10px", 
          paddingBottom:"4rem"
        }} 
        css={ContainerStyle}
        tabletCSS="width: 90%;"
        >


          <Container
          css="border-bottom: 1px solid black; margin-bottom:30px;">

        <Text 
          fontSize="40px" 
          weight="500" 
          marginT="10" 
          marginB="10">
        Resumen.
        </Text>
        </Container>

{loading ? <Container height="300px" center background="white">
    <Text fontSize="30px">Cargando...</Text>
  
  </Container>:<ItemList items={items}/>}


        
      </Container>
  </React.Fragment>
  )
}
export default Workspace;

