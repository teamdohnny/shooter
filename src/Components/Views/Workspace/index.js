import React, {useState, useContext, useEffect} from 'react';
import styled from 'styled-components';
import Navbar from "Components/Molecules/Navbar"

import ItemList from "./ItemList";

import {Button,ClayButton} from "Components/Primitives/Button";
import {Container, Text} from 'local_npm/clay-components';
import SharpArrowBack from "react-md-icon/dist/SharpAdd";
import {Link} from "react-router-dom"

import Options from "./Sidebar/Options"
import firebase from 'firebase';
import Rebase from "re-base";

const base = Rebase.createClass(firebase.database());

const ContainerStyle = `
  text-align: center;
  border-radius:10px;
`;


const Tag = ({children, active=false, onClick}) => {

 

  return <Container
  onClick={onClick} 
          height="18px"
          margin="15px auto"
          css={`
            cursor:pointer;
            font-size: 14px;
            padding: 0rem 1rem;
            width: 150px;
            text-align: left; 
            `}>
 <Text  color={active?"#212121":"#80868B"}>
    {children}
    </Text>
          

          {active && <Container 
            width="3px"
            background="#212121"
            css={` 
              position: absolute;
              left: 0;
              top:0;
              bottom:0;
              border-radius: 0 20px 20px 0px; 
            `}/>
          }

  </Container>
}










const Sidebar = ({status, setType, type}) => {
  return ( <Container

    tabletCSS="display: none;"
   css={` 
      width: 25%;
      position: fixed;
      left:0;
      top: 130px;
      text-align: center;
    `}>


    <Container width={"150px"} margin="0 auto">

      <Options />
      </Container>





<Container width="40%" margin="1rem auto">
<Link style={{textDecoration: "none"}} to="/w/story">
  <Tag active={type==="story"}>
    Casos de éxito
  </Tag>
  </Link>
<Link style={{textDecoration: "none"}} to="/w/blog">
  <Tag active={type==="blog"} > 
    Keywords
  </Tag>
  </Link>
</Container>

  </Container>
)
}

  

const Workspace =  ({match}) => {

  const [items, setItems] = useState([])
  const [status, setStatus] = useState("")
  const type= match.params.type
  const [date, setDate] = useState(0)
  const [loading, setLoading] = useState(true)


    useEffect( ()=>{

      setLoading(true)
    //const types = ["blog", "stories", "email", "modals"]

    var queries = {
      orderByChild: 'type', 
      equalTo:type,
      limitToLast: 100,

    }

   


    if(status && date)
      queries = {
      orderByChild: '&type_status_created', 
      startAt: `${type}_${status}_${new Date().getTime() - (date *1000*60*60)}`,
      endAt: `${type}_${status}_${ Date.now()}}`,
      limitToLast: 100

    }
    else if(status)
      queries = {
      orderByChild: '&type_status', 
      equalTo: `${type}_${status}`,
      limitToLast: 100
    }


     base.fetch('shooter/siac/data',{
      asArray: true,
      queries,
      
     }).then((data)=>{

          setItems(data);
         // console.log("Status:", status,data, data[0]["&created_status"])
          setLoading(false)
         
        });


    }, [type, date, status, match])

 

  return(
  <React.Fragment>

   <Navbar />

    <Sidebar
      type={type}
      status={status}
      date={date}
    
      setDate={setDate}
     //setStatus={}
    />

     <Container
     	  row
      	width="50%"
        margin="60px auto" 
        style={{
          borderRadius:"0px 0px 10px 10px", 
          paddingBottom:"4rem"
        }} 
        css={ContainerStyle}
        tabletCSS="width: 90%;"
        >


        
    {loading?
      <Container 
        center 
        height="300px">
        <h3>Cargando..</h3>
      </Container>:
    
    <ItemList

      type={type}
      status={status}
      date={date}

      setDate={setDate}
      setStatus={setStatus}


      items={items.sort((a, b) => {
          if(a["created"] < b["created"]) return -1;
          if(a["created"] > b["created"]) return 1;
            return 0;
      }).reverse()} title={type} 
    />}
  
      </Container>
  </React.Fragment>
  )
}
export default Workspace;

