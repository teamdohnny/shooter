import React, {useState, useContext} from 'react';
import {Container, Text} from 'local_npm/clay-components';
import {Input} from "Components/Primitives/Input"
import {Button, ClayButton} from "Components/Primitives/Button"

import firebase from "firebase"
import { Formik } from 'formik';



const loginContainerStyle = {
    "overflowHidden": true,
    "defaultShadow": true,
    "defaultCorner": true,
    "css": "padding: 2rem;  \n",
    "tabletCSS": "width: 90%;\n\n    ",
    "background": "white ",
    "position": "relative",
    "height": "500px",
    "width": "35%"
}


const loginContainerStyleButton ={
    "css": ` 
	    font-weight: 400;
	    position: absolute; 
	    right: 0rem; 
	    bottom: 0rem;
    `,

    "fullscreen": false,
    "width": "146px",
    "height": "44px"
}


const loginContainerStyleForm = {
    "height": "100%",
    "css": "padding-top: 80px;"
}




const Login = () => {


  const logIn = (value)=>{
  	console.log(value)
        firebase.auth().signInWithEmailAndPassword(value.name + "@DAShooter.com", value.password)
    .catch((error) =>{

    	alert("There is an error.	")
  
    });
  }


  return(
  <React.Fragment>


  <Container
  	width="100%"
  	height="100vh"
  	center>
   
     <Container
     	id = "loginContainer" 
     	{...loginContainerStyle}
        >


<Formik

      initialValues={{ name: "", password:"" }}
      onSubmit={logIn}
      render={props => (

        <Container 
        	as="form"
        	id="loginContainerStyleForm"    
        	{...loginContainerStyleForm} 
        	onSubmit={props.handleSubmit}>
        
	        <Container center>

		        <Text id="loginContainerText" weight="500" fontSize="46px" marginB="20">
		        	Bienvenido.
		        </Text>


		        </Container>

		        <Input 	
					name="name" 
					onChange={props.handleChange}
				    onBlur={props.handleBlur}
				    value={props.values.name}
			        placeholder="Name" />


		        <Input 	
		        	onChange={props.handleChange}
		            onBlur={props.handleBlur}
		            value={props.values.password}
		            name="password" 
		            placeholder="Password" 
		            type="password"/>



		    	<Button
		    		type="submit"
		    		id="loginContainerButton"
		        	{...loginContainerStyleButton} >Iniciar sesíon</Button>


	        </Container>)}/>


      </Container>



</Container>
  </React.Fragment>
  )
}
export default Login;


//ClayStyledComponent.button("")


//{type, value, name, validation}