import React, {Component, Fragment} from 'react';
import Navbar from 'Components/Molecules/Navbar';
//import Footer from 'Components/Molecules/Footer';
import { Content, Section } from './elements'
class Privacy extends Component {
	componentDidMount() {
		window.scrollTo(0, 0)
	}

	render() {
		return(
			<Fragment>
				<Navbar/>
				<div>
					<Content>
						<div>
							<Section>
							<h1> Privacy notice </h1>
							 Dohnny ("us") values ​​the trust you ("user") have in us (Dohnny)
							when using (you /user) Dohnny.com;we understand that keeping your
							information is a serious issue and not at all of low importance,
							therefore we write the following Privacy notice in a clear and
							understandable way. You should know how we use your information that
							we collect in any Dohnny section, even if you access it on mobile
							devices.By accessing or using the Service, you accept this Privacy
							Policy, our Terms of Service and our Acceptable Use Policy. If you
							do not accept the terms of this Privacy Policy, do not use our websites.
							We may make changes to this Privacy policy from time to time.
							We will post any changes on our websites. The continued use of
							our Websites after the posting of any change means that you accept
							those changes.
							<h3> Index: </h3>
							<ul>
							  <li> Information we collect about you </li>
							  <li> What information we do not collect </li>
							  <li> How we use your information </li>
							  <li> How we share the information we collect </li>
							  <li> How Dohnny protects your information </li>
							  <li> Other important information </li>
							  <li>
									{" "}
									How you can access and control the information we collect{" "}
								</li>
							  <li> How we communicate with you </li>
							</ul>
							<h2> Information we collect about you </h2>
							We collect, process and retain information about you and any
							device you may use when accessing our websites, use our services,
							register to obtain an account with us, provide us with information in
							a web form,update or add information to your account, participate
							in community discussions, chat or interact with Dohnny and the
							services of the platform. The specific information we collect
							depends on its use on the websites, as described below. If you are
							a user of our premium service paid, we will use a credit card payment
							processing company. third-party credit to collect payment
							information, including your credit card number, billing address and
							phone number. We will share this payment information with the
							third party processing company as detailed in continued on "How we
							share your information: with trusted service providers and business
							partners". We do not save your payment information.
							<ol>
							  <li> Information that tells us specifically who you are </li>
							  <p>
									The moment you create an account we need some basic
									information necessary to continue with the use of Dohnny,
								   you create an account with your own name and password,
									we will also ask you for a valid email account. There is
									also the possibility of giving us more options if you wish, for
									example: your occupation and the level of experience
								  that you have. This could include "personal information".
								 The "User's personal information" is any information about one of our users that could, alone or
								  along with other information, identify him personally.
									Information such as a username and password, an address
								  of email, a real name and a photograph are examples of
									"Personal information of the user".
								</p>
							  <li> Information we collect automatically </li>
							  <p>
								  We receive and store certain types of information every
									time you interact with us or our websites. Our sites
								  Websites use "cookies", tagging and other tracking
									technologies. This information includes information from the
									computer and fromthe connection, such as statistics on
									the views of your pages, traffic to and from our websites,
									reference URL, Ad data, your IP address and device
									identifiers; this information can also include your history of
								  navigation, transaction history and your web
									registration information.
								</p>
							  <li>
									{" "}
									Code and data you provide when interacting with platform services{" "}
								</li>
							  <p>
								  The code, animations or graphics generated within
									Dohnny's services will be used to train the algorithm.
								  It will be public only if the user is using the Free
									license, where the project will be shown as an example.
								</p>
							  <li> Information on social networks and other sites </li>
							  <p>
								  When you interact with our websites or services on a
									social media platform, we may collect the information
								  personal that makes us available on that page, including
									the identification of your account or username. If you choose to
									start session in your Dohnny account with or through a
									social network service, Dohnny and that service can share some
								  information about you and your activities.
								</p>

							</ol>
							<h2> What information we do not collect </h2>
							We do not knowingly collect confidential personal information,
							such as social security numbers, genetic data, information from
							health or religious information. Although Dohnny does not
							solicit or intentionally collect sensitive personal information, we
							give ourselves Note that you can store this type of information
							in your account, such as in a repository. If you store personal
							information confidential in our servers, is consenting that we
							store that information in our servers, which are in the United
							States. We do not collect information intentionally stored in
							their repositories or other content entries freely. If your
							repository is public, anyone (including us) can see its contents. If
							you have included private information or confidential in its
							public repository, such as email addresses, that information can be
							indexed by search or used by third parties. Also, although we
							generally do not search for content in their repositories, we can scan
							our servers looking for certain tokens or security signatures.
							If you are a child under the age of 13, you may not have a
							Dohnny account. Dohnny does not knowingly collect or steer any of
							our contents specifically to children under 13 years. If we
							discover or have reason to suspect that you are a user Under 13
							years old, unfortunately we will have to close your account. We
							do not want to discourage you from learning to code, but those are the
							rules. See our Terms of Service to obtain information about the
							cancellation of the account.
							<h2> How we use the information we collect from you </h2>
							We use your information to help us personalize and continually
							improve your experience on websites, including the compliance
							with their requests and requests for information, the analysis and
							compilation of trends and statistics, and the communication with
							you
							<h3> We can use your information to: </h3>
							  <ul>
								<li>
									{" "}
									Providing, maintaining and improving websites for internal
									purposes or other commercial purposes;{" "}
								</li>
								<li> Comply with requests for information; </li>
								<li> Provide customer support; </li>
								<li> Track and evaluate the use of websites; </li>
								<li>
									{" "}
									Communicate with you about your customer account, content owner
									account, profile or transactions with us,   * changes in
									our policies or terms;{" "}
								</li>
								<li>
									{" "}
									Send you information about the features and improvements on our
									websites;{" "}
								</li>
								<li> Send you newsletters or other materials; </li>
								<li>
									{" "}
									Send offers, promotions or other communications about our products
									and services, including special events or   Promotional,
									including services, products or events for which we collaborate or
									co-offer with a third party{" "}
								</li>
								<li>
									{" "}
									Detect, investigate and prevent activities that may violate our
									policies or be fraudulent or illegal;{" "}
								</li>
								<li>
									{" "}
									Optimize or improve our products, services and operations;{" "}
								</li>
								<li>
									{" "}
									Perform statistical, demographic and marketing analysis of website
									users and their viewing patterns.{" "}
								</li>
								<li>
									{" "}
									Train machine learning algorithms to improve Dohnny services{" "}
								</li>
							</ul>
							<h2> How we share the information we collect </h2>
							 We do not share your personal information with third parties,
							except in the cases detailed below. We can disclose Information
							that does not specifically identify you, such as aggregate
							information, device identifiers or other identifiers unique to
							third parties in the manner we deem appropriate.
							<h4> Third party service providers: </h4>
							 We have affiliates and service providers that perform functions on
							our behalf, including, among others, payment processing, hosting,
							content syndication, content management, technical integration,
							marketing, analysis and protection against fraud. These third
							parties may have access to your personal information when necessary to
							perform their functions. If we share information with these third
							parties that personally identify you, such as your name, address or
							telephone number, we will request third parties that maintain the
							confidentiality and security of their personal information, and will
							restrict their use and sale. , or distributing this information in
							any way other than to provide the services requested to the websites.
							<h4> Sale, assignment or change of control: </h4>
							 We can change our property or corporate organization while we
							provide the websites. We can transfer to another entity or its
							affiliates part or all information about you in relation to, or during
							negotiations of, any merger, acquisition, sale of assets or any
							business, other transaction of change of control or financing. In such
							circumstances, We ask the acquiring party to follow the practices
							described in this Privacy Policy. However, we can not promise that
							an acquiring party or merged entity will have the same privacy
							practices or will treat your information in the same way which is
							described in this Privacy Policy.
							<h4> Law enforcement, legal process and emergency situations: </h4>
							  We may also use or disclose your personal information if
							required by law or if we believe in good faith that such action it
							is necessary to (a) comply with the applicable law or comply with the
							laws process served on us or on the Site; (b) protect and defend
							our rights or property, the websites or our users, or (c) act to
							protect the personal safety of us, the users of the websites or
							the public.
							<h2> How we protect your information </h2>
							 We take the security of your information seriously. We use
							technical and administrative security measures, such as, among others,
							technical of encryption and authentication procedures to maintain
							the security of your online session. However, understand that although
							 We do everything possible to protect your personal information
							once we receive it, can not guarantee that the transmission of
							 data through the Internet or any other public network is 100%
							secure. It can help protect the privacy of your own information by
							using encryption and other techniques to avoid unauthorized
							interception of your personal information. You are responsible for the
							security of your information when you use non-encrypted, public or
							otherwise unsecured networks.
							<h2> Other important information </h2>
							<h4> Use of cookies </h4>
							Dohnny uses cookies to make interactions with our service easy
							and meaningful. We use cookies (and similar technologies, such
							as HTML5 localStorage) to keep you connected, remember your
							preferences and provide information for the future development
							of Dohnny. A cookie is a small piece of text that our web server
							stores on your computer or mobile device, that your Browser
							sends us when it returns to our site. Cookies do not necessarily
							identify you if you are only visiting Dohnny; however, a cookie
							can store a unique identifier for each connected user. The cookies of
							Dohnny sets They are essential for the operation of the website,
							or are used for performance or functionality. When using our site
							web, you agree that we can place this type of cookies on your
							computer or device. If you disable the capacity of your browser
							or device to accept cookies, you can not log in or use Dohnny's
							services.
							<h4> Google analytics </h4>
							We use Google Analytics as a third-party tracking service, but
							we do not use it to track it individually or collect your
							personal information from the user. We use Google Analytics to gather
							information about how our site works web and how our users, in
							general, browse and use Dohnny. This helps us to evaluate the use that
							our users make of Dohnny; compile statistical reports on the
							activity; and improve our content and website performance.
							Google Analytics collects certain simple information that does
							not personally identify over time, such as your IP address, type
							of browser, Internet service provider, reference and exit pages,
							timestamp and similar information about his use of Dohnny. We do
							not link this information with any of your personal information, such
							as your username. Dohnny will not allow any third party to use
							the Google Analytics tool to track our users individually;
							collect any personal information from the user other than the IP
							address; or correlate your IP address with your identity. Google
							provides more information about its own privacy practices and offers a
							browser add-on for disable Google Analytics tracking.
							Certain pages of our site can configure other third-party
							cookies. For example, we can incorporate content, as videos,
							from another site that establishes a cookie. We can not always
							control what cookies this third-party content establishes. <h4>
								{" "}
								Children{" "}
							</h4>
							Our Services are not intended for users under 13 years of age.
							Accordingly, we will not knowingly collect or use personal
							information of children we know to be under 13 years of age.
							Mentioning above that if we discover that there is a child under
							13, we will close your account. <h4>
								{" "}
								Users outside of Mexico{" "}
							</h4>
							If you use our websites outside the United States of Mexico, you
							understand and accept the transfer of your information
							personnel, and the collection, processing and storage of your
							personal information in the United Mexican States and other
							countries . The laws in the United Mexican States with respect
							to personal information may be different from the laws of your
							state or country
							<h2> How you can access and control the information we collect </h2>
							If you are already a Dohnny user, you can access, update, modify
							or delete your basic user profile information by editing your
							user profile .. <h4> Data retention and deletion </h4>
							Dohnny will retain the user's personal information for as long
							as their account is active or as necessary for provide the
							services. We may retain certain User Personal Information
							indefinitely, unless you delete it or request its deletion. For
							example, we do not automatically delete inactive user accounts,
							therefore, unless you choose to delete your account, We will
							retain your account information indefinitely. If you wish to
							cancel your account or delete your personal information from the user,
							you can do so in your user profile. We will retain and use your
							information as necessary to comply with our legal obligations, resolve
							disputes and enforce our agreements, but unless there are legal
							requirements, we will delete your full profile (within reason)
							within 30 days.
							<h2> How to limit the use of your information </h2>
							In many cases, you have choices about the information you
							provide and limit the way we use your information. These options,
							and any related consequence, are described in detail below.
							<h4> Personal information: </h4>
							You can choose not to provide your personal information, such as
							your name, address, telephone number or payment information, but
							you may not be able to take advantage of many features of our site and
							/or payment. Emails, newsletters and Other communications: When
							you create an account through our websites, you must provide us with
							an email address precise through which we can get in touch with
							you. The registration through our websites or applications constitutes
							Your express acknowledgment that Drifty can use your email
							address to communicate with you about offers from products from
							our side. While you can not choose not to receive notifications and
							other communications related to your account or your
							transactions, you may choose not to receive newsletters and
							promotional emails and other communications on our part through
							the function "cancel subscription" in our marketing emails and
							newsletters <h4> Location tracking: </h4>
							Most mobile devices allow you to control or disable the use of
							location services by any application on your mobile device
							through the device configuration menu. Please note that the
							exclusion of advertising network services does not mean that you will
							not receive advertising based on interests of third parties that
							do not participate in these programs. However, it will exclude you
							from interest-based advertising carried out through the
							participating networks, as provided by their policies and mechanisms
							of choice.
							<h2> How we communicate with you </h2>
							We will use your email address to communicate with you, if you
							have said it is okay, and only for the reasons who said it's
							fine You have a lot of control over how your email address is used and
							shared. through Dohnny. You can manage your communication
							preferences in your user profile .. For questions about our
							Privacy Policy, to make decisions about how to receive promotional
							communications, update your personal information or place an
							order, you can contact us at: teamdohnny@gmail.com
							<h2> Changes to our privacy notice: </h2>
							 Although most of the changes are likely to be minor, Dohnny can
							change our Statement of  privacy from time to time. We will
							provide notifications to Users of material changes to this Declaration
							of  Privacy through our website at least 30 days before the
							change takes effect by publishing a notice  on our homepage or
							by sending an email to the email address specified in your account
							 principal of Dohnny. For changes to this Privacy Statement that
							do not affect your rights, we encourage visitors to  Check this
							page frequently. Thank you for reading the privacy notices and
							accepting each of our terms.
							</Section>
			      </div>
					</Content>
					{/*<Footer/>*/}
				</div>
			</Fragment>
		)
	}
}

export default Privacy;
