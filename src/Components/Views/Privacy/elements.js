import styled from 'styled-components';

export const Content = styled.div `
	height: auto;
	padding: 100px 10%;

	${props => props.theme.media.tablet `
		padding-top: 0;
	`}
`

export const Section = styled.section `
	text-align:left;
	font-family: Rubik, 'sans-serif';
	width: 100%;
	overflow: hidden;
	font-weight: 200;
	h1,h2,h3,h4,h5,h6{
			font-weight: 500;
	}
	color: rgba(0,0,0,0.8);
`
