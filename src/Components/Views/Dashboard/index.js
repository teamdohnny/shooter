import React, {useState, useContext} from 'react';
import styled from 'styled-components';
import Navbar from "Components/Molecules/Navbar"
import {Container} from 'local_npm/react-container';
import ProjectItem, {AddProjectItem} from "./ProjectItem";

const ContainerStyle = `
  top: 100px;
  text-align: center;
  border-radius:10px;
  overflow: hidden;
`;

const Dashboard = () => {
  return(
  <React.Fragment>

   <Navbar/>

     <Container 
        width="50%"
        margin="60px auto"
        row
        style={{
          borderRadius:"0px 0px 10px 10px", 
          paddingBottom:"4rem"
        }} 

        css={ContainerStyle}>


        <AddProjectItem />
        <ProjectItem />


      </Container>
  </React.Fragment>
  )
}
export default Dashboard;
