import React, {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import Navbar from "Components/Molecules/Navbar"
import {Container, Text} from 'local_npm/clay-components';
import {Img} from 'local_npm/react-container';
import RoundAdd from "react-md-icon/dist/RoundAdd";

const ProjectItem = ({color="blue"}) => {
  return(
  <React.Fragment>
   <Navbar />
     <Container
        wrapper={{width:"50%", css:"padding:1rem;"}}
        margin="100px auto"
        height="250px"
        corner="10px"
        background={color}
        css= "padding: 1rem;"
        >
        <Link 
          to="/project/dohnny"
          style={{
            color:"white", 
            "text-decoration": "none"
          }}
        >
        <Container  
          wrapper={{height:"60%", center:true}}
          circle="100px" 
          background="white"
          center
         >

        <Img 
          width="70%"
          src="https://creative.dohnny.com/static/media/logo.44dc073c.png" />
        </Container>
    
        <Text 
          fontSize="28px" 
          weight="500">
        Dohnny App.</Text>

        <Text fontSize="18px" weight="400">
      This is just an example.</Text>
      </Link>
      </Container>
  </React.Fragment>
  )
}


export const AddProjectItem = () => {
  return(
  <React.Fragment>
   <Navbar />
     <Container

        defaultBorder 
        wrapper={{width:"50%", css:"padding:1rem;"}}
        margin="100px auto"
        height="250px"
        corner="10px"
        color="black"
        background="white"
        center
  
        >
        <RoundAdd style={{fontSize:"30px"}} />
        <br/>
<Text fontSize="18px" weight="500">
        Add new</Text>
      </Container>
  </React.Fragment>
  )
}
export default ProjectItem;
