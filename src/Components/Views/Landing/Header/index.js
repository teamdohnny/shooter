import React from 'react';
import Navbar from "Components/Molecules/Navbar"
import Brands from "Components/Molecules/Brands"
import styled from 'styled-components';
import {Container} from 'local_npm/react-container';

export const Body = styled.div`
  width: 100%;
  height: 100vh;
  margin: 0 auto;
  text-align: center;
  position: relative;
  overflow: hidden;

`;

/*

 ${props=>props.theme.media.tablet`
    height: 90vh;
    width: auto;
  `}


*/



export const Title = styled.div`
  font-size: 70px;
  color: white;
  font-weight: 500;
  width: 60%;
  margin: 0 auto;
  text-align: left;
  position: absolute;
  left: 4rem;
  bottom: 5rem;
  ${props=>props.theme.media.tablet`
    width: 90%;
    font-size: 45px;
    text-align: left;
    left: 1rem;
    line-height: 50px;
    bottom: 12vh;
  `}
`;

const Landing = () => (
   <React.Fragment>
    <Body>
    <Container
    height="100vh"
    width="100%"
    backgroundImg={require("static/imgs/Landing1.png")}
    >
    
  
    <Title>Supera tus mayores desafíos con diseño y tecnología.</Title>
      </Container>
    </Body>
  
    </React.Fragment>
);

export default Landing;