import React from 'react';
import Navbar from "Components/Molecules/Navbar"
import OutlineArrowForward from "react-md-icon/dist/OutlineArrowForward";
import styled from 'styled-components';



export const Body = styled.div`
  width: 100%;
  margin: 0 auto;
  text-align: center;
  position: relative;
`;

export const Title = styled.div`
  font-size: 45px;
  color: black;
  font-weight: 500;
  width: 90%;
  margin: 0 auto;
  text-align: left;
 
  ${props=>props.theme.media.tablet`
    width: 90%;
    font-size: 45px;
  `}
`;

export const ImgContainer = styled.div`
  margin: 0 auto;
  margin-top: 2rem;
  border-radius: 4px;
  width: 100%;
  z-index: 1;
  min-height: 565px;
`;

export const Img = styled.img`
  width: 100%;
`;


export const Card = styled.div`
  width: 45%;
  background: white;
  padding: 100px 0rem;
   position: absolute;
   border-radius: 10px 10px 0 0;
  right: 2rem;
  bottom: 0rem;
`;


export const Text = styled.div`
  width: 40%;
  height: 30px;
   position: absolute;
   color:#0059FA;
  font-weight: 500;
  left: 2rem;
  bottom: 2rem;
  ${props=>props.theme.utils.rowContent()}
`;


const Landing = () => (
   <React.Fragment>
    <Body>
    <ImgContainer>
    <Img src={require("static/imgs/Estamos-listos.png")}/>
    </ImgContainer>

    <Card>
    <Title>Comience contándonos sobre lo que estas buscando</Title>
    <Text>
    Tus primeros pasos aquí
     <OutlineArrowForward  style={{fontSize:"22px"}} />
     </Text>
    </Card>

    </Body>

  



    </React.Fragment>
);

export default Landing;