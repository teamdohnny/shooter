import React, {useState, useContext} from 'react';
import styled from 'styled-components';

import Navbar from "Components/Molecules/Navbar"
//import Footer from "Components/Molecules/Footer"
import {Container} from 'local_npm/react-container';

const ContainerStyle = `
  top: 100px;
  width: 90%;
  text-align: center;
  border-radius:10px;
  overflow: hidden;
  box-shadow: 0px 0.5px 3px rgba(0,0,0,0.2);
`;

const Landing = () => {
  return(
  <React.Fragment>
   <Navbar />
     <Container 
        margin="100px auto"
        background="red"
        style={{
          borderRadius:"0px 0px 10px 10px", 
          paddingBottom:"4rem"
        }} 
        css={ContainerStyle}>
      </Container>
  </React.Fragment>
  )
}
export default Landing;
