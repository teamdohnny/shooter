import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container} from 'local_npm/react-container';
import Brands from "Components/Molecules/Brands";
import Cards from "./Cards";
import DATA from "./data";

const Process = () => (
   <React.Fragment>

  <Container 
    width="90%" 
    css="margin-top: 50px;"
      tabletCSS="margin-top: 30px;"
    margin="0 auto">

  <Container 
    width="59%" 
      tabletCSS="width:100%;"
    margin="0 auto">

    <Typography   
      fontSize="45px" 
      align="center" 
      marginT="150" 
        tabletCSS="font-size:30px; line-height:40px;"
      marginB="50" 
      weight="500">
    Confía en nosotros.
    </Typography>

    <Typography 
      fontSize="20px" 
      align="center" 
        tabletCSS="font-size:16px; line-height:26px;"
      css="line-height:30px;"
      marginB="50">
      Al confiar en nosotros, usted esta eligiendo a una pequeña, pero experimentada compañía con personas capacitadas que trabajan para construir un mejor futuro digital.
    </Typography>
    </Container>

      <Brands />

     <Container  row margin="50px auto">
      {DATA.map((data)=><Cards {...data} key={data.title} />)}
     </Container>
  
    </Container>

  </React.Fragment>
);

export default Process;