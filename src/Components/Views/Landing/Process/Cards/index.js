import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import {Link} from "react-router-dom"


const Cards = ({
    width = "32%",
    link="",
    src = require("static/pictures/Landing/Nuestros procesos.png"),
    title = "Nuestros procesos.", 
    description = `Explora, testea y comunica visualmente los módulos de tu aplicación para validar interacciones y mejoras con tus usuarios finales. En esta etapa, tomamos tus ideas y las trasladamos en un prototipo que parecerá tu producto final pero que puede ser modificado cuantas veces se necesite. `,
    ImgCSS= "height:100%; width:auto;",
    TextCSS=""
    }) => (
   <React.Fragment>
 
    <Container
      border="solid 1px #E3E4E7"
      wrapper={{width, tabletCSS:`width:100%;`}}
      margin="1rem auto" 
      css="border-radius: 5px; cursor: pointer;  height:537px; position: relative; &:hover{border:1px solid #0059FA;}">
<a href={link} style={{textDecoration:"none"}}>
      <Container 
        width="100%"
        tabletCSS="padding:1rem;"
        css="padding:2rem; position: relative;" 
        >


        <Img 
            
            src={src}
            img={{css:ImgCSS}} 
            height="160px"
            css="margin-bottom: 5px;"
            margin="0 auto"  
            />

        <Typography 
          fontSize="16px"
          weight="400"
          marginB="20"
         css={"line-height:24px; " + TextCSS}
          marginT="40"
          
          color="#000"> 

        <Typography 
          fontSize="25px"
          weight="500"
          tabletCss={"font-size: 20px;"} 
          marginB="10"
          variant="h1"
          color="#000">
          {title}
        </Typography>
<br/>
          {description}
            
            
          </Typography>

      </Container>

<a href={link}>
      <Button transparent style={{
        marginTop:"70px", 
        position:"absolute",
        bottom:"44px",
        left:"2rem",

    }} >
              <Container center row style={{fontWeight:"500"}}><Typography weight="500" align="left" marginR="10"> Leer más </Typography> <ArrowIcon/>  </Container>
        </Button>
        </a>
        </a>
    </Container>



    </React.Fragment>
);

export default Cards;