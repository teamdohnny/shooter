const products = [
	{
	  src:require("static/pictures/Landing/Nuestros procesos.png"),
	  title:"Nuestros procesos.", 
	  description:`Trabajamos con las mejores metodologías de la industria, siguiendo los pasos y el ejemplo de los grandes como Google.`,
     ImgCSS:"height:100%; width:auto;",
  	link:"/whyus#process",
    },
    	{
    		link:"/whyus#experience",
	  src:require("static/pictures/Landing/Nuestra experiencia.png"),
	  title:"Nuestra experiencia.", 
	  description:`7 años construyendo experiencias digitales con la mejor tecnología y los mejores procesos del siglo XXI.`,
     	ImgCSS:"height:100%; width:auto;"
    },
    	{
    		link:"/whyus#team",
	  src:require("static/pictures/Landing/Nuestro equipo.png"),
	  title:"Nuestro equipo.", 
	  description:`Somos un pequeño equipo de expertos en productos digitales con una gran hambre de innovar.`,
    	ImgCSS:" width:90%;"
    },

]

export default products;