import React from 'react';
import styled from 'styled-components';

import ProductCard from "Components/Molecules/ProductCard";
import PRODUCTLIST from "Components/Constants/productList";

import Typography from 'local_npm/react-styled-typography';
import {Container} from 'local_npm/react-container';


const Products = () => (
   <React.Fragment>

    <Container 
      width="90%" 
      margin="0 auto">

    <Container 
      width="52%" 
      tabletCSS="width:100%;"
      margin="0 auto">

    <Typography 
      fontSize="45px" 
      css="line-height:55px;"
      tabletCSS="font-size:28px; line-height:38px;"
      align="center" 
      marginT="150" 
      marginB="50" 
      weight="500" >
    Vea lo que es posible de hacer con Dohnny <b style={{fontWeight:"300", color:"#0059FA"}}>Agency</b>.
    </Typography>

    <Typography 

      fontSize="20px" 
      css="line-height:30px; "
      tabletCSS="font-size:16px; line-height:26px;"
      align="center" 
      marginB="150">
     Ayudamos, guiamos y asesoramos en la planeación, diseño y desarrollo de productos digitales como mockups, prototipos y aplicaciones web.
    </Typography>

    </Container>
    {PRODUCTLIST.map((data)=><ProductCard {...data} key={data.title} />)}
    </Container>
  </React.Fragment>
);

export default Products;