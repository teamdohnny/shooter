import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import Sidebar from "./Sidebar"
import { 
  Redirect, 
  Link,
  BrowserRouter, 
  Route, 
  Switch, 
  withRouter} from 'react-router-dom';


 
import HelpIcon from "react-md-icon/dist/RoundHelpOutline";
import { Button } from 'Components/Primitives/Button';
import RoundMenu from "react-md-icon/dist/RoundMenu";
import RoundClose from "react-md-icon/dist/RoundClose";
import RoundLibraryBooks from "react-md-icon/dist/RoundLibraryBooks";

import OutlineNotifications from "react-md-icon/dist/OutlineNotifications";


import {Container} from 'local_npm/react-container';

import Options from "./Options"
import {ModalsContext} from "App"
const _ = require('lodash');









const loginContainerStyleButton ={
    "css": ` 

      font-weight: 400;
      
    `,
    "background":"#212121",
    "width": "120px",
    "height": "38px"

}
const UserCirlce = styled.div`
  height: 34px;
  width: 34px;
  border-radius: 50%;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  background-image: ${props => props.src? "url(" +props.src+ ")": "url('http://placehold.it/50x50')"};
`;

 const LogoTitle = styled.div`
    font-size: 25px;
`;

 const Logo = styled.img`
    
    width: 38px;
    margin:  0.1rem 0.5rem 0 0;

    transition: 10s;

    &:hover{
      width: 500px;
    }
   
`;

 const Nav = styled.nav`
    top:0;
    left:0;
    position: fixed;
   
    width: 100%;
    background: white;
    ${props=>!props.noShadow && "box-shadow: 0 0.6px 2px rgba(0,0,0,0.1);"};
    color: ${props=>props.theme.color.navbarText};
    height: 60px;
    ${props=>props.theme.utils.rowContent}
      z-index:9999999999999999999999999999999;
  
`;

 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent}
    width: 98%;
    margin: 0 auto;
   
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;
    white-space: nowrap;
`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
     width: 33%;
`;

 const AlignCenter = styled(_Section)`
    justify-content: flex-center;
    width: 33%;
`;


 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
     width: 33%;
`;

 const Separator = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;

const Icon = styled.i`
  vertical-align: bottom;
`;


const NavsContainer = styled.div`
  z-index:2;

  ${props=> props.shadow && ` 
    box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);
    position: fixed;
    top:0;
    width: 100%;
    height: ${65*2}px;
  `}
`;

const TabSet = styled.div`
  border: 1px red solid;
  display: ${props=>props.notNavbar?"none": "inline"};
  @media (max-width: 768px) {
    display: none;
  }
`;

 const Item = styled.div`
    position: relative; 
    height: 60px;
    cursor: pointer;
    min-width: 45px;
    font-size: 14px;
    font-weight: 500;
    color:${props=> props.active?"#212121":"#80868B" };
    ${props=>props.theme.utils.centerContent()}
    ${props=>props.cursor && "cursor: pointer;"}
    ${props=>props.margin && "margin: 0 1.5rem;"}
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 768px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}

    ${props=>props.hide && `
        @media (max-width: 768px)
        {
          display: none;
        }
    `} 

    a{
      text-decoration: none;
      color:${props=> props.active?"#212121":"#80868B" };
      font-weight: 500;
    }
`;

const TabItem = ({children, active,hide ,onClick})=>{

  const TabSelector = styled.div`
    background: #212121;
    position: absolute;
    width: 100%;
    height: 4px;
    font-size: 15px;
    border-radius: 5px 5px 0 0;
    bottom: 0;
  
  `;

  return(
    <Item 
    hide
      style={{margin:"0 1.2rem"}}
      onClick={onClick} 
      active={active}
      cursorPointer>
      {children}
      {active &&  <TabSelector/>}
    </Item>
    )
}


 const ButtonMore = styled.button`
  background: white;
  color: ${props=>props.theme.colors.green};
  border : none;
  box-shadow: 0 0.5px 5px rgba(0,0,0,0.2);
  width: 120px;
  position: relative;
  padding: 0.6rem 0.8rem;
  margin: 0 0.5rem;
  border-radius: 20px;
  overflow: hidden;
  cursor: pointer;
  font-size: 16px;
`;

const logo = require("static/img/Logo da.png")


class Header extends Component
{
  constructor(props) 
  {
      super(props)
      this.state = {
        sidebar: false,
        tutorial: false,
      }
  }
  
  render() {

    
    const tab = this.props.location.pathname;

 
    return ( 
    <ModalsContext.Consumer>
    {value=> 
    <React.Fragment>  

      <NavsContainer>
      <Nav>
        <NavContent>
              <AlignStart>
            {true && <Item style={{}}>
                        <Link to="/">
                        <Logo  src={logo}/>
                        </Link>
                        </Item>}


            <Item
              hide 
              style={{
              marginRight:"0.2rem",
              }}> 
              <Link 
                to="/"
                style={{ 
                fontWeight:"200",
                fontSize: "18px",
                color: "#212121"
                }}>
              Shooter
              </Link>
            </Item>



              {this.props.subtitle &&


<React.Fragment>
               <Item
                            hide 
                            style={{
                              width:"auto",
                            margin:"0rem",
                            }}> 
                            <Container height="40%" width="1px" background="#80868B" css="opacity:0.2;"/>
                          </Item>
              
              
                           <Item
                            hide 
                            style={{
                              width:"auto",
                         
                            }}> 
                            <div 
                              
                              style={{ 
                              fontWeight:"200",
                              fontSize: "14px",
                              color: "#80868B"
                              }}>
                            {this.props.subtitle}
                            </div>
                          </Item>

</React.Fragment>
                        }

            
          </AlignStart>



          <AlignEnd>
        

<Item>

          {this.props.status === "published" &&<Button
          type="button" 
          disabled={!this.props.created}  

                     onClick={()=>this.props.changeStatus("draft")}
                     {...{
    "css": ` 

      font-weight: 400;

      border:solid 1px #DADCE0;
      color:#5F6368;
      font-size: 14px;

    `,
    "border":"solid 1px #212121",
    "width": "120px",
    "height": "38px"

}} transparent>Hacer Borrador</Button>}




       


      {this.props.status === "draft" &&<Button
      type="button"
          disabled={!this.props.created}  

                 onClick={()=>this.props.changeStatus("published")}
                    {...{
      "css": ` 

      font-weight: 400;

      border:solid 1px #DADCE0;
      color:#5F6368;
      font-size: 14px;

      `,
      "border":"solid 1px #212121",
      "width": "120px",
      "height": "38px"

      }} transparent>Publicar</Button>}

       

</Item>



<Item>

           <Button 
           type="button"
            disabled={!this.props.save}   
            onClick={()=> this.props.submitForm()}
            {...{
              "css": ` 
          
                font-weight: 400;
                font-size: 14px;
                margin: 0 1rem; 
                
              `,
              "background":"#212121",
              "width": "120px",
              "height": "38px"
          
          }}> Guardar</Button>








          </Item>
       
          <Item>
          <Options/> 
         
          </Item> 


          </AlignEnd>
        </NavContent>
      </Nav> 

      </NavsContainer>

          </React.Fragment>}

          </ModalsContext.Consumer>
    
      );
  }
}

export default withRouter(Header)



//width: "100%";