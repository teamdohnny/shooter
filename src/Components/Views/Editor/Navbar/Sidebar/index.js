import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import { Redirect, Link,BrowserRouter, Route, Switch, withRouter} from 'react-router-dom';
//import MorePopover from "./MorePopover"
import HelpIcon from "react-md-icon/dist/RoundHelpOutline";
import { Button } from 'Components/Primitives/Button';
import RoundMenu from "react-md-icon/dist/RoundMenu";
import RoundClose from "react-md-icon/dist/RoundClose";
const _ = require('lodash');

const UserCirlce = styled.div`
  height: 34px;
  width: 34px;
  border-radius: 50%;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  background-image: ${props => props.src? "url(" +props.src+ ")": "url('http://placehold.it/50x50')"};
`;

 const LogoTitle = styled.div`
    font-size: 25px;
`;

 const Logo = styled.img`
    padding-top:2px;
    width: 23px;
    margin-left: -10px;
`;

 const Nav = styled.nav`
    top:${65}px;
    left:0;

    position: fixed;
    overflow: hidden;
    width: 100%;
    height:100vh;
    background: white;
    text-aling: left;
    
    ${props=>!props.noShadow && "box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);"};
    
    color: ${props=>props.theme.color.navbarText};
 width: 100%;

    z-index:999;
`;

 const NavContent = styled.nav`
  
    width: 100%;
    margin: 2rem auto;
`;


 
const Item = styled.div`
    position: relative; 
    height: 35px;
    cursor: pointer;
    min-width: 100%;
    font-size: 20px;
    padding-top:4px;
    font-weight: 500;
    overflow: hidden;
    color:${props=> props.active?"#0059FA":"black" };
    margin: 0.5rem 0;
    ${props=>props.cursor && "cursor: pointer;"}

    text-aling: left;

    a{
      text-decoration: none;
      color:${props=> props.active?"#0059FA":"black" };
      font-weight: 500;
    }
`;

const TabItem = ({children, active,hide ,onClick})=>{

  const TabSelector = styled.div`
    background: #0059FA;
    position: absolute;
    width: 4px;
    height: 90%;
    font-size: 15px;
    border-radius: 0px 5px 5px 0;
    bottom: 10%;
    
  `;

  return(
    <Item 
 
      onClick={onClick} 
      active={active}
      cursorPointer>
      {children}
      {active &&  <TabSelector/>}
    </Item>
    )
}



class Header extends Component
{

  
  render() {
   
    return (  
    <React.Fragment>  
          <Nav>
          <NavContent>


            <TabItem  active = {this.props.tab === ("/services")}>
                
                  <Link 
                    style={{ 
                   
                      marginLeft:"1.5rem",
                      textDecoration:"none",
                    }}
                    to="/services">


                    Servicios
                  </Link>
              </TabItem>
              <br/>

               <TabItem  active = {this.props.tab === ("/whyus")} >
                
                  <Link 
                    style={{
                      marginLeft:"1.5rem",
                      textDecoration:"none"
                    }}
                    to="/whyus">
                    Nosotros
                  </Link>
              </TabItem>
                  <br/>

                   <TabItem  active = {this.props.tab === ("/success")} >
                
                  <Link 
                    style={{ 
                    position: "relative",
                    paddingLeft:"1.5rem",
                    textDecoration:"none",
                    
                    }}
                    to="/success">


                    Casos de exito
                    <div style={{position: "absolute", width:"2px", blackground:"red", left:"0", top:"0", height:"100vh"}}/>
                  </Link>
              </TabItem>
                  <br/>
                <div style={{position:"absolute", bottom:"150px", left:"0", width:"100%"}}>
            <Item style={{height:"auto"}}>
                
                  <Button 
                  onClick={()=>{this.props.openMail()}}
                  style={{ width: "90%", height:"50px", "marginLeft":"17px"}}>Primeros pasos</Button>
              
              </Item>
       
           
              <Item style={{height:"auto", }}>

              <Link to="/contact">
          
                <Button 
                style={{
                  "marginLeft":"17px",
                  width: "90%", 
                  height:"50px"}} transparent>Contáctanos</Button>
                 </Link>
              </Item>

            
  </div>
            </NavContent>
            </Nav>  
          </React.Fragment>
      );
  }
}

export default withRouter(Header)

