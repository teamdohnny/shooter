import React, {useState, useContext, useEffect} from 'react';
import {Container, Text} from 'local_npm/clay-components';
import {Input2 as Input} from "Components/Primitives/Input"
import UploadFile from "Components/Primitives/UploadFile"
import Draft from "Components/Molecules/Draft"
//import Draft from "Components/Molecules/Draft/StaticToolbar"
import {Button,ClayButton} from "Components/Primitives/Button"
import Navbar from "./Navbar"
import firebase from "firebase"
import { Formik } from 'formik';
import OutlineInfo from "react-md-icon/dist/OutlineInfo";
import SharpArrowBack from "react-md-icon/dist/SharpArrowBack";
import {Link} from "react-router-dom"


const loginContainerStyle = {
    
    "tabletCSS": "width: 90%;",
    "background": " ",
    "position": "relative",
    "height": "",
    "width": "50%",
    margin:"0 auto",
    css:"margin-bottom: 300px;",
    "center": false
}



const loginContainerStyleButton ={
    "css": ` 

	    font-weight: 400;
	    position: absolute; 
	    right: 0rem; 
	    bottom: 0rem;
    `,
  
    "width": "146px",
    "height": "44px"
}


const loginContainerStyleForm = {
    "css": "margin-top: 130px; padding-bottom: 60px;",
    "height": "100%",
    tabletCSS:"margin-top:10px;"
}




const uuid = require('uuid/v4');


const useEditor = ({postData, update, match, history}) => {

  const [id, setID] =  useState(postData?postData.id : uuid())






  const type = postData?postData.type:match.params.type;
  
  const [status, setStatus] = useState(postData?postData.status:"draft");
  const [created, setCreated] = useState(postData?!(!postData.id):false);

  const [save, setSave] = useState(false);


  var  defaultValues = {};
  
  if(postData)
   defaultValues = postData.data 



    const submitPost  = (data)=>{



    const createdDate = postData?postData.created:Date.now()

    const posterRef = firebase.database().ref(`shooter/siac/data/${id}`)
    if(!update)
      posterRef.set({
        data,
        type,
        id, 
        status,
        "&status_created":`${status}_${createdDate}`,
        "&type_created":`${type}_${createdDate}`,
        "&type_status_created":`${type}_${status}_${createdDate}`,
        "&type_status":`${type}_${status}`,
        created:createdDate, 
        count:0, 
        author:"dev", 
        layout:"{info:'This is just an expample of the layout used here to render inputs and show info in the client.'}"
      }).then((res)=>{

     

        setSave(false)
       setCreated(true)

        setID(id);
        alert("Se ha creado este articulo.")
        
    });
    else
      posterRef.update({
        data,
        "&status_created":`${status}_${createdDate}`,
        "&type_created":`${type}_${createdDate}`,
        "&type_status_created":`${type}_${status}_${createdDate}`,
        "&type_status":`${type}_${status}`,
        lastUpdate: Date.now(), 
       }).then((res)=>{

                

           setSave(false)
        alert("Se ha actualizado este articulo.")
              /*  if(status === "draft")
          history.push(`/draft`);
        else
        history.push(`/w/${type}`);*/

      });
  }




  const changeStatus = (status = "published")=>{

 
    if(!created)
    {
      alert("Debes guardar articulo antes de publicarlo.");
      return null;
    }


    const createdDate = postData?postData.created:Date.now()

   

  


    const posterRef = firebase.database().ref(`shooter/siac/data/${id}`);
    const successLink = `/success/${type}/${id}`
    posterRef.update({
        status,
        updated: Date.now(), 
        statusChanged: Date.now(),
        "&created_status":`${createdDate}_${status}`,
        "&created_type":`${createdDate}_${type}`,
        "&created_type_status":`${createdDate}_${type}_${status}`,
        "&type_status":`${type}_${status}`,

  
      }).then((res)=>{

        //Error - road... <-()->

        setStatus(status)



        if(status === "published")
          {
             history.push(successLink)
          }
         else if(status === "draft")
          alert("Este articulo ahora es un borrador.")

      });
  }


  return {
    id,
    type,
    save,
    created,
    update,
    setSave,
    status,
    changeStatus,
    defaultValues,
    submitPost
}



}





const Title = {
 "story": "Casos de éxito",
  "blog": "Keywords",
}





const Editor = (props) => {

const {
  id,
  type,
  update,
  status,
  setSave,
  save,
  defaultValues,
  submitPost,
  changeStatus,
  created
} = useEditor(props);

console.log(props.match.params.type)




const exit = ()=>{



  //props.values !== undefined  &&

  if(save)
  { 
      if(window.confirm("¿Salir sin guardar?"))
      { 
        if(status === "draft")
          props.history.push(`/draft`);
        else
          props.history.push(`/w/${type}`);
      }
  }
  else{

      if(status === "draft")
        props.history.push(`/draft`);
      else
        props.history.push(`/w/${type}`);
  }

}



return(
<React.Fragment>

<Container {...loginContainerStyle}>


<Formik
      initialValues={defaultValues}
      
      onSubmit={(data)=>{
        submitPost(data)
      }}

      render={props => (

        <React.Fragment>
          <Navbar 
          submitForm={props.submitForm}
          update={update}
          save= {save}
          created={created}
          changeStatus={changeStatus} 
          status={status}  
          subtitle={`${update?"Actualizar":type==="blog"?"Nuevo":"Nueva" } ${Title[type]}`} />


        <Container 
        	as="form"
         
        	{...loginContainerStyleForm} 
        	onSubmit={props.handleSubmit}>
        
	        <Container>

	
		      </Container>
 
            <Input
              maxlength={154} 	
              name="titulo" 
              limit="154"
              onChange={(e)=>{
                props.handleChange(e);
                setSave(true)


              }}
              onBlur={props.handleBlur}
              value={props.values.titulo}
              placeholder="Titulo" />


		        <Input 
              maxlength={354}    	
              limit="354"
               onChange={(e)=>{
                props.handleChange(e);
                setSave(true)


              }}
              onBlur={props.handleBlur}
              value={props.values.descripcion}
              name="descripcion" 
              placeholder="Descripcion" 
              type="text"/>

            <UploadFile 
              name="cover" 
              value={props.values.cover}
              onChange={(data)=>{
                props.setFieldValue("cover",data.url);
                setSave(true)
               // console.log(data);
              }}/>

            <Draft 
              name="historia" 
              placeholder="Escribe tu historia aquí" 
              value={props.values.historia || {markdown:"", content:""}}
              onChange={(data)=>{
                props.setFieldValue("historia",data)
                setSave(true)
                //console.log(data);
              }}/>

	        </Container>

          </React.Fragment>

          )}/>



 <Container
     
   css={` 


width: 25%;
position: fixed;
left:0;
top: 130px;
text-align: center;





    `}

    tabletCSS={"position: absolute; top:0; width: 160px;"}
        >
      <div to={status === "draft"? `/draft`: `/w/${type}`}>
      <Button
          onClick={exit}
        width="auto" 
        transparent> 

        <Container 
          center 
          css="color:#5F6368;"
          row 
          style={{fontWeight:"500"}}>

          <SharpArrowBack 
            style={{fontSize:"24px"}} 
          />

          <Text 
            fontSize="16px"
            weight="500" 
            align="left" 
            marginL="10"> 
         Salir
          </Text> 

        </Container>
      </Button>
      </div>
  
  </Container>



      </Container>

  </React.Fragment>
  )
}


const injectPost = (Component)=>{
return  (props)=>{

  const [postData, setPostData] = useState({})
  const [loading, setLoading] = useState(true)

  useEffect(()=>{


    const type = props.match.params.type
    const id = props.match.params.id

   //console.log(type, id)

    if(id)
      firebase.database().ref(`shooter/siac/data/${id}`).once('value').then((snapshot)=>{
        
        const data = snapshot.val()
        setPostData(data)
        setLoading(false);

      },[])

else 
  alert("No exite 404")




  },[])

  if(loading)
    return <Container height="100vh" center background="white">
    <Text fontSize="30px">Cargando...</Text>
  
  </Container>

  return <Component {...props} update postData={postData}/>

}

}




export const EditorUpdate = injectPost(Editor);
export default Editor;



