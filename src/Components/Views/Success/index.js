import React, {useState, useContext, useEffect} from 'react';
import styled from 'styled-components';
import Navbar from "Components/Molecules/Navbar"



import {Button,ClayButton} from "Components/Primitives/Button";
import {Container, Text} from 'local_npm/clay-components';
import SharpArrowBack from "react-md-icon/dist/OutlineArrowBack";
import {Link} from "react-router-dom"

import ItemRequest from "Components/Molecules/ItemRequest/Item";


import firebase from 'firebase';
import Rebase from "re-base";



const ContainerStyle = `
  text-align: center;
  border-radius:10px;
`;


const LINKS ={
	story:"http://siacpage.herokuapp.com/story",
	blog:"http://siacpage.herokuapp.com/post"
}

const Title = {
	blog:  " ✔️ Blog publicado",
	story:   "✔️ Historia publicada"
}

  

const Workspace =  (props) => {

  
  const [postData, setPostData] = useState({})
  const [loading, setLoading] = useState(true)

  useEffect(()=>{


    const type = props.match.params.type
    const id = props.match.params.id

   //console.log(type, id)

    if(id)
      firebase.database().ref(`shooter/siac/data/${id}`).once('value').then((snapshot)=>{
        
        const data = snapshot.val()
        setPostData(data)
        setLoading(false);
    })

      },[])


  
  





  return(
  <React.Fragment>
   <Navbar hideMiddle />




 <Container
     
   css={` 


width: 25%;
position: fixed;
left:0;
top: 130px;
text-align: center;





    `}

    tabletCSS={"position: absolute; top:0; width: 160px;"}
        >
    <Link to={"/open/editor/"+postData.type+"/"+postData.id}>
      <Button 
        width="auto" 
        transparent> 

        <Container 
          center 
          css="color:#5F6368;"
          row 
          style={{fontWeight:"500"}}>

          <SharpArrowBack 
            style={{fontSize:"24px"}} 
          />

          <Text 
            fontSize="16px"
            weight="500" 
            align="left" 
            marginL="10"> 
            Atras
          </Text> 

        </Container>
      </Button>
  </Link>
  </Container>

     <Container
     	  
      	width="50%"
        margin="100px auto" 
        style={{
          borderRadius:"0px 0px 10px 10px", 
          paddingBottom:"4rem"
        }} 
        css={ContainerStyle}
        tabletCSS="width: 90%;"
        >


          <Container
          css="text-align: center; margin-bottom:30px;">

        <Text 
        align="center"
          fontSize="40px" 
          weight="500" 
          marginT="10" 
          marginB="10">

            {Title[postData.type]}
     
        </Text>
        </Container>

       
       
{postData &&  <Container margin="50px auto"> 

<a
style={{textDecoration:"none"}}

href={`${LINKS[postData.type]}/${postData.id}`} 


target="_black">
<Container css="z-index:-1;">
<ItemRequest {...postData.data} item={postData} noMenu/>
</Container>
</a>
</Container>


}








        
      </Container>
  </React.Fragment>
  )



}



export default Workspace;
