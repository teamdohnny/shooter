import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Container from 'Components/Primitives/Container';
import { Content, Info, Title, ImageContainer, Image } from './elements';
import { Button } from 'Components/Primitives/Button';
 
//const jaredImg = require('static/images/fourohfour/jared.gif')

class FourOhFour extends Component {
	render() {
		return(
			<Container>
				<Content>
					<Info>
						<Title>We´re sorry, we did not find that 😢 </Title>
						<ImageContainer>
							{/*<Image src={jaredImg}/>*/}
						</ImageContainer>
						<Link to="/">
							<Button>Go back</Button>
						</Link>
					</Info>
				</Content>
			</Container>
		)
	}
}

export default FourOhFour;

