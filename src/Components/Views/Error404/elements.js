import styled from 'styled-components';

export const Info = styled.div `
	display: flex;
	flex-direction: column;
	align-items: center;
`

export const Title = styled.div `
	font-family: Kollektif, 'sans-serif';
	font-size: 35px;
	color: ${props => props.theme.darkGray};
	margin-bottom: 50px;
	text-align: center;
`

export const Image = styled.img `
	width: 100%;
	border-radius: 7px;
`

export const ImageContainer = styled.div `
	width: 60%;
	margin-bottom: 50px;
`

export const Content = styled.div `
	width: 100%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	overflow-y: hidden;
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	${props => props.theme.media.tablet `
		padding: 0 10%;
		${Info} {
			${ImageContainer} {
				width: 90%;
			}
		}
	`}
`
