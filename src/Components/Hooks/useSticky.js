import React, {useState, useContext, useEffect} from 'react';
//import OutlineAddPhotoAlternate from "react-md-icon/dist/OutlineAddPhotoAlternate";

import {Container, Text} from 'local_npm/clay-components';


const useSticky = (active, closeAtBottom)=>{




const [sickStatus, setStatus] = useState(false);
const [boundingClientRect, setBoundingClientRect] = useState({});
const [windowSize, setWindowSize] = useState();

  useEffect(() => {


  const isClient = typeof window === 'object';

  function getSize() {


    return {
      width: isClient ? window.innerWidth : undefined,
      height: isClient ? window.innerHeight : undefined
    };
  }

  if (!isClient) {
      return false;
    }

    
    function handleResize() {
     
      setWindowSize(getSize());
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);


  }, []);



useEffect(() => {

 const backup = document.getElementById("stickyB");
 if(backup)
  {
    setBoundingClientRect(backup.getBoundingClientRect());

  }


},[windowSize])




useEffect(() => {


if(!active)
  return false

    const header = document.getElementById("stickyA");
    const sticky = header.getBoundingClientRect().top - 60;

    const stickyBottom =  header.getBoundingClientRect().top + header.getBoundingClientRect().height - 10;

    const scrollCallBack = window.addEventListener("scroll", () => {
      const condition = closeAtBottom?(window.pageYOffset > sticky && window.pageYOffset < stickyBottom) : (window.pageYOffset > sticky);
      if (condition)
      {

        setStatus(true);

      } else {
        setStatus(false);
      }

    });



    return () => {


      window.removeEventListener("scroll", scrollCallBack);

    }
  }, [active]);



return {sickStatus, bR:boundingClientRect}

}


const StickyContainer = (props)=>{



 const {sickStatus,bR} = useSticky(props.active);



  return (
    <React.Fragment>

    <Container  
      id={"stickyB"} />
    <Container 
    
    width={bR.width && sickStatus?`${bR.width}px`:"100%"}

   
    height="40px"
    background="red"
      id={"stickyA"}
      css={`


        z-index: 999;

        ${sickStatus && `

            position: fixed;
            top: 60px;
          

          `}



        `} {...props}/> 

         

    </React.Fragment>

  )
}


export default StickyContainer
