import React from 'react';
import firebase from 'firebase';
import Rebase from "re-base";

export const AuthContext = React.createContext();
const base = Rebase.createClass(firebase.database());



const uuid = require('uuid');

export default class AuthProvider extends React.Component
{
    constructor()
    {
      super()
      this.state = {
        user: null,
        loading: true
      }
    }
    
   async  __firebaseLogin()
   {
   this.__firebaseAuthListener = firebase.auth().onAuthStateChanged(async (user) =>
    {
    if (user) 
        {
          await this.setState({user,loading: false});
        }      
        else 
        {
          await this.setState({user:null, loading: false});
        }
      })
    }

  componentWillUnmount()
  {
    this.__firebaseAuthListener();
  }

  componentDidMount()
  {
     this.__firebaseLogin();
  }
  
  render()
  {     
      return (
        <AuthContext.Provider 
          value={{
            user: this.state.user,
            loading : this.state.loading,

          }}>       
          {this.props.children}
        </AuthContext.Provider>
      );
  }

}


export const withFirebase = (WrapperedComponent)=>
{
  return (props)=>(
            <AuthContext.Consumer>      
             {(rest)=>(
                <WrapperedComponent {...rest} {...props}/>
              )}
            </AuthContext.Consumer>
          )
  }