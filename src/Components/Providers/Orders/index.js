import React, { Component } from 'react';
import firebase from 'firebase';
import Rebase from "re-base";

export const OrdersContexts = React.createContext();

const _ = require('lodash');

const PRODUCT = {

  "00maanaz":{
    category: "Frutas",
    id: "00maanaz",
    name: "Manzana",
    description: "Manzana Ataulfa",
    price: 12.00,
    codigo: "001",
    units: "Pz",
    img: "https://images.unsplash.com/photo-1513677785800-9df79ae4b10b?ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80",
  },

  "00maana2":{
     category: "Frutas",
    id: "00maana2",
    name: "Uva",
    description: "Uva Francesa",
    price: 14.00,
    codigo: "005",
    units: "KG",
   
    img: "https://images.unsplash.com/photo-1515779122185-2390ccdf060b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80",
  },


    "sd":{
       category: "Frutas",
    id: "sd",
    name: "Platano",
    description: "Platano fresco",
    price: 13.00,
    codigo: "003",
    units: "KG",
    img: "https://images.unsplash.com/photo-1536228891610-d27ef66f7110?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80",
  },

    "fyjhjhhhghgjb":{
       category: "Verduras",
    id: "fyjhjhhhghgjb",
    name: "Calabaza",
    description: " Para las quesadillas",
    price: 30.00,
    codigo: "0601",
    units: "KG",
    img: "https://images.unsplash.com/photo-1534330318816-04d10b49f478?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80",
  },

  "222":{
     category: "Abarrotes",
    id: "222",
    name: "Miel",
    description: "Miel de abaja",
    price: 14.00,
    codigo: "005",
    units: "Unidad",
   
    img: "https://images.unsplash.com/photo-1543880624-c2c89de9d25e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=672&q=80",
  },


    "sdwdwf":{
      category: "Cereales",
    id: "sdwdwf",
    name: "Garbanzo",
    description: "Pozolero",
    price: 1.00,
    codigo: "003",
    units: "KG",
    img: "https://images.unsplash.com/photo-1521763069287-579ca77694e8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80",
  },



    "FRESI333":{
    category: "Frutas",
    id: "FRESI333",
    name: "Fresas",
    description: "Deli Fresa",
    price: 30.00,
    codigo: "003",
    units: "KG",
    img: "https://images.unsplash.com/photo-1518635017498-87f514b751ba?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=752&q=80",
  },
}


const base = Rebase.createClass(firebase.database());
const uuid = require('uuid/v4');


//Component Did mount shoud fetch products.

export default class FirebaseProvider extends React.Component
{
    constructor()
    {
      super()
      this.state = {
        user: null,
        loading: true,
        openOrder: null,
        products: PRODUCT,
        filterProducts: null,
        filterProductsType: null,

        order:{
          status: 0,
          products:{}
        }
      }
    }
  
  async createOrder(dateEntrega)
   {
    const id = uuid();

    const order = {
      ...this.state.order, 
      entrega: dateEntrega, 
      date: Date.now(),
      folio: Math.floor((Math.random() * 99999) + 1) + Math.floor((Math.random() * 99) + 1),
      owner: firebase.auth().currentUser.uid,//"zLnRsjuHIqYov9a6qNJPvtYwBen2",//firebase.auth().currentUser.uid,
      id
    }

   firebase.database().ref(`orders/${id}`).set(order).then((res)=>{

    firebase.database().ref(`profile/${firebase.auth().currentUser.uid}`).update({lastOrder: id}).then(()=>{
        //Open status
        this.setState({openOrder: order});
        this.cleanCar();

      }).catch((err)=>{

        alert("Hubo un error con la conexion, intenta de nuevo.EEE");

      })

    }).catch((err)=>{

        alert("Hubo un error con la conexion, intenta de nuevo.222");
     
    })


}

 addProductAmount(id,x = 1)
 {
    const products = this.state.order.products;
    products[id].amount = products[id].amount + x;

    if(products[id].amount + x <= 0)
       {
        this.removeProductFromCar(id);
       }
    else
      this.setState({order: {...this.state.order, products}});
  }



  addProductToCar(product){
    console.log(product)
    const products = this.state.order.products;
    products[product.id] = {...product, amount: 10};
    this.setState({order: {...this.state.order, products}})
  }

   removeProductFromCar(product){
    
    const products = this.state.order.products;
    delete products[product]
    this.setState({order: {...this.state.order, products}})
  }

  cleanCar()
  {
    const products = {}
    this.setState({order: {...this.state.order, products}})  
  }


  filterProduct(filter = "", filterProductsType = "category")
  {
    this.setState({filter, filterProductsType})
  }


  _filteredProduct(filter, product){

    if(!this.state.filter)
    {
      return this.state.products;
    }
    else if(this.state.filterProductsType === "category")
    {
     
      return _.filter(this.state.products, (p)=>p.category === this.state.filter);
    }

      return _.filter(this.state.products, (p)=> this.state.filter.includes(p.name));
  }

  componentDidMount(){
    base.fetch('products',{}).then((data)=>{
      this.setState({products:data})

    })
  }


  

  render()
  {  
    // if(this.state.loading)
      //return null   
      return (
        <OrdersContexts.Provider 
          value={{
            loading : this.state.loading,
            products: this.state.products,
            openOrder: this.state.openOrder,
            changeOpenOrder: (id)=>{this.setState({openOrder: id})},
            filteredProducts: this._filteredProduct(),
            filter: this.state.filter,
            filterProductsType: this.state.filterProductsType,
            orders:{
              filterProduct: this.filterProduct.bind(this),
              cleanCar: this.cleanCar.bind(this),
              value: this.state.order,
              createOrder: this.createOrder.bind(this),
              addProductAmount: this.addProductAmount.bind(this),
              addProductToCar: this.addProductToCar.bind(this),
              removeProductFromCar: this.removeProductFromCar.bind(this)
            }
          }}>       
          {this.props.children}
        </OrdersContexts.Provider>
      );
  }

}


export const withFirebase = (WrapperedComponent)=>
{
  return (props)=>(
            <OrdersContexts.Consumer>      
             {(rest)=>(
                <WrapperedComponent {...rest} {...props}/>
              )}
            </OrdersContexts.Consumer>
          )
  }



export const getTotal = (products)=>{

  

  const prices = _.map(products,(p)=>{ return {price: p.price, amount: p.amount}});

  console.log(prices)


  var total = 0;

  for (var i = prices.length - 1; i >= 0; i--)
  {


    total = total + ( prices[i].price * prices[i].amount);
  }

  console.log("Total: ", total);
  return total
}