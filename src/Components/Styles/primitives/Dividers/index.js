import styled from 'styled-components';

const Vertical = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;

//<Dohnny.Divider.Vertical>
//This call ui components on styles


//<$Dohnny.Divider.Vertical>
//This call ui components from Dohnny.