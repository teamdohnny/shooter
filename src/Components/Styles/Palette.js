const Palette  =  { 
	"primary": "#212121",
	"secondary": "#64B5F6",
	"primaryText": "#ffffff",
	"secondaryText": "#000000",
}
//#00B8D4
export default Palette;