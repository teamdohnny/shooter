import React, { Component } from 'react';
import styled from 'styled-components';

const _ = require('lodash');

export const SelectStyles = styled.select`
    padding: 0 1rem;
    font-size: 18px;
    margin: 0.5rem auto;
    width: 100%;
    height: 50px;
    border-radius: 5px;
    position: relative;
    background: white;
    color: black;
    border: 1px solid #E5E5E5;
    &:focus {
        outline: none !important;
        border:1.5px solid black;
    }
  
`;


const InputStyle2 = styled.input`
padding: 1rem;
     ${props=>props.value && `padding-top: 2.2rem;
                padding-bottom: 1.3rem;`}
 
    font-size: 12px;
    width: 100%;
    height: 50px;
    font-weight: 500;
    
   
    position: relative;
    background: #F8F9FB;
    color: #212121;
    border: none;
    border-bottom: 1px solid ${props=> props.error?"#E53935":"#E5E5E5"};  
    
    transition: 0.2s;
    &:focus {
    padding-top: 2.2rem;
    padding-bottom: 1.3rem;
    outline: none !important;
    border-bottom:1.5px solid black;
    
    }

    margin: 0.5rem auto; 
`;







const InputStyles = styled.input`
padding: 1rem;
     ${props=>props.value && `padding-top: 1.3rem;
                padding-bottom: 1.3rem;`}
 
    font-size: 22px;
    width: 100%;
    height: 70px;
    font-weight: 400;
    
        border-radius: 10px;
    position: relative;
    background: #white;
    color: #212121;
    border: none;
    border: 1px solid ${props=> props.error?"#E53935":"#DADCE0"};  
    transition: 0.2s;
    &:focus {
    padding-top: 1.3rem;
    padding-bottom: 1.3rem;
    outline: none !important;
    border:1px solid #212121;
    
    }

    margin: 0.5rem auto; 

    ${props=>props.limit && `padding-right: 4.5rem;`}


`;



const Body = styled.div`
    position: relative;
    width: 100%;
    margin: 0 auto;
    ${props=> props.error && "margin-bottom: 3.5rem;"}  
`;

const Tag = styled.label`
    position: absolute;
    color:${props=>props.error?" #E53935":"#212121"};

${props=>props.focus=== false && "color:gray;"}


    background: white;
    padding:0 0.1rem;
    border-radius:2px;
    font-size: 12px;
    font-weight: 500;
    top: 1px;
    left: 0.71rem;
`;



const TagLimit = styled.label`
    position: absolute;
    color:#BDC1C6;
   
    padding:0.2rem 0.3rem;
    border-radius:2px;
    font-size: 12px;
    font-weight: 500;
    right: 1rem;
    top: 50%;
    margin-top:-6px;
  



   
`;

const TagError = styled.label`
    width: 100%;
    position: relative;
    color:#E53935;

    padding:0 0.1rem;
    border-radius:2px;
    font-size: 12px;
    font-weight: 500;
    text-align:left;
    position: absolute;
    padding:0 0.1rem;
    bottom: -2rem;
    left: 0rem;
`;



export class Input extends Component{



  constructor(){
    super();
    this.state = {
      focus: false,
      value: ""
    }
  }
  
  
  render(){

    const error = this.props.error  && !this.state.focus
      
    const InputStyle = this.props.InputStyle || {} 

    
    return (
    <Body   error={error}  style={this.props.style}>


        <InputStyle2 
        {...this.props}
        style={InputStyle}
        error={error}

        onBlur = {()=>{this.setState({focus: false})}}
        
        onFocus = {()=>{
            this.setState({focus: true});
        }}

        type={this.props.type || "text" }
      
    
        />  
    {(this.state.focus || this.props.value) && <Tag focus={this.state.focus} error={error}>{this.props.placeholder}</Tag>}

     {(error) && <TagError>{this.props.error}</TagError>} 
    
    </Body>)
      
  }
    
}

export class Input2 extends Component{



  constructor(){
    super();
    this.state = {
      focus: false,
      value: ""
    }
  }
  
  
  render(){

    const error = this.props.error  && !this.state.focus
      
    const InputStyle = this.props.InputStyle || {} 

    
    return (
    <Body   error={error}  style={this.props.style}>


        <InputStyles
        {...this.props}
   
        style={InputStyle}
        limit={this.props.limit}
        error={error}

            onBlur = {()=>{this.setState({focus: false})}}
            onFocus = {()=>{
            this.setState({focus: true});
        }}

     
      
            maxLength ={this.props.maxlength}
        />  
    {(this.state.focus || this.props.value) && <Tag 
        
        error={error}>{this.props.placeholder}</Tag>}

     {error && <TagError>{this.props.error}</TagError>} 

     {(this.props.limit && this.props.value) && <TagLimit>{this.props.value.length}/{this.props.limit}</TagLimit>} 
    
    </Body>)
      
  }
    
}




 

export const Select = (props)=>(

<Body>
    <SelectStyles
    value={props.value}
    onChange={(e)=>{props.onChange(e.target.value)}}
   > 
       {props.options && props.options.map((item, id)=>(<option 
       key={id}
       value={item.value}>{item.title}</option>))}
    </SelectStyles> 
    {/*props.value && <Tag>{props.placeholder}</Tag>*/}
</Body>)