import React, { useEffect, useState } from "react";
import firebase from "firebase";
import FileUploader from "react-firebase-file-uploader";
import RoundWhatshot from "react-md-icon/dist/OutlineAddPhotoAlternate";

import {Container, Text} from 'local_npm/clay-components';

function isFunction(functionToCheck) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const UploadFile = ({onChange, name = "image", value =""})=> {

  const [progress, setProgress] = useState(0)
  const [isUploading, setIsUploading] = useState(false)
  const [filename, setFilename] = useState("")
  const [fileURL, setFileURL] = useState(value)



  const handleUploadStart = () => {
    setProgress(0);
    setIsUploading(true);
  }

  const handleProgress = progress => setProgress(progress);

  const handleUploadError = error => {
    setIsUploading(false);
    console.error(error);
  };

  const handleUploadSuccess = filename => {

    setProgress(100);
    setIsUploading(false);
    setFilename(filename)

  
    firebase
      .storage()
      .ref("images")
      .child(filename)
      .getDownloadURL()
      .then(url =>{ 
        setFileURL(url)
        if(isFunction(onChange))
            onChange({url, filename, src:"images"})
      });
  };





    return (
            
           <Container 
            css={`
              cursor:pointer;
              `}
              as="label"
              width="100%"
              defaultShadow>


              <Container 
                margin= "0.5rem auto"
                background="#F8F9FB" 
                backgroundImg={fileURL}
                height="400px"  
                center>


              {isUploading?
                <React.Fragment>
                  <Text color="#5F6368" fontSize="28px">{progress}%</Text>
                  <Text marginT="10" color="#5F6368" fontSize="14px">Subiendo imagen.</Text>
                </React.Fragment>:

                <React.Fragment>

                  <RoundWhatshot 
                    style={{
                      color:fileURL?"white":"#5F6368",
                      fontSize:"24px"
                    }}/>

                  <Text 
                    marginT="40" 
                    color={fileURL?"white":"#5F6368"} 
                    fontSize="14px">{fileURL? "Cambiar imagen": "Agregar una imagen (1158 x 651 pixels)"}</Text>

                </React.Fragment>
              }

            </Container>

    <input name={name}  type="hidden" value={fileURL}/>

    <FileUploader
            hidden
            accept="image/*"
            name="avatar"
            randomizeFilename
            storageRef={firebase.storage().ref("images")}
            onUploadStart={handleUploadStart}
            onUploadError={handleUploadError}
            onUploadSuccess={handleUploadSuccess}
            onProgress={handleProgress}
          />
           </Container>

            
         
   
    );
}


export default UploadFile;













/*






  const [progress, setProgress] = useState(0)
  const [isUploading, setIsUploading] = useState(false)
  const [filename, setFilename] = useState("")
  const [fileURL, setFileURL] = useState("")




      const handleUploadStart = () => {
    setProgress(0);
    setIsUploading(true);
  }

  const handleProgress = progress => setProgress(progress);

  const handleUploadError = error => {
    setIsUploading(false);
    console.error(error);
  };

  const handleUploadSuccess = filename => {

    setProgress(100);
    setIsUploading(false);
    setFilename(filename)

  
    firebase
      .storage()
      .ref("images")
      .child(filename)
      .getDownloadURL()
      .then(url =>{ 
        setFileURL(url)
        if(isFunction(onUpload))
            onUpload({url, filename, src:"images"})
      });
  };







          <FileUploader
            hidden
            accept="image/*"
            name="avatar"
            randomizeFilename
            storageRef={firebase.storage().ref("images")}
            onUploadStart={handleUploadStart}
            onUploadError={handleUploadError}
            onUploadSuccess={handleUploadSuccess}
            onProgress={handleProgress}
          />



*/