import React, { useEffect, useState } from "react";
import firebase from "firebase";
import FileUploader from "react-firebase-file-uploader";
import RoundWhatshot from "react-md-icon/dist/OutlineAddPhotoAlternate";

import {Container, Text} from 'local_npm/clay-components';

function isFunction(functionToCheck) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const UploadFile = ({onChange, name = "image", value =""})=> {

  const [progress, setProgress] = useState(0)
  const [isUploading, setIsUploading] = useState(false)
  const [filename, setFilename] = useState("")
  const [fileURL, setFileURL] = useState(value)



  const handleUploadStart = () => {
    setProgress(0);
    setIsUploading(true);
  }

  const handleProgress = progress => setProgress(progress);

  const handleUploadError = error => {
    setIsUploading(false);
    console.error(error);
  };

  const handleUploadSuccess = filename => {

    setProgress(100);
    setIsUploading(false);
    setFilename(filename)

  
    firebase
      .storage()
      .ref("images")
      .child(filename)
      .getDownloadURL()
      .then(url =>{ 
        setFileURL(url)
        if(isFunction(onChange))
            onChange({url, filename, src:"images"})
      });
  };





    return (
            
           <Container 
            css={`
              cursor:pointer;
              `}
              as="label"
              width="100%"
              defaultShadow>


              <Container 
              corner="10px"
                border={`dashed 1px #DADCE0`}
                margin= "0.5rem auto"
                background="#FFFFFF" 
                backgroundImg={fileURL}
                height="196px"  
                center>


              {isUploading?
                <React.Fragment>
                  <Text color="#5F6368" fontSize="28px">{progress}%</Text>
                  <Text marginT="10" color="#5F6368" fontSize="14px">Subiendo imagen.</Text>
                </React.Fragment>:

                <Container row width="30%" margin="0 auto" tabletCSS="width: 100%;">
<Container width="22%" height="40px" center>
                  <RoundWhatshot 
                    style={{
                      color:fileURL?"white":"#212121",
                      fontSize:"24px"
                    }}/>
 </Container>

 <Container  width="75%" height="40px" center>
                  <Text 
                    weight="500"
                    color={fileURL?"white":"#212121"} 
                    fontSize="14px">{fileURL? "Cambiar imagen": "Agregar imagen"}</Text>
</Container>
                </Container>
              }

            </Container>

    <input name={name}  type="hidden" value={fileURL}/>

    <FileUploader
            hidden
            accept="image/*"
            name="avatar"
            randomizeFilename
            storageRef={firebase.storage().ref("images")}
            onUploadStart={handleUploadStart}
            onUploadError={handleUploadError}
            onUploadSuccess={handleUploadSuccess}
            onProgress={handleProgress}
          />
           </Container>

            
         
   
    );
}


export default UploadFile;













/*






  const [progress, setProgress] = useState(0)
  const [isUploading, setIsUploading] = useState(false)
  const [filename, setFilename] = useState("")
  const [fileURL, setFileURL] = useState("")




      const handleUploadStart = () => {
    setProgress(0);
    setIsUploading(true);
  }

  const handleProgress = progress => setProgress(progress);

  const handleUploadError = error => {
    setIsUploading(false);
    console.error(error);
  };

  const handleUploadSuccess = filename => {

    setProgress(100);
    setIsUploading(false);
    setFilename(filename)

  
    firebase
      .storage()
      .ref("images")
      .child(filename)
      .getDownloadURL()
      .then(url =>{ 
        setFileURL(url)
        if(isFunction(onUpload))
            onUpload({url, filename, src:"images"})
      });
  };







          <FileUploader
            hidden
            accept="image/*"
            name="avatar"
            randomizeFilename
            storageRef={firebase.storage().ref("images")}
            onUploadStart={handleUploadStart}
            onUploadError={handleUploadError}
            onUploadSuccess={handleUploadSuccess}
            onProgress={handleProgress}
          />



*/