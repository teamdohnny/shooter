import React, { useEffect, useState, useRef } from "react";
import {Container} from "local_npm/clay-components"


const Dropdown = (props) => {

  const node = useRef();

  const [open, setOpen] = useState(false);




  const handleClickOutside = e => {
    if(node.current)

    if (node.current.contains(e.target)) {
      return;
    }

    setOpen(false);
  };

 

  useEffect(() => {
    if (open) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [open]);

 

  return (
    <React.Fragment>
    <div ref={node}>

    <Container onClick={()=>{setOpen(!open);}}>{props.children}</Container>


    {open && <Container
          background="white" 
          defaultCorner
          defaultShadow
          position="absolute" 
          distance="auto auto -180px -180px" 
          height="200px" 
          width="200px" 
          {...props} 
           >
          {props.elements}
          </Container>}
      </div>
    </React.Fragment>

  );
};

export default Dropdown;


/*

 const handleChange = selectedValue => {
    onChange(selectedValue);
    setOpen(false);
  };

<button className="dropdown-toggler" onClick={e => setOpen(!open)}>
        {value || placeholder}
      </button>
      {open && (
        <ul className="dropdown-menu">
          {options.map(opt => (
            <li className="dropdown-menu-item" onClick={e => handleChange(opt)}>
              {opt}
            </li>
          ))}
        </ul>
      )}

*/