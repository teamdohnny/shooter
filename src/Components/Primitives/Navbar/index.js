import React, {useState, useContext} from "react"
import {Container, Text} from 'local_npm/react-master-component';
import styled from 'styled-components';
import BaselineMoreVert from "react-md-icon/dist/BaselineMoreVert"
import BaselineCheckBoxOutlineBlank from "react-md-icon/dist/BaselineCheckBoxOutlineBlank"
import BaselineCheckBox from "react-md-icon/dist/BaselineCheckBox"

const NAVBARHEIGHT = "60px"

const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
   
    
     white-space: nowrap;
`;

export const AlignStart = styled(_Section)`
    justify-content: flex-start;
   width: 50%;
`;


export  const AlignEnd = styled(_Section)`
    justify-content: flex-end;
       width: 50%;
      
`;



/*

export const AlignCenter = styled(_Section)`
    justify-content: flex-center;
   width: 30%;
`;


<AlingNav direction="start">
<AlingNav>

<AlingNav direction="center">
<AlingNav>

<AlingNav direction="end">
<AlingNav>


<ItemNav active>

<TabNav active>



*/


const Item = (props)=>{
  return (<Container
        width="auto"
        {...props}
        center
        height={NAVBARHEIGHT}>
        {props.children}
      </Container>)
}


const Nav = (props)=> {
  const {children, top="0"} = props

  return (<Container
    defaultShadow
    defaultCorner
    center
        background="white"
        {...props}
        width = "100%"
        height="60px"
        margin="2rem 0"
        row 
        css={"padding: 0 1rem;  .myIcons{display:none;} &:hover{.myIcons{display: inline-block} }"}
        height={NAVBARHEIGHT}>
        {children}
      </Container>)

  /*
        position= "fixed"
        distance ={ `${top} 0 auto 0`}

  */
}





const ItemRequest = (props)=> {
  const {children, top="0"} = props

  return (  
      <Nav>
        <AlignStart>
        <Item 
        onClick={props.close}
        center
        width="40px">
      {props.check?<BaselineCheckBox style={{fontSize:"24px"}}/>:<BaselineCheckBoxOutlineBlank style={{fontSize:"24px"}}/>}
        </Item>


          <Item width="auto" margin="0 1rem">
            <Text fontSize="18px" weight="500" color="black">{props.title}</Text>
          </Item>
        </AlignStart>
     

        <AlignEnd>

           <Item 
        margin="0 0.5rem"
        center
        >
        <Text fontSize="14px" color="rgba(0,0,0,0.6)">Completed</Text>

        </Item>



        <Item 
       margin="0 0.5rem"
        center
        >
        <Text fontSize="14px" color="rgba(0,0,0,0.6)">Posted April 14</Text>

        </Item>
        <Item 
        margin="0 0.5rem"
        center
        width="40px">
        <b><BaselineMoreVert className="myIcons"/></b>
        </Item>
        </AlignEnd>
      </Nav> )
}

export default ItemRequest;