import styled from 'styled-components';
import React from 'react';
import BaselineArrowForward from "react-md-icon/dist/BaselineArrowForward";
import RoundPlayCircleFilledWhite from "react-md-icon/dist/RoundPlayCircleFilledWhite";
import {Container, Text} from 'local_npm/clay-components';


export const Button = (props)=>(<Container


    background={(!props.transparent ? "#212121" : "transparent")}
        color= {(!props.transparent ? "white" : "#212121")}

      {...props}

      css={`

         /*====Start Default Button styles====*/

        position: relative;
        margin: 0;
        border-radius: 3px;
        overflow: hidden;
        cursor: pointer;
        font-weight: 500;
        font-size: 18px;
        border: 0; 

        /*====Finsih Default Button styles====*/



        ${props.css && props.css}

        ${props.disabled && "opacity: 0.2; cursor: not-allowed;"}


        `}
       as="button"/>)




/*
    <Button transparent css="&:focus {outline: none; }" >
    <Container
      center 
      row 
      css="color: #80868B; padding: 0.6rem 2rem;"
    
      style={{fontWeight:"500"}}>
    
      <Text 
        fontSize="14px"
        weight="500" 
        align="left" 
        marginL="10"> 
          Filtro
        </Text>
        <BaselineTune style={{fontSize:"22px"}}  /> 
        </Container>
      </Button>

*/

export const ArrowIcon = styled(BaselineArrowForward) `
	color: blue;
`

export const PlayIcon = styled(RoundPlayCircleFilledWhite) `
	color: ${props => props.theme.secondaryBlue};
	font-size: 20px;
`
