import styled, {createGlobalStyle} from 'styled-components';
import React from 'react';

const BodyScrollFixed = createGlobalStyle`
  body {
    position: fixed;
    width:100%;
  }`

export default BodyScrollFixed