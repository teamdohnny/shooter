import React from 'react';

import {Route, Redirect } from 'react-router-dom';
import Login from "Components/Views/Login";
import Error404 from "Components/Views/Error404";
//<code>{location.pathname}</code>
export const NoMatch= ({ location }) => (
  <div>
    <Error404 />
  </div>
)


export const AuthenticatedRouteRedirect = ({component: Component, authenticated, ...rest}) =>
{
  return (
    <Route
      {...rest}
      render={(props) => authenticated?
        <Component {...props} {...rest} />
          :<Redirect to="/" />}
      />)
}

export const AuthenticatedRoute = ({component: Component, authenticated, ...rest}) =>
{
  return (
    <Route
      {...rest}
      render={(props) => authenticated?
        <Component {...props} {...rest} />
          :<Login {...props}/>}
      />)
}


export const AuthenticatedRouteHidden = ({component: Component, authenticated, ...rest}) =>
{
  return (
    <Route
      {...rest}
      render={(props) => authenticated?
        <Component {...props} {...rest} />
          :null}
      />)
}
