import React, { Component } from 'react';
import styled from 'styled-components';

import {
  Route, 
  Redirect, 
  Switch 
  } from 'react-router-dom';

import {
  NoMatch, 
  AuthenticatedRouteRedirect,

  AuthenticatedRoute
  } from './utils/auth';

import Login from "Components/Views/Login"
import Logout from "Components/Molecules/Logout"
import Dashboard from "Components/Views/Dashboard"
import Profile from "Components/Views/Profile"
import Error404 from "Components/Molecules/Error404";
//Import components
import Navbar from "Components/Molecules/Navbar"

//Nested Routes:
import Workplace from "./Workplace"



import Create from "Components/Molecules/NewProject"





export default ({authenticated})=>(
  <div>

      <Switch>
        {/*Primary Components Routes*/}

        <AuthenticatedRoute
          exact
          path="/"
          authenticated={authenticated}
          component={(props) => { 
            return (<Redirect {...props} to="/dashboard" /> )  
          }} />

        <AuthenticatedRouteRedirect 
          path="/dashboard/" 
          authenticated={authenticated}
          component={Dashboard}
        />

          <Route  
          path="/w/" 
          authenticated={authenticated}
          component={Workplace} 
          />


        <Route  
          path="/u/:id" 
          component={Profile} />

        <Route  
          path="/logout" 
          render={(props) => { return <Logout />}} />

        <Route 
          exact 
          path="/404/" 
          component={Error404} 
          />

        <Route 
          component={NoMatch}/>

      </Switch>
      
      <Switch>
         
          <Route
            path="/w/preview/app/:id" 
            component={null} />

          <Route
            path="/dashboard" 
            authenticated={authenticated} 
            component={Navbar} />

          <Route
            path="/w" 
            authenticated={authenticated} 
            component={Navbar} />

          <Route
            path="/u" 
            authenticated={authenticated} 
            component={Navbar} />

     </Switch>

     <AuthenticatedRouteRedirect 
          path="/dashboard/create-project" 
          authenticated={authenticated}
          component={Create}
        />

  </div>
);