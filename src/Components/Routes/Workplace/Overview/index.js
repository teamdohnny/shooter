import React, { Component } from 'react';
import {Route, Redirect, Switch } from 'react-router-dom';

//Components
import Project from "Components/Views/Project"



const _ = require('lodash');

class App extends Component 
{
  render()
  {
    return (
      
      <div>
      <Switch>

        <Route
          path={`${this.props.match.url}/:id`} 
          component={Project} 
        />

      </Switch>

    </div>
  );
    }
}
export default App
