import React, { Component } from 'react';
import {Route, Redirect, Switch } from 'react-router-dom';

// Routes
import Notebook from "./Notebook"
import Overview from "./Overview"
import Preview from "./Preview"
//import {PreviewProject} from "Files/Views/Preview"

import Console from 'Components/Molecules/Console';
import DragManager from "Files/Providers/DragManager";
import XrayProvider from "Files/Providers/XrayManager"
const _ = require('lodash');

//Current Path: w/
class App extends Component 
{
  render()
  {
    return (
      <XrayProvider>
        <DragManager>
        
        <div style={{position:"relative"}}>
          <Switch>

            <Route
              path={`${this.props.match.url}/overview/`} 
              component={Overview} 
            />
     
            <Route  
              path={`${this.props.match.url}/notebook/`} 
              component={Notebook} 
            />

               <Route  
              path={`${this.props.match.url}/preview/`} 
              component={Preview} 
            />
    
        </Switch>
        <Console />
        </div>
        </DragManager>
    </XrayProvider>
  );
    }
}

export default App
