import React, { Component } from 'react';
import {Route, Redirect, Switch } from 'react-router-dom';



import Notebook from "Components/Views/Notebook"



class App extends Component 
{
  render()
  {
    return (
      

      <div >
        <Switch>


        <Route
        path={`${this.props.match.url}/:id`} 
        component={Notebook} 
        />


      </Switch>
      
  

      </div>
  );
    }
}
export default App
