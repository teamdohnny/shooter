import React, { Component } from 'react';
import {Route, Redirect, Switch } from 'react-router-dom';

//============================================

import Preview from "Components/Views/Preview"
import {PreviewProject} from "Components/Views/Preview"
//import Xray from "Components/Molecules/Xray"
const _ = require('lodash');

class App extends Component 
{
  render()
  {
    return (
      
      <div>
      <Switch>

        <Route
        exact
          path={`${this.props.match.url}/component/:id/app/:projectId`} 
          component={Preview} 
        />
         <Route
          path={`${this.props.match.url}/app/:id`} 
          component={PreviewProject} 
        />

      </Switch>

      

    

    </div>
  );
    }
}
export default App
 