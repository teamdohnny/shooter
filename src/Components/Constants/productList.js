const products = [
	{
	  src:require("static/pictures/Landing/Mockups y prototipos.png"),
	  title:"Mockups y prototipos.", 
	  description:`Explora, testea y comunica visualmente los módulos de tu aplicación para validar interacciones y mejoras con tus usuarios finales. En esta etapa, tomamos tus ideas y las trasladamos en un prototipo que parecerá tu producto final pero que puede ser modificado cuantas veces se necesite. `
    },
    	{
	  src:require("static/pictures/Landing/Desarrollo web.png"),
	  title:"Desarrollo web y PWA.", 
	  description:`Diseñamos y desarrollamos web apps y progresive web apps, ya sea que quieras iniciar un proyecto nuevo o agregar módulos al que tienes actualmente. Te ayudamos a elegir las tecnologías que mejor se adecuan a las necesidades de tu producto, siempre con enfoque en mantenimiento y escalabilidad.`   
		},
	   {
	  src:require("static/pictures/Landing/UX y producto.png"),
	  title:" UX y producto.", 
	  description:`Realizamos un taller que puede durar unas horas o semanas completas con tu equipo de diseño y desarrollo para ayudarte a tomar las decisiones correctas en la construcción y el diseño de un nuevo producto o la mejora de uno existente. El objetivo siempre es construir algo deseable y escalable.` 
	  	,consultoria:true
	},
	      {
	  src:require("static/pictures/Landing/Stacks tecnologicos.png"),
	  title:"Stacks tecnologicos.", 
	  description:`Realizamos un taller con tu equipo de desarrollo con el objetivo de entender las necesidades de tu producto para encontrar las tecnologías que mejor se adecuen. El objetivo siempre es construir algo deseable y escalable tecnológicamente.`  
	  	,consultoria:true
	},

    	{
	  src:require("static/pictures/Landing/Metodologias agiles.png"),
	  title:"Metodologias agiles.", 
	  description:`En uno o varios talleres te ayudamos a ti y a tu equipo a entender y adoptar las metodologías agiles enfocadas en aumentar la productividad entregando resultados cuantificables para mejoras constantes.` 
  	,consultoria:true
	},
   

]

export default products;